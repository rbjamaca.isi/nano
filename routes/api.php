<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdvertController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TrainingController;
use App\Http\Controllers\SpeakerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('auth.login'); 
    Route::post('register', [AuthController::class, 'register']);
    Route::get('logout', [AuthController::class, 'logout'])->middleware('auth:api');
});

Route::group(['middleware' => ['auth:api']], function () {
    Route::apiResource('member', 'UserController');
    Route::post('upload/proof', [UserController::class, 'uploadProof']);
    Route::get('member/training/join/{id}', [UserController::class, 'trainingJoin']);
    Route::apiResource('speaker', 'SpeakerController');
    Route::get('admin/attendees', [AdminController::class, 'attendees']);
    Route::get('admin/attendees/{id}', [AdminController::class, 'trainingAttendees']);
    Route::get('admin/trainings', [AdminController::class, 'trainings']);
    Route::put('admin/training-status/{id}', [AdminController::class, 'trainingStatus']);
    Route::put('admin/attendee/status/{id}', [AdminController::class, 'attendeeStatus']);
    Route::get('admin/charts/overview', [AdminController::class, 'chartOverview']);
    Route::apiResource('admin', 'AdminController');
    Route::post('training', [TrainingController::class, 'store']);
    Route::get('tags', [TrainingController::class, 'tags']);
    Route::post('training/proof/{id}', [Usercontroller::class, 'trainingProof']);
    Route::get('auth-user', [Usercontroller::class, 'authenticatedUser']);
    Route::put('admin/member/{id}', [UserController::class,'changeStatus']);
    Route::post('training-update', [TrainingController::class, 'update']);
    Route::apiResource('sponsor', 'SponsorController');
    Route::get('training-availed', [UserController::class, 'availed']);
});

Route::post('member/details', [AuthController::class, 'register']);
Route::post('training/non-member/proof/{id}', [Usercontroller::class, 'trainingProofNonMember']);
Route::post('email-checker', [Usercontroller::class, 'emailChecker']);
Route::get('training', [TrainingController::class, 'index']);
Route::get('training/{id}', [TrainingController::class, 'show']);
Route::get('link/code/{code}', [TrainingController::class, 'code']);
Route::get('public/advert/', [AdvertController::class, 'index']);
Route::get('public/advert/{id}', [AdvertController::class, 'show']);
Route::apiResource('advert', 'AdvertController');
Route::post('member-update', [UserController::class,'update']);
Route::post('password', [UserController::class,'password']);

Route::group(['prefix' => 'auth'], function () {
    Route::post('/', [UserController::class, 'create']);
    Route::get('/userslist', [UserController::class, 'userlist']);
});

