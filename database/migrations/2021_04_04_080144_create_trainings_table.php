<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('code');
            $table->longText('desc')->nullable();
            $table->timestamp('schedule_start')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('schedule_end')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->text('location');
            $table->text('link');
            $table->text('image')->nullable();
            $table->float('fee')->nullable();
            $table->boolean('is_free')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
