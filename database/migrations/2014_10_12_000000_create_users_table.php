<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('lastname');
            $table->enum('role', ['Member', 'Participant', 'Admin', 'Super Admin'])->default('Member');
            $table->enum('gender', ['Male', 'Female'])->nullable();
            $table->string('civilstatus')->nullable();
            $table->date('dob')->nullable();
            $table->string('occupation')->nullable();
            $table->string('avatar')->nullable();
            $table->text('street')->nullable();
            $table->string('barangay')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('proof')->nullable();
            $table->string('mode')->nullable();
            $table->string('reference')->nullable()->unique();
            $table->enum('type', ['Regular', 'Student']);
            $table->boolean('is_premium')->default(false);
            $table->enum('status', ['Pending', 'Active', 'Inactive', 'Rejected'])->default('Pending');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
