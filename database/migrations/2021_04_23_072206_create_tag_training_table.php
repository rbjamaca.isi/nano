<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tag_training', function (Blueprint $table) {
            $table->id();
            $table ->unsignedBigInteger('training_id');
            $table->foreign('training_id')
                    ->references('id')->on('trainings')
                    ->onDelete('cascade');
            $table ->unsignedBigInteger('tag_id');
            $table->foreign('tag_id')
                    ->references('id')->on('tags')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tag_training');
    }
}
