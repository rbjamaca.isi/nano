<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Speaker;
use App\Training;
use Faker\Generator as Faker;

$factory->define(Speaker::class, function (Faker $faker) {
    return [
        'training_id' => Training::doesntHave('speakers')->select('id')->orderByRaw("RAND()")->first()->id,
        'name' => $faker->name
    ];
});
