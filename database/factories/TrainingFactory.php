<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Training;
use Faker\Generator as Faker;

$factory->define(Training::class, function (Faker $faker) {
    return [
        'title' => ucwords($faker->sentence(3)),
        'location' => $faker->dateTimeBetween('now', '+20 days'),
        'certificate' => $faker->dateTimeBetween('now', '+20 days'),
        'code' => $faker->numerify('####'),
        'desc' => $faker->paragraph,
        'schedule_start' => $faker->dateTimeBetween('now', '+20 days'),
        'schedule_end' => $faker->dateTimeBetween('now', '+2 months'),
        'fee' => $faker->numberBetween(250, 300),
        'discounted_fee' => 20,
        'location' => 'Virtual',
        'image' => $faker->imageUrl,
        'link' => 'https://meet.google.com/qqf-qkit-kfe'
    ];
});
