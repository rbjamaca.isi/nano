<?php

use App\Speaker;
use Illuminate\Database\Seeder;
use App\Training;

class TrainingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Training::class, 10)->create();
        factory(Speaker::class, 10)->create();
    }
}
