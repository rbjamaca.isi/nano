<?php

use App\User;
use Illuminate\Database\Seeder;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('trainings')->truncate();
        DB::table('training_user')->truncate();
        DB::table('speakers')->truncate();
        // DB::table('tag_training')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        factory(User::class, 1)->create();
    }
}
