// ** Redux Imports
import { combineReducers } from 'redux'

// ** Reducers Imports
import auth from './auth'
import navbar from './navbar'
import layout from './layout'
import file from './file'
const ReduxStore = require('@uppy/store-redux')

const rootReducer = combineReducers({
  auth,
  navbar,
  layout,
  file,
  uppy: ReduxStore.reducer
})

export default rootReducer
