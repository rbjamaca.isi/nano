// ** Initial State
const initialState = {
    files: []
  }
  
  const fileReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'HANDLE_FILE_SAVE':
        return { ...state, files: action.payload }
      default:
        return state
    }
  }

  export default fileReducer