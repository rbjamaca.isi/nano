import { lazy } from 'react'

// ** Document title
const TemplateTitle = '%s - NANO'

// ** Default Route
const DefaultRoute = '/home'

// ** Merge Routes
const Routes = [
  // ** Admin Routes
  {
    path: '/admin',
    component: lazy(() => import('../../views/admin/dashboard/ecommerce')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/trainings',
    component: lazy(() => import('../../views/training/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/trainings/attendees/:id',
    component: lazy(() => import('../../views/attendees/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/members',
    component: lazy(() => import('../../views/member/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/member-profile/:id',
    component: lazy(() => import('../../views/shared/profile')),
    layout: 'BlankLayout',
    exact: true,
    meta: {
      resource: 'Member'
    }
  },
  {
    path: '/admin/member/:id',
    component: lazy(() => import('../../views/shared/profile')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/attendees/view',
    component: lazy(() => import('../../views/attendees/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/sponsors',
    component: lazy(() => import('../../views/sponsor/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/sponsor/add',
    component: lazy(() => import('../../views/sponsor/add')),
    exact: true,
    meta: {
      navLink: '/sponsors',
      resource: 'Admin'
    }
  },
  {
    path: '/ads/add/:id',
    component: lazy(() => import('../../views/advertisement/add')),
    exact: true,
    meta: {
      navLink: '/sponsors',
      resource: 'Admin'
    }
  },
  {
    path: '/sponsor/preview/:id',
    component: lazy(() => import('../../views/sponsor/preview')),
    exact: true,
    meta: {
      navLink: '/sponsors',
      resource: 'Admin'
    }
  },
  {
    path: '/certificates',
    component: lazy(() => import('../../views/certificate/view')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/certificate/preview/:id',
    component: lazy(() => import('../../views/certificate/preview')),
    exact: true,
    layout: 'BlankLayout',
    meta: {
      navLink: '/certificate',
      resource: 'Admin'
    }
  },
  // ** Member Routes
   {
    path: '/availed',
    component: lazy(() => import('../../views/training/availed')),
    exact: true,
    layout: 'BlankLayout',
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/member/add/:firstname/:lastname/:email',
    component: lazy(() => import('../../views/member/add')),
    exact: true,
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  // ** Reports
  {
    path: '/report/members',
    component: lazy(() => import('../../views/report/member')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/report/attendees',
    component: lazy(() => import('../../views/report/attendees')),
    exact: true,
    meta: {
      resource: 'Admin'
    }
  },
  // ** Shared Routes
  {
    path: '/home',
    component: lazy(() => import('../../views/training')),
    exact: true,
    layout: 'BlankLayout',
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/landing',
    component: lazy(() => import('../../views/training')),
    exact: true,
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/training/add',
    component: lazy(() => import('../../views/training/add')),
    exact: true,
    meta: {
      navLink: '/trainings',
      resource: 'Admin'
    }
  },
  {
    path: '/training/view',
    component: lazy(() => import('../../views/training/view')),
    exact: true,
    meta: {
      navLink: '/trainings',
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/admin/training/preview/:id',
    component: lazy(() => import('../../views/training/preview')),
    exact: true,
    meta: {
      navLink: '/trainings',
      resource: 'Admin'
    }
  },
  {
    path: '/admin/training/edit/:id',
    component: lazy(() => import('../../views/training/edit')),
    exact: true,
    meta: {
      navLink: '/trainings',
      resource: 'Admin'
    }
  },
  {
    path: '/training/preview/:id',
    component: lazy(() => import('../../views/training/preview')),
    exact: true,
    layout: 'BlankLayout',
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/training/edit/:id',
    component: lazy(() => import('../../views/training/edit')),
    exact: true,
    meta: {
      navLink: '/trainings',
      resource: 'Admin'
    }
  },
  {
    path: '/file',
    component: lazy(() => import('../../views/shared/file-uploader'))
  },
  {
    path: '/home',
    component: lazy(() => import('../../views/Home')),
    meta: {
      resource: 'Admin'
    }
  },
  {
    path: '/second-page',
    component: lazy(() => import('../../views/SecondPage'))
  },
  {
    path: '/login',
    component: lazy(() => import('../../views/Login')),
    layout: 'BlankLayout',
    meta: {
      authRoute: true
    }
  },
  {
    path: '/register',
    component: lazy(() => import('../../views/Register')),
    layout: 'BlankLayout',
    meta: {
      authRoute: true
    }
  },
  {
    path: '/forgot-password',
    component: lazy(() => import('../../views/ForgotPassword')),
    layout: 'BlankLayout',
    meta: {
      authRoute: true
    }
  },
  {
    path: '/reset-password',
    component: lazy(() => import('../../views/ResetPasswordV2')),
    layout: 'BlankLayout',
    meta: {
      authRoute: true
    }
  },
  {
    path: '/error',
    component: lazy(() => import('../../views/Error')),
    layout: 'BlankLayout'
  },
  {
    path: '/training/link/:code',
    component: lazy(() => import('../../views/misc/ComingSoon')),
    exact: true,
    meta: {
      // publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/about',
    component: lazy(() => import('../../views/About')),
    layout: 'BlankLayout',
    exact: true,
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  },
  {
    path: '/about_us',
    component: lazy(() => import('../../views/About')),
    exact: true,
    meta: {
      publicRoute: true,
      action: 'read',
      resource: 'Auth'
    }
  }
]

export { DefaultRoute, TemplateTitle, Routes }
