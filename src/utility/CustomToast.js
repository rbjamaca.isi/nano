import Avatar from '@components/avatar'
import { Fragment } from 'react'


export const CustomToast = ({ title, msg, icon, color }) => (
    <Fragment>
      <div className='toastify-header'>
        <div className='title-wrapper'>
          <Avatar size='sm' color={color} icon={icon} />
          <h6 className='toast-title font-weight-bold'>{title}</h6>
        </div>
      </div>
      <div className='toastify-body'>
        <span>{msg}</span>
      </div>
    </Fragment>
  )