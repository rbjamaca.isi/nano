// ** React Imports
import { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'

// ** Custom Components
import Avatar from '@components/avatar'

// ** Utils
import { isUserLoggedIn } from '@utils'

// ** Store & Actions
import { useDispatch } from 'react-redux'
import { handleLogout } from '@store/actions/auth'

// ** Third Party Components
import { UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap'
import { User, Mail, CheckSquare, MessageSquare, Settings, CreditCard, HelpCircle, Power, Award, ChevronDown } from 'react-feather'

// ** Default Avatar Image
import defaultAvatar from '@src/assets/images/portrait/small/avatar-s-11.jpg'
import { getUserData, renderClient } from 'utility/Utils'

const UserDropdown = ({ type }) => {
  // ** State
  const history = useHistory()

  // ** Store Vars
  const dispatch = useDispatch()

  // ** State
  const [userData, setUserData] = useState(null)

  //** ComponentDidMount
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      setUserData(JSON.parse(localStorage.getItem('userData')))
    }
  }, [])

  //** Vars
  const userAvatar = (userData && userData.avatar) || defaultAvatar,
    id = isUserLoggedIn() && getUserData().id

  return (
    <UncontrolledDropdown tag='li' className='dropdown-user nav-item'>
      <DropdownToggle href='/' tag='a' className='nav-link dropdown-user-link' onClick={e => e.preventDefault()}>
        <div className='user-nav d-sm-flex d-none'>
          <span className='user-name font-weight-bold'>
            {type === 'member' && <span> Welcome, </span>}
            {(userData && `${userData['firstname']} ${userData['lastname']}`) || ''} {type === 'member' && <ChevronDown size={14} />}</span>
          {type !== 'member' && <span className='user-status'>{(isUserLoggedIn() && getUserData().role)}</span>}
        </div>
        {type !== 'member' && renderClient(userData)}
      </DropdownToggle>
      <DropdownMenu left>
        {type === 'member' &&
          <DropdownItem onClick={() => history.push(`/member-profile/${id}`)}>
            <User size={14} className='mr-75' />
            <span className='align-middle'>Profile { }</span>
          </DropdownItem>
        }
        {type === 'member' &&
          <DropdownItem onClick={() => history.push('/availed')}>
            <Award size={14} className='mr-75' />
            <span className='align-middle'>My Trainings</span>
          </DropdownItem>
        }
      <DropdownItem tag={Link} to='/login' onClick={() => dispatch(handleLogout())}>
        <Power size={14} className='mr-75' />
        <span className='align-middle'>Logout</span>
      </DropdownItem>
    </DropdownMenu>
    </UncontrolledDropdown >
  )
}

export default UserDropdown
