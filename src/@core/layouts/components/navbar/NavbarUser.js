// ** Dropdowns Imports
import { Fragment, useState } from 'react'

import UserDropdown from './UserDropdown'

// ** Third Party Components
import { Sun, Moon, Menu } from 'react-feather'
import { NavItem, NavLink, Button } from 'reactstrap'
import { isUserLoggedIn } from 'utility/Utils'
import { useHistory } from 'react-router-dom'

// ** Custom Components
import Sidebar from '@components/sidebar'
import Register from '@src/views/Shared-Register'

const NavbarUser = props => {
  // ** States
  const [open, setOpen] = useState(false)

  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  // ** Props
  const { skin, setSkin, setMenuVisibility } = props
  const history = useHistory()
  // ** Function to toggle Theme (Light/Dark)
  const ThemeToggler = () => {
    if (skin === 'dark') {
      return <Sun className='ficon' onClick={() => setSkin('light')} />
    } else {
      return <Moon className='ficon' onClick={() => setSkin('dark')} />
    }
  }

  return (
    <Fragment>
      <ul className='navbar-nav d-xl-none d-flex align-items-center'>
        <NavItem className='mobile-menu mr-auto'>
          <NavLink className='nav-menu-main menu-toggle hidden-xs is-active' onClick={() => setMenuVisibility(true)}>
            <Menu className='ficon' />
          </NavLink>
        </NavItem>
      </ul>
      <div className='bookmark-wrapper d-flex align-items-center'>
        <NavItem className='d-none d-lg-block'>
          <NavLink className='nav-link-style'>
            {/* <ThemeToggler /> */}
          </NavLink>
        </NavItem>
      </div>
      <ul className='nav navbar-nav align-items-center ml-auto'>
        {isUserLoggedIn() ? <UserDropdown /> : window.location.href.toLowerCase().startsWith(`${window.location.origin}/member/add`) ? <Button color="primary" onClick={() => history.push('/register')} >Start Over</Button> : <div>
          <Button color="secondary" onClick={toggleSidebar} >Create New Account</Button> &nbsp;
          <Button color="primary" onClick={() => history.push('/login')} >Login</Button>
        </div>}
      </ul>
      <Sidebar
        size='lg'
        open={open}
        title='Sign Up'
        headerClassName='mb-1'
        contentClassName='p-0'
        toggleSidebar={toggleSidebar}
      >
        <Register />
      </Sidebar>
    </Fragment>
  )
}
export default NavbarUser
