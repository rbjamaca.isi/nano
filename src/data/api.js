import { getUserData } from "utility/Utils.js"
import http from "./http-common.js"

export const API_ACTION = async (data, url, id = null) => {
    const method = id === null ? http.post : http.put
    return await method(url, data, {
        headers: {
        Accept: 'application/json',
        Authorization: localStorage.getItem('accessToken') && `Bearer ${localStorage.getItem('accessToken')}`
        }
    }).then(response => {
        return response
    })
}

export const API_RETRIEVE = async (url, action = '') => {
    const method = action === '' ? http.get : http.delete
    return await method(url, {
        headers: {
        Accept: 'application/json',
        Authorization: localStorage.getItem('accessToken') && `Bearer ${localStorage.getItem('accessToken')}`
        }
    }).then(response => {
        return response
    })
}