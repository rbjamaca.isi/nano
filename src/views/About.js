// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Utils
import { isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import { Card, Button } from 'reactstrap'
import Footer from '@src/@core/layouts/components/footer'
import Header from '@src/views/misc/Header'
import { useHistory } from 'react-router-dom'

const About = () => {
  const history = useHistory()
  return (

    <Fragment>
      {getUserData()?.role !== 'Admin' &&
        <Card>
          <Header />
        </Card>
      }
      <div class='container'>
        <Card className='px-5'>
          <section className='pt-5'>
            <div class="row">
              <div class="col-12 col-md-10 col-lg-9 mx-auto">
                <div class="row mb-5 text-center">
                  <div class="col-12 col-lg-6 mx-auto">
                    <h1 class="mb-3 fs-1 fw-bold">
                      <span class="text-primary">About Us</span>
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="container">
              <div class="row">
                <div class="col d-flex">
                  <div class="my-auto">
                    <h2 class="mb-4 fs-1 fw-bold">DIT.ADS History</h2>
                    <p class="mb-4 fw-bold lh-lg d-flex align-items-center justify-content-center text-justify">
                      Neilson and Wilson made up the name zas Digital Institute Training and Development Services (DIT.ADS) on June 19, 2019. The concept of the business started when Neilson, the son, and Wilson, the father attended the Seminar on Research held at Davao City. The two  (Neilson and Wilson) together with the support of the family members envisioned to help the students and teachers in the city of Cagayan de Oro and those who find difficulty in engaging research especially when a research paper required the use of descriptive and inferential statistical tools. The pioneered also anticipated that in the coming years digital training for research will be in demand and could be easily learned by anybody interested in research through the use of Statistical Package for the Social Sciences (SPSS) software. SPSS is software for editing and analyzing all sorts of data.
                    </p>
                    <p class="mb-4 fw-bold lh-lg d-flex align-items-center justify-content-center text-justify ">
                      The pioneering leaders of the business firmly believed in the concept that “Research and development (R&D) is a valuable tool for growing and improving the individual and the business. R&D involves researching the market and the customer needs and developing new and improved products and services to fit these needs. Businesses that have an R&D strategy have a greater chance of success than businesses that don't. An R&D strategy can lead to innovation and increased productivity and can boost the business's competitive advantage.”
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="container">
              <div class="row">
                <div class="col d-flex">
                  <div class="my-auto">
                    <h2 class="mb-4 fs-1 fw-bold">Business Profile </h2>
                    <p class="mb-4 fw-bold lh-lg d-flex align-items-center justify-content-center text-justify">
                      Zas DIGITAL INSTITUTE TRAINING AND DEVELOPMENT SERVICES (DIT.ADS) provides Management, Marketing, Finance, Accounting, Real Estate, Environmental Planning, Production Planning, Data Analysis, Data Interpretation, Academic and Business Research, Hospitality consultancy, and Training services. It also offers customized training sessions to students, managers, and company owners.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="container">
              <div class="row">
                <div class="col d-flex">
                  <div class="my-auto">
                      <h2 class="mb-4 fs-1 fw-bold">Mission</h2>
                      <p class="mb-2 fw-bold lh-lg d-flex align-items-center justify-content-center text-justify">
                        1. To provide customized training services in the fields of Management, Marketing, Finance, Accounting, Real Estate, Environmental Planning, Production Planning, Data Analysis, and Data Interpretation, Academic and Business Research, Hospitality consultancy, and Training services.
                      </p>
                      <p class="mb-4 lh-lg fw-bold">
                        2. To empower the firms and workers in their respective fields and to enhance their potentials that add value to their endeavors and businesses.
                      </p>
                    </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="container">
            <div class="row">
                <div class="col d-flex">
                  <div class="my-auto">
                      <h2 class="mb-4 fs-1 fw-bold">Vision</h2>
                      <p class="mb-2 fw-bold lh-lg d-flex align-items-center justify-content-center text-justify">
                        “To be the leading research institute in Mindanao by 2025.” <br /><br /> To create dependable management, marketing, finance, accounting, hospitality consultancy business, and training services that provide employment and business opportunities to common and highly respected people in the community.
                      </p>
                    </div>
                  </div>
                </div>
            </div>
          </section>
          <section>
            <div class="container">
              <div class="row">
                <div class="col d-flex">
                  <div class="my-auto">
                    <p class="mb-4 fw-bold lh-lg d-flex align-items-center justify-content-center">
                      Currently, the business is celebrating its two years of operations and survival in serving the students, teachers, and employees working in the private sector in the city even the time of pandemic wherein virtual training is the accepted mode in the area through the use of Zoom and Google Meet.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </Card>
        <div class="text-center"><a class="btn btn-primary mb-2" onClick={() => history.goBack()}>Show Trainings</a></div>
        {getUserData()?.role !== 'Admin' &&
          <Footer />
        }
      </div>
    </Fragment>
  )
}

export default About
