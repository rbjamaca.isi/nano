// ** React Imports
import { Fragment, memo, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import Moment from 'react-moment'
import {
  Table,
  Col,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  UncontrolledTooltip,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap'
import {
  Eye,
  TrendingUp,
  Send,
  MoreVertical,
  Download,
  Edit,
  Trash,
  Copy,
  Check,
  Save,
  ArdataDownCircle,
  Info,
  PieChart
} from 'react-feather'
import Avatar from '@components/avatar'
import cmtImg from '@src/assets/images/banner/banner-12.jpg'

// ** Custom Components
import Sidebar from '@components/sidebar'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import { Slide, toast } from 'react-toastify'
import moment from 'moment'

// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='toast-title font-weight-bold'>Success!</h6>
      </div>
      <small className='text-muted'>Just now</small>
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const AttendeesTable = ({ reload, setReload, userData, list, loading }) => {
  // ** States
  const { register, errors, handleSubmit, trigger } = useForm()
  const [open, setOpen] = useState(false)
  const [basicModal, setBasicModal] = useState(false)
  const [data, setData] = useState(null)
  let total = 0
  let attendees = 0

  // ** Form Submit
  const onSubmit = async (status, id) => {
    await API_ACTION(status, `api/admin/attendee/status/${id}`, 'put')
    toast.success(
      <ToastContent msg={`Request ${status.status}`} />,
      { transition: Slide, hideProgressBar: false, autoClose: 2000 }
    )
    setReload(!reload)
    setOpen(!open)
  }

  const handleOpenProof = (data) => {
    console.log(data)
    setData(data)
    setOpen(!open)
  }
  

  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  return (
    <Card>
      <Table responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Full Name</th>
            <th>Membership Type</th>
            <th>Fee</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {list && list.map(data => {
            total = total + (data.status !== 'Rejected' ? data.payment_amount : 0)
            attendees = attendees + (data.status !== 'Rejected' ? 1 : 0)
            return (
            <tr key={data.id}>
              <td>
                {data.id}
              </td>
              <td>
                {data.firstname} {data.lastname}
              </td>
              <td>
                {data.is_premium ? data.type : 'Non-Premium'}
              </td>
              <td>
                {data.payment_amount || 'Free'}
              </td>
              
              <td>
                {data.attend_status}
              </td>
              <td>
                <div className='column-action d-flex align-items-center'>
                  <Button.Ripple
                    onClick={() => handleOpenProof(data)} className="btn-icon" color='flat-primary'>
                    <Eye size={17} className='mx-1' />
                  </Button.Ripple>
                  {/* <UncontrolledTooltip placement='top' target={`pw-tooltip-${data.id}`}>
                    Preview Account
                  </UncontrolledTooltip>
                  <UncontrolledDropdown>
                    <DropdownToggle tag='span'>
                      <MoreVertical size={17} className='cursor-pointer' />
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem tag='a' href='/' className='w-100' onClick={e => e.preventDefault()}>
                        <Download size={14} className='mr-50' />
                        <span className='align-middle'>Download</span>
                      </DropdownItem>
                      <DropdownItem tag={Link} to={`/apps/invoice/edit/${data.id}`} className='w-100'>
                        <Edit size={14} className='mr-50' />
                        <span className='align-middle'>Edit</span>
                      </DropdownItem>
                      <DropdownItem
                        tag='a'
                        href='/'
                        className='w-100'
                        onClick={e => {
                          e.preventDefault()
                          store.dispatch(deleteInvoice(data.id))
                        }}
                      >
                        <Trash size={14} className='mr-50' />
                        <span className='align-middle'>Delete</span>
                      </DropdownItem>
                      <DropdownItem tag='a' href='/' className='w-100' onClick={e => e.preventDefault()}>
                        <Copy size={14} className='mr-50' />
                        <span className='align-middle'>Duplicate</span>
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown> */}
                </div>
              </td>
            </tr>
          ) 
          })}
        </tbody>
        <tfoot>
          <tr>
            <td><h5><strong>Attendees:</strong></h5></td>
            <td><h5><strong>{attendees}</strong></h5></td>
            <td><h5><strong>Collected:</strong></h5></td>
            <td><h5><strong>{total}</strong></h5></td>
            <td></td>
            <td></td>
          </tr>
        </tfoot>
      </Table>
      <Sidebar
        size='lg'
        open={open}
        title='Proof of Payment'
        headerClassName='mb-1'
        contentClassName='p-0'
        toggleSidebar={toggleSidebar}
      >
        <Form onSubmit={handleSubmit(onSubmit)}>
        <FormGroup row>
          <Label className='form-label'>
            <h4 className='font-weight-bolder'>Training Details</h4>
          </Label>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Training</h6>
            </Label>
              <Col>
            {data?.title}
              </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Start</h6>
            </Label>
              <Col>
              <Moment format="LLL">
                  {data?.schedule_start}
                </Moment>
              </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>End</h6>
            </Label>
              <Col>
                <Moment format="LLL">
                  {data?.schedule_end}
              </Moment>
              </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Fee</h6>
            </Label>
              <Col>
                  {data?.payment_amount}
              </Col>
          </FormGroup>
          <hr></hr>
        <FormGroup row>
          <Label className='form-label'>
            <h4 className='font-weight-bolder'>Payment Details</h4>
          </Label>
          </FormGroup>
          {data?.payment_method !== null ? <>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Payment Gateway</h6>
            </Label>
              <Col>
            {data?.payment_method}
              </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Amount</h6>
            </Label>
            <Col>
              {data?.payment_amount}
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Reference Number</h6>
            </Label>
              <Col>
              {data?.ref_no}
              </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
            <h6>Proof of Payment:</h6>
            </Label>
          </FormGroup>
          <img src={`/storage/images/training/proof/${data?.photo}`} width='200px' />
          </> : "Free"
          }

          <FormGroup className='d-flex flex-wrap mt-2'>
            <Button className='mr-1' color='primary' onClick={() => onSubmit({status: 'Approved', training_id: data?.training_id}, data?.user_id)}>
              Approve
            </Button>
            <Button className='mr-1' color='danger' onClick={() => onSubmit({status: 'Rejected', training_id: data?.training_id}, data?.user_id)}>
              Reject
            </Button>
            <Button color='secondary' onClick={() => setOpen(false)} outline>
              Cancel
            </Button>
          </FormGroup>
        </Form>
      </Sidebar>

      <Modal isOpen={basicModal} toggle={() => setBasicModal(!basicModal)} className='modal-xl'>
        <ModalHeader toggle={() => setBasicModal(!basicModal)}>Proof of Payments</ModalHeader>
        <ModalBody>
        <img src={`/storage/images/training/proof/${data?.proof}`} width='700' />
        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={() => setBasicModal(!basicModal)}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </Card>
  )
}

export default memo(AttendeesTable)
