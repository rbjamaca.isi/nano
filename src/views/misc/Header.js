// ** React Imports
import { Fragment, useState } from 'react'
import { useHistory } from 'react-router-dom'

// ** Utils
import { isUserLoggedIn } from 'utility/Utils'

// ** Third Party Components
import { Button } from 'reactstrap'
import Sidebar from '@components/sidebar'
import Register from '@src/views/Shared-Register'
import UserDropdown from '@src/@core/layouts/components/navbar/UserDropdown'

const Header = () => {
    // ** States
    const history = useHistory()
    const [open, setOpen] = useState(false)

    // ** Function to toggle sidebar
    const toggleSidebar = () => setOpen(!open)
    const logo = require('@src/assets/images/logo/logo.png').default
    return (
        <Fragment>
            <section className='mx-1'>
                <nav class="navbar">
                    <a class="navbar-brand" href="#"><img src={logo} alt="" width="60" /></a>
                    Changing Lives. Building Knowledge. Making Success.
                    <ul className='navbar-nav align-items-center ml-auto '>
                        {isUserLoggedIn() ? <UserDropdown type='member' /> : localStorage.getItem('registration') ? <></> : <div>
                            <Button color="secondary" onClick={toggleSidebar} >Create New Account</Button> &nbsp;
                            <Button color="primary" onClick={() => history.push('/login')} >Login</Button>
                        </div>}
                        {/* <div>
                            <Button color="secondary" onClick={toggleSidebar} >Create New Account</Button> &nbsp;
                            <Button color="primary" onClick={() => history.push('/login')} >Login</Button>
                        </div> */}
                    </ul>
                </nav>
            </section>
            <Sidebar
                size='lg'
                open={open}
                title='Create New Account'
                headerClassName='mb-1'
                contentClassName='p-0'
                toggleSidebar={toggleSidebar}
            >
                <Register />
            </Sidebar>
        </Fragment>
    )
}

export default Header
