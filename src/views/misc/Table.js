// ** React Imports
import React from 'react'

// ** Third Party Components
import DataTable from 'react-data-table-component'

// ** Icons
import { ChevronDown } from 'react-feather'

// **  Styles

export default function Table({
    filteredData,
    searchSubscription,
    data,
    searchStatus,
    searchName,
    searchMode,
    columns,
    currentPage
}) {
    // ** Table data to render
    const dataToRender = () => {
        if (
            searchName?.length ||
            searchStatus?.length ||
            searchSubscription?.length ||
            searchMode?.length
        ) {
            return filteredData
        } else {
            return data
        }
    }

    return (
        <>
            <div className="datatable flex flex-col min-h-full overflow-hidden">
                <DataTable
                    noHeader
                    pagination
                    highlightOnHover
                    bordered
                    columns={columns}
                    paginationPerPage={10}
                    className='react-dataTable'
                    sortIcon={<ChevronDown size={10} />}
                    paginationDefaultPage={currentPage + 1}
                    data={dataToRender()}
                />
            </div>
        </>

    )
}

