// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import {
  Eye, Check
} from 'react-feather'
import {
  Card, CardHeader, Badge, CardBody, Input, Row, Col, Label, CustomInput, Button, Table as Tble,
  Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Form
} from 'reactstrap'

// ** Tables
import Table from '@src/views/misc/Table'

// ** Custom Components
import Sidebar from '@components/sidebar'
import Moment from 'react-moment'
import jsPDF from 'jspdf'
import 'jspdf-autotable'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'

// ** Toastify
const ToastContent = ({ msg, role }) => (
  <Fragment>
    <div className='toastify-header'>
      {/* <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Welcome, {name}</h6>
      </div> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const ReportAttendees = () => {
  // ** State
  const history = useHistory()
  const [loading, setLoading] = useState(null)
  const [reload, setReload] = useState(false)

  const { register, errors, handleSubmit, trigger } = useForm()
  const [data, setData] = useState([])
  const [trainings, setTrainings] = useState([])
  const [item, setItem] = useState(null)
  const [filter, setFilter] = useState(false)
  const [filteredData, setFilteredData] = useState([])
  const [currentPage, setCurrentPage] = useState(0)
  const [searchName, setSearchName] = useState('')
  const [searchStatus, setSearchStatus] = useState('')
  const [searchSubscription, setSearchSubscription] = useState('')
  const [searchMode, setSearchMode] = useState('')
  const [currentRole, setCurrentRole] = useState({ value: 'all', label: 'All' })
  const [trainingTitle, setTrainingTitle] = useState('')

  let total = 0
  let attendees = 0

  const [open, setOpen] = useState(false)
  const [basicModal, setBasicModal] = useState(false)

  // ** Vars
  const { id } = useParams()

  // ** Get trainings
  const getTrainingsData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/admin/trainings`)
      setLoading(false)
      setTrainings(response.data.data.trainings)
    } catch (err) {
      console.log(err)
    }
  }

  // ** Get attendees
  const getAttendeesData = async (training_id) => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/admin/attendees/${training_id || 1}`)
      setLoading(false)
      setData(response.data.data.users)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    getTrainingsData()
    getAttendeesData()
  }, [reload])

  // ** Form Submit
  const onSubmit = async (status, id) => {
    await API_ACTION(status, `api/admin/attendee/status/${id}`, 'put')
    toast.success(
      <ToastContent msg={`Request ${status.status}`} />,
      { transition: Slide, hideProgressBar: false, autoClose: 2000 }
    )
    setReload(!reload)
    setOpen(!open)
  }

  const handleOpenProof = (data) => {
    setItem(data)
    setOpen(!open)
  }


  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  // ** Function to handle name filter
  const handleNameFilter = e => {
    const value = e.target.value

    const index = e.target.selectedIndex
    const optionElement = e.target.childNodes[index]
    const optionElementTitle = optionElement.getAttribute('title')

    setFilter(true)
    getAttendeesData(value)
    setTrainingTitle(optionElementTitle)
  }

  // ** Function to handle subscription filter
  const handleSubscriptionFilter = (e) => {
    const value = e.target.value
    if (value === 'All') {
      setSearchName('')
      setFilteredData([...data])
      setSearchStatus('')
      setSearchSubscription('All')
    } else {
      setFilter(true)
      let updatedData = []
      const dataToFilter = () => {
        if (searchStatus.length || searchName.length || searchMode.length) {
          return filteredData
        } else {
          return data
        }
      }
      if (value) {
        updatedData = dataToFilter().filter(item => {
          const startsWith = item.is_premium === +value
          if (startsWith) {
            return startsWith
          } else return null
        })
        setSearchSubscription(value)
        setFilteredData([...updatedData])
      }
    }
  }

  // ** Function to handle status filter
  const handleStatusFilter = e => {
    const value = e.target.value
    if (value === 'All') {
      setSearchName('')
      setFilteredData([...data])
      setSearchStatus(value)
      setSearchMode('')
    } else {
      setFilter(true)
      let updatedData = []
      const dataToFilter = () => {
        if (searchSubscription.length || searchName.length || searchMode.length) {
          return filteredData
        } else {
          return data
        }
      }
      setSearchStatus(value)
      if (value.length) {
        updatedData = dataToFilter().filter(item => {
          const startsWith = item.status.toLowerCase().startsWith(value.toLowerCase())
          const includes = item.status.toLowerCase().includes(value.toLowerCase())
          if (startsWith) {
            return startsWith
          } else if (!startsWith && includes) {
            return includes
          } else return null
        })
        setFilteredData([...updatedData])
        setSearchStatus(value)
      }
    }
  }

  // ** Function to handle name filter
  const handleModeFilter = e => {
    setFilter(true)
    const value = e.target.value
    let updatedData = []
    const dataToFilter = () => {
      if (searchSubscription.length || searchName.length || searchStatus.length) {
        return filteredData
      } else {
        return data
      }
    }

    setSearchMode(value)
    if (value.length) {
      updatedData = dataToFilter().filter(item => {
        const startsWith = item.mode?.toLowerCase().startsWith(value.toLowerCase())
        const includes = item.mode?.toLowerCase().includes(value.toLowerCase())

        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData([...updatedData])
      setSearchMode(value)
    }
  }

  const statusObj = {
    Pending: 'light-warning',
    Active: 'light-success',
    Inactive: 'light-warning',
    Rejected: 'light-danger'
  }

  // ** Table Common Column
  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
      width: '5%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.id}
          </div>
        )
      }
    },
    {
      name: 'Full Name',
      selector: 'name',
      sortable: true,
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.firstname} {row.lastname}
          </div>
        )
      }
    },
    {
      name: 'Membership Type',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.is_premium ? row.type : 'Non-Premium'}
          </div>
        )
      }
    },

    {
      name: 'FEE',
      selector: 'fee',
      sortable: true,
      width: '10%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.payment_amount || 'Free'}
          </div>
        )
      }
    },
    {
      name: 'Payment Mode',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.mode ? row.mode : 'N/A'}
          </div>
        )
      }
    },
    {
      name: 'STATUS',
      selector: 'status',
      sortable: true,
      width: '10%',
      cell: row => {
        return (
          <Badge className='text-capitalize' color={statusObj[row.status]} pill>
            {row.status}
          </Badge>
        )
      }
    }
  ]

  const exportPDF = () => {
    const unit = "pt"
    const size = "A4" // Use A1, A2, A3 or A4
    const orientation = "portrait" // portrait or landscape

    const marginLeft = 40
    const marginTop = 40
    const doc = new jsPDF()

    doc.setFontSize(15)

    const title = trainingTitle
    const num_attendees = attendees.toString()
    const num_total = total.toString()
    const headers = [["ID", "FULL NAME", "EMAIL", "MEMBERSHIP TYPE", "FEE", "PAYMENT MODE", "STATUS"]]

    const table_data = !filter ?
      data.map(elt => [
        elt.id,
        elt.firstname + ' ' + elt.lastname,
        elt.email,
        elt.is_premium ? "Premium" : "Non-Premium",
        elt.payment_amount || 0,
        elt.mode || 'N/A',
        elt.status
      ])
      :
      // ** filteredData.map
      data.map(elt => [
        elt.id,
        elt.firstname + ' ' + elt.lastname,
        elt.email,
        elt.is_premium ? "Premium" : "Non-Premium",
        elt.payment_amount || 0,
        elt.mode || 'N/A',
        elt.status
      ])

    const content = {
      startY: 50,
      head: headers,
      body: table_data
    }

    doc.setFont("helvetica")
    doc.text(title, 105, 20, null, null, "center")

    doc.text("Total Number of Attendees: " + num_attendees, 105, 30, null, null, "center")
    doc.text("Amount Collected: " + num_total, 105, 40, null, null, "center")

    doc.autoTable(content)
    // doc.text(num_attendees, marginLeft, 40)
    doc.save(trainingTitle + ".pdf")
  }

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/attendees/view'> <h5 className="text-primary">List of Attendees</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <Card>
        <CardBody>
          <Row className='mt-1'>
            <Col sm='2'>
              Select Training Title
            </Col>
            <Col>
              <FormGroup>
                <Input className='w-full ' type='select' onChange={data => handleNameFilter(data)}>
                  {trainings && trainings.map((data, idx) => {
                    return (
                      <option key={idx} value={data.id} title={data.title}> {data.title} </option>
                    )
                  })}
                </Input>
              </FormGroup>
            </Col>
            {/* <Col md='2'>
              <Input className='w-full ' type='select' onChange={data => handleStatusFilter(data)}>
                <option value='All'>Select Status</option>
                <option value='All'>All</option>
                <option value='Pending'>Pending</option>
                <option value='Active'>Active</option>
                <option value='Inactive'>Inactive</option>
                <option value='Rejected'>Rejected</option>
              </Input>
            </Col>
            <Col md='3'>
              <Input className='w-full' type='select' onChange={data => handleSubscriptionFilter(data)}>
                <option value='All'>Select Subscription Type</option>
                <option value='All'>All</option>
                <option value={1}>Premium</option>
                <option value={0}>Non-Premium</option>
              </Input>
            </Col>
            <Col className='my-md-0 my-1'>
              <FormGroup>
                <Input id='mode' placeholder='Payment Mode' value={searchMode} onChange={handleModeFilter} />
              </FormGroup>
            </Col> */}
          </Row>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <div className='invoice-list-table-header w-100'>
            <Row>
              <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
                <div className='d-flex align-items-center mr-2'>
                  <Label for='rows-per-page'>Show</Label>
                  <CustomInput
                    className='form-control ml-50 pr-3'
                    type='select'
                    id='rows-per-page'
                  // onChange={handlePerPage}
                  >
                    <option value='10'>10</option>
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                  </CustomInput>
                </div>
              </Col>
              <Col lg='6' className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'>
                <Button.Ripple onClick={() => exportPDF()}>
                  Export PDF
                </Button.Ripple >
              </Col>
            </Row>
          </div>
        </CardHeader>
        <Row>
          <Col sm='12'>
            <Card>
              <Table
                filteredData={filteredData}
                data={data}
                searchName={searchName}
                searchStatus={searchStatus}
                searchSubscription={searchSubscription}
                searchMode={searchMode}
                columns={columns}
                currentPage={currentPage}
              />
              <Tble>
                {data && data.map(data => {
                  total = total + (data.status !== 'Rejected' ? data.payment_amount : 0)
                  attendees = attendees + (data.status !== 'Rejected' ? 1 : 0)
                })}
                <tfoot>
                  <tr>
                    <td><h5><strong>Attendees:</strong></h5></td>
                    <td><h5><strong>{attendees}</strong></h5></td>
                    <td><h5><strong>Collected:</strong></h5></td>
                    <td><h5><strong>{total}</strong></h5></td>
                    <td></td>
                    <td></td>
                  </tr>
                </tfoot>
              </Tble>
            </Card>
          </Col>
        </Row>
      </Card>
      <Sidebar
        size='lg'
        open={open}
        title='Proof of Payment'
        headerClassName='mb-1'
        contentClassName='p-0'
        toggleSidebar={toggleSidebar}
      >
        <Form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup row>
            <Label className='form-label'>
              <h4 className='font-weight-bolder'>Training Details</h4>
            </Label>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
              <h6>Training</h6>
            </Label>
            <Col>
              {item?.title}
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
              <h6>Start</h6>
            </Label>
            <Col>
              <Moment format="LLL">
                {item?.schedule_start}
              </Moment>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
              <h6>End</h6>
            </Label>
            <Col>
              <Moment format="LLL">
                {item?.schedule_end}
              </Moment>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label className='form-label'>
              <h6>Fee</h6>
            </Label>
            <Col>
              {item?.payment_amount}
            </Col>
          </FormGroup>
          <hr></hr>
          {item?.payment_method && <>
            <FormGroup row>
              <Label className='form-label'>
                <h4 className='font-weight-bolder'>Payment Details</h4>
              </Label>
            </FormGroup>
            <FormGroup row>
              <Label className='form-label'>
                <h6>Payment Gateway</h6>
              </Label>
              <Col>
                {item?.payment_method}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label className='form-label'>
                <h6>Amount</h6>
              </Label>
              <Col>
                {item?.payment_amount}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label className='form-label'>
                <h6>Reference Number</h6>
              </Label>
              <Col>
                {item?.ref_no}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label className='form-label'>
                <h6>Proof of Payment:</h6>
              </Label>
            </FormGroup>
            <img src={`/storage/images/training/proof/${item?.photo}`} width='200px' />
          </>}

          <FormGroup className='d-flex flex-wrap mt-2'>
            <Button className='mr-1' color='primary' onClick={() => onSubmit({ status: 'Approved', training_id: item?.training_id }, item?.user_id)}>
              Approve
            </Button>
            <Button className='mr-1' color='danger' onClick={() => onSubmit({ status: 'Rejected', training_id: item?.training_id }, item?.user_id)}>
              Reject
            </Button>
            <Button color='secondary' onClick={() => setOpen(false)} outline>
              Cancel
            </Button>
          </FormGroup>
        </Form>
      </Sidebar>

      <Modal isOpen={basicModal} toggle={() => setBasicModal(!basicModal)} className='modal-xl'>
        <ModalHeader toggle={() => setBasicModal(!basicModal)}>Proof of Payments</ModalHeader>
        <ModalBody>
          <img src={`/storage/images/training/proof/${data?.proof}`} width='700' />
        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={() => setBasicModal(!basicModal)}>
            Close
          </Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  )
}


export default ReportAttendees
