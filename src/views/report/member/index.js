// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Third Party Components
import {
  Card, CardHeader, CardBody, Input, Row, Col, Label, CustomInput, Button,
  Breadcrumb, BreadcrumbItem, UncontrolledTooltip, Badge, FormGroup, Table as Tble
} from 'reactstrap'
import { Eye } from 'react-feather'
import Moment from 'react-moment'
import jsPDF from 'jspdf'
import 'jspdf-autotable'

// ** Tables
import Table from '@src/views/misc/Table'

// ** Store & Actions
import { API_RETRIEVE } from '@src/data/api'

const ReportMembers = () => {
  // ** State
  const [loading, setLoading] = useState(null)
  const history = useHistory()

  const [data, setData] = useState([])
  const [filteredData, setFilteredData] = useState([])
  const [filter, setFilter] = useState(false)
  const [currentPage, setCurrentPage] = useState(0)
  const [searchName, setSearchName] = useState('')
  const [searchStatus, setSearchStatus] = useState('')
  const [searchSubscription, setSearchSubscription] = useState('')
  const [searchMode, setSearchMode] = useState('')
  const [currentSubscription, setcurrentSubscription] = useState({ value: 'all', label: 'All' })
  let userData

  //** User data
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      userData = JSON.parse(localStorage.getItem('userData'))
    } else {
      history.push('/login')
    }
  }, [])

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/admin`)
      setLoading(false)
      setData(response.data.data.users)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  // ** Filter options
  const subscriptionValue = [
    { value: 'all', label: 'All' },
    { value: 'Premium', label: 'Premium' },
    { value: 'Non-Premuim', label: 'Non-Premuim' }
  ]

  // ** Function to handle name filter
  const handleNameFilter = e => {
    setFilter(true)
    const value = e.target.value
    let updatedData = []
    const dataToFilter = () => {
      if (searchStatus.length || searchSubscription.length || searchMode.length) {
        return filteredData
      } else {
        return data
      }
    }

    setSearchName(value)
    if (value.length) {
      updatedData = dataToFilter().filter(item => {
        const fullname = item.firstname + ' ' + item.lastname
        const startsWith = fullname.toLowerCase().startsWith(value.toLowerCase())
        const includes = fullname.toLowerCase().includes(value.toLowerCase())

        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData([...updatedData])
      setSearchName(value)
    }
  }

  // ** Function to handle subscription filter
  const handleSubscriptionFilter = (e) => {
    const value = e.target.value
    if (value === 'All') {
      setSearchName('')
      setFilteredData([...data])
      setSearchStatus('')
      setSearchSubscription('All')
    } else {
      setFilter(true)
      let updatedData = []
      const dataToFilter = () => {
        if (searchStatus.length || searchName.length || searchMode.length) {
          return filteredData
        } else {
          return data
        }
      }
      if (value) {
        updatedData = dataToFilter().filter(item => {
          const startsWith = item.is_premium === +value
          if (startsWith) {
            return startsWith
          } else return null
        })
        setSearchSubscription(value)
        setFilteredData([...updatedData])
      }
    }
  }

  // ** Function to handle status filter
  const handleStatusFilter = e => {
    const value = e.target.value
    if (value === 'All') {
      setSearchName('')
      setFilteredData([...data])
      setSearchStatus(value)
      setSearchMode('')
    } else {
      setFilter(true)
      let updatedData = []
      const dataToFilter = () => {
        if (searchSubscription.length || searchName.length || searchMode.length) {
          return filteredData
        } else {
          return data
        }
      }
      setSearchStatus(value)
      if (value.length) {
        updatedData = dataToFilter().filter(item => {
          const startsWith = item.status.toLowerCase().startsWith(value.toLowerCase())
          const includes = item.status.toLowerCase().includes(value.toLowerCase())
          if (startsWith) {
            return startsWith
          } else if (!startsWith && includes) {
            return includes
          } else return null
        })
        setFilteredData([...updatedData])
        setSearchStatus(value)
      }
    }
  }

  // ** Function to handle name filter
  const handleModeFilter = e => {
    setFilter(true)
    const value = e.target.value
    let updatedData = []
    const dataToFilter = () => {
      if (searchSubscription.length || searchName.length || searchStatus.length) {
        return filteredData
      } else {
        return data
      }
    }

    setSearchMode(value)
    if (value.length) {
      updatedData = dataToFilter().filter(item => {
        const startsWith = item.mode?.toLowerCase().startsWith(value.toLowerCase())
        const includes = item.mode?.toLowerCase().includes(value.toLowerCase())

        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData([...updatedData])
      setSearchMode(value)
    }
  }

  const statusObj = {
    Pending: 'light-warning',
    Active: 'light-success',
    Inactive: 'light-danger',
    Rejected: 'light-danger'
  }

  // ** Table Common Column
  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
      width: '5%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.id}
          </div>
        )
      }
    },
    {
      name: 'Full Name',
      selector: 'name',
      sortable: true,
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.firstname} {row.lastname}
          </div>
        )
      }
    },
    {
      name: 'Email',
      selector: 'email',
      sortable: true,
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.email}
          </div>
        )
      }
    },
    {
      name: 'Date Registered',
      selector: 'created_at',
      sortable: true,
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            <Moment format="LL">
              {row.created_at}
            </Moment>
          </div>
        )
      }
    },
    {
      name: 'STATUS',
      selector: 'status',
      sortable: true,
      width: '10%',
      cell: row => {
        return (
          <Badge className='text-capitalize' color={statusObj[row.status]} pill>
            {row.status}
          </Badge>
        )
      }
    },
    {
      name: 'FEE',
      selector: 'fee',
      sortable: true,
      width: '10%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.payment_amount || 0}
          </div>
        )
      }
    },
    {
      name: 'Subscription',
      sortable: true,
      cell: row => {
        return (
          <Badge className='text-capitalize' color={row.is_premium ? 'light-success' : 'light-warning'} pill>
            {row.is_premium ? "Premium" : "Non-Premium"}
          </Badge>
        )
      }
    },
    {
      name: 'Payment Mode',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.mode ? row.mode : 'N/A'}
          </div>
        )
      }
    },
    {
      name: 'ACTIONS',
      width: '10%',
      cell: row => {
        return (
          <div className='d-flex'>
            <div className='column-action d-flex align-items-center'>
              <Link
                to={(`/admin/member/${row.id}`)}
                id={`send-tooltip-${row.id}`}>
                <Eye size={17} />
              </Link>
              <UncontrolledTooltip placement='top' target={`send-tooltip-${row.id}`}>
                Preview Account
              </UncontrolledTooltip>
            </div>
          </div>
        )
      }
    }
  ]

  const exportPDF = () => {
    const unit = "pt"
    const size = "A4" // Use A1, A2, A3 or A4
    const orientation = "landscape" // portrait or landscape

    const marginLeft = 40
    const doc = new jsPDF(orientation, unit, size)

    doc.setFontSize(15)

    const title = "List of Members"
    const headers = [["ID", "FULL NAME", "EMAIL", "DATE REGISTERED", "STATUS", "FEE", "SUBSCRIPTION", "PAYMENT MODE"]]

    const table_data = !filter ?
      data.map(elt => [
        elt.id,
        elt.firstname + ' ' + elt.lastname,
        elt.email,
        new Date(elt.created_at).toLocaleDateString(),
        elt.status,
        elt.payment_amount || 0,
        elt.is_premium ? "Premium" : "Non-Premium",
        elt.mode || 'N/A'
      ])
      :
      filteredData.map(elt => [
        elt.id,
        elt.firstname + ' ' + elt.lastname,
        elt.email,
        new Date(elt.created_at).toLocaleDateString(),
        elt.status,
        elt.payment_amount || 0,
        elt.is_premium ? "Premium" : "Non-Premium",
        elt.mode || 'N/A'
      ])

    const content = {
      startY: 50,
      head: headers,
      body: table_data
    }

    doc.text(title, marginLeft, 40)
    doc.autoTable(content)
    doc.save("report.pdf")
  }

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/members/view'> <h5 className="text-primary">List of Members</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <Card>
        <CardHeader>
          Search Filter
        </CardHeader>
        <CardBody>
          <Row>
            {/* <Col md='4'>
            <Select
              isClearable={false}
              className='react-select'
              classNamePrefix='select'
              options={subscriptionValue}
              value={currentSubscription}
              onChange={data => {
                setcurrentSubscription(data)
                handleSubscriptionFilter(data)
              }}
            />
          </Col> */}
            <Col className='my-md-0 my-1'>
              <FormGroup>
                <Input id='name' placeholder='Search Name' value={searchName} onChange={handleNameFilter} />
              </FormGroup>
            </Col>
            <Col md='4'>
              <Input className='w-full ' type='select' onChange={data => handleStatusFilter(data)}>
                <option value='All'>Select Status</option>
                <option value='All'>All</option>
                <option value='Pending'>Pending</option>
                <option value='Active'>Active</option>
                <option value='Inactive'>Inactive</option>
                <option value='Rejected'>Rejected</option>
              </Input>
            </Col>
            <Col md='4'>
              <Input className='w-full' type='select' onChange={data => handleSubscriptionFilter(data)}>
                <option value='All'>Select Subscription Type</option>
                <option value='All'>All</option>
                <option value={1}>Premium</option>
                <option value={0}>Non-Premium</option>
              </Input>
            </Col>
            <Col className='my-md-0 my-1'>
              <FormGroup>
                <Input id='mode' placeholder='Payment Mode' value={searchMode} onChange={handleModeFilter} />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <div className='invoice-list-table-header w-100'>
            <Row>
              <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
                <div className='d-flex align-items-center mr-2'>
                  <Label for='rows-per-page'>Show</Label>
                  <CustomInput
                    className='form-control ml-50 pr-3'
                    type='select'
                    id='rows-per-page'
                  // onChange={handlePerPage}
                  >
                    <option value='10'>10</option>
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                  </CustomInput>
                </div>
                <div className='float-right'></div>
              </Col>
              <Col lg='6' className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'>
                <Button.Ripple onClick={() => exportPDF()}>
                  Export PDF
                </Button.Ripple >
              </Col>
            </Row>
          </div>
        </CardHeader>
        <Row>
          <Col sm='12'>
            <Card>

              <Table
                filteredData={filteredData}
                data={data}
                searchName={searchName}
                searchStatus={searchStatus}
                searchSubscription={searchSubscription}
                searchMode={searchMode}
                columns={columns}
                currentPage={currentPage}
              />
              <Tble>
                <tbody>
                  <tr>
                    <th width="15%"></th>
                    <th width="15%"></th>
                    <th width="20%"></th>
                    <th width="20%"><h6><strong>Total Collected: {searchStatus.length > 0 || searchName.length > 0 || searchSubscription.length ? filteredData.filter(r => r.status !== 'Inactive').reduce((prev, next) => prev + next.payment_amount, 0) : data.filter(r => r.status !== 'Inactive').reduce((prev, next) => prev + next.payment_amount, 0)}</strong></h6></th>
                    <th><h6><strong>Total Members: {searchStatus.length > 0 || searchName.length > 0 || searchSubscription.length ? filteredData?.filter(r => r.status !== 'Inactive').length : data?.filter(r => r.status !== 'Inactive').length}</strong></h6></th>
                    <th></th>
                    <th></th>
                  </tr>
                </tbody>
              </Tble>
            </Card>
          </Col>
        </Row>
      </Card>
    </Fragment>
  )
}


export default ReportMembers
