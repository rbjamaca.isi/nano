// ** React Imports
import React, { useRef, useState, useEffect, Fragment } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Third Party Components
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { toast, Slide } from 'react-toastify'

// ** Pages
import Wizard from '@components/wizard'
import Address from './steps-with-validation/Address'
import ProofPayments from './steps-with-validation/ProofPayments'
import PersonalInfo from './steps-with-validation/PersonalInfo'
import AccountDetails from './steps-with-validation/AccountDetails'
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import { getUserData } from 'utility/Utils'
import { useDispatch } from 'react-redux'
import { handleLogin, handleLogout } from 'redux/actions/auth'
import Avatar from '@components/avatar'
import { Check, X, AlertTriangle, Info, XOctagon } from 'react-feather'


// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='text-success ml-50 mb-0'>Successful!</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const ToastContentError = ({ title, msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='danger' icon={<XOctagon size={12} />} />
        <h6 className='toast-title ml-50 mb-0'>{title}</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const MemberAdd = () => {
  // ** States
  const dispatch = useDispatch()
  const [stepper, setStepper] = useState(null)
  const ref = useRef(null)
  const [formdata, setFormdata] = useState([])
  const [premium, setPremium] = useState(null)
  const history = useHistory()

  // ** Vars
  const { id } = getUserData()
// Form

  const handleFormSubmit = (props) => {
    const form_data = new FormData()

    for (const key in formdata) {
        form_data.append(key, formdata[key])
    }

    if (props.mode) {
      form_data.append('proof', props.proof)
      form_data.append('mode', props.mode)
      form_data.append('reference', props.reference)
    }

    API_ACTION(form_data, `/api/member/details/${id}`).then(res => {
      toast.success(
        <ToastContent msg="Successfully registered. Please login again." />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      dispatch(handleLogout())
      history.push('/login')
    }).catch(err => {
      console.log(err.response)
      if (err.response?.data?.errors?.reference) {
        toast.error(
          <ToastContentError title="Invalid Reference No." msg="The reference number you provided is not valid" />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      } else {
        toast.error(
          <ToastContentError title="Error" msg="An error has occured. Please try again later." />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      }
      
    })
  }

  // useEffect(() => {
  //   console.log(premium)
  // }, [premium])

  const steps = [
    {
      id: 'personal-info',
      title: 'Personal Info',
      subtitle: 'Add Personal Info',
      content: <PersonalInfo setFormdata={setFormdata} stepper={stepper} type='wizard-horizontal' />
    },
    {
      id: 'step-address',
      title: 'Contact Info',
      subtitle: 'Add Contact Info',
      content: <Address setFormdata={setFormdata} stepper={stepper} handleFormSubmit={handleFormSubmit} type='wizard-horizontal' />
    }
  ]

  return (
    <>
      <div className='horizontal-wizard'>
        <Wizard instance={el => setStepper(el)} ref={ref} steps={steps} />
      </div>
    </>
  )
}

export default MemberAdd
