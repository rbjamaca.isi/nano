import { Fragment, useState, useEffect } from 'react'
import classnames from 'classnames'
import { isObjEmpty } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { Label, FormGroup, Row, Col, Button, Form, Input } from 'reactstrap'

const Address = ({ stepper, setFormdata, handleFormSubmit }) => {
  const { register, errors, handleSubmit, trigger } = useForm()
  
  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      handleFormSubmit({...data})
    }
  }


  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Contact Info</h5>
        <small>Add Contact Information.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
      <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='street'>
             Street
            </Label>
            <Input
              type='text'
              name='street'
              id='street'
              placeholder='Street'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['street'] })}
            />
            </FormGroup>
              <FormGroup tag={Col}>
            <Label className='form-label' for='barangay'>
             Barangay
            </Label>
            <Input
              type='text'
              name='barangay'
              id='barangay'
              placeholder='Barangay'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['barangay'] })}
            />
          </FormGroup>
        </Row>
        <Row>
        <FormGroup tag={Col}>
            <Label className='form-label' for='city'>
              City
            </Label>
            <Input
              type='text'
              name='city'
              id='city'
              placeholder='City'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['city'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='province'>
              Province
            </Label>
            <Input
              type='text'
              name='province'
              id='province'
              placeholder='Province'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['province'] })}
            />
            </FormGroup>
        </Row>
        <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='mobile'>
            Mobile Number
            </Label>
            <Input
              name='mobile'
              id='mobile'
              placeholder='Mobile Number'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['mobile'] })}
            />
          </FormGroup>
          {/* <FormGroup tag={Col}>
            <Label className='form-label' for='telephone'>
              Telephone
            </Label>
            <Input
              type='text'
              name='telephone'
              id='telephone'
              placeholder='Telephone'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['telephone'] })}
            />
          </FormGroup> */}
          <FormGroup tag={Col}>
            <Label className='form-label' for='postalcode'>
              Postal Code
            </Label>
            <Input
              type='number'
              name='postalcode'
              id='postalcode'
              placeholder='Postal Code'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['postalcode'] })}
            />
          </FormGroup>
        </Row>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev'>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
            <Button.Ripple type='submit' color='success' className='btn-submit'>
            Submit
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default Address
