// ** React Imports
import React, { useRef, useState, useEffect, Fragment } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Third Party Components
import {  Row, Col, Button, Card, CardBody, ListGroup, Badge, CardText, ListGroupItem} from 'reactstrap'
import { toast, Slide } from 'react-toastify'
import '@styles/base/pages/page-pricing.scss'

// ** Pages
import Wizard from '@components/wizard'
import Address from './steps-with-validation/Address'
import ProofPayments from './steps-with-validation/ProofPayments'
import PersonalInfo from './steps-with-validation/PersonalInfo'
import AccountDetails from './steps-with-validation/AccountDetails'
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import { getUserData, pesoSign } from 'utility/Utils'
import { useDispatch } from 'react-redux'
import { handleLogin, handleLogout } from 'redux/actions/auth'
import Avatar from '@components/avatar'
import { Check, X, AlertTriangle, Info, XOctagon, Coffee } from 'react-feather'
import PricingCards from './PricingCards'
import classnames from 'classnames'
import useJwt from '@src/auth/jwt/useJwt'
import axios from 'axios'
import { CustomToast } from 'utility/CustomToast'


// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='text-success ml-50 mb-0'>Successful!</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const ToastContentError = ({ title, msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='danger' icon={<XOctagon size={12} />} />
        <h6 className='toast-title ml-50 mb-0'>{title}</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const MemberAdd = () => {
  // ** States
  const dispatch = useDispatch()
  const [stepper, setStepper] = useState(null)
  const ref = useRef(null)
  const [formdata, setFormdata] = useState([])
  const [premium, setPremium] = useState(null)
  const history = useHistory()

  // ** Vars
  const {firstname, lastname, email} = useParams()
  

  // Form

  const handleFormSubmit = (props) => {
    const form_data = new FormData()

    for (const key in formdata) {
      form_data.append(key, formdata[key])
    }

    if (props.mode) {
      form_data.append('proof', props.proof)
      form_data.append('mode', props.mode)
      form_data.append('reference', props.reference)
      form_data.append('is_premium', true)
      form_data.append('payment_amount', formdata.type === "Regular" ? 250 : 150)
    } else {
      form_data.append('status', "Active")
    }

    form_data.append('firstname', firstname)
    form_data.append('lastname', lastname)
    form_data.append('email', email)

    if (props.street) {
      form_data.append('street', props.street)
      form_data.append('barangay', props.barangay)
      form_data.append('city', props.city)
      form_data.append('province', props.province)
      form_data.append('mobile', props.mobile)
      form_data.append('postalcode', props.postalcode)
    }

    // setValErrors({})
    axios.post(`https://api.email-validator.net/api/verify?EmailAddress=${email}&APIKey=ev-a4653e5496a3b877ebde76b4f12e98e3`).then(res => {
      if (res.data.ratelimit_remain === 0) {
        toast.error(
          <CustomToast title="Cannot validate email" msg='Sorry. We cannot validate your email at the moment. Please try again after a few hours' icon={<Coffee size={12} />} color="danger" />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      } else {
        if (res.data.status !== 200) {
          toast.error(
            <CustomToast title="Invalid email" msg='The email you provided is not valid. Please use an existing email.' icon={<Coffee size={12} />} color="danger" />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        } else {
          

    API_ACTION(form_data, `/api/member/details`).then(res => {
      if (res.data.data.is_premium) {
        toast.success(
          <ToastContent msg="Successfully registered. You will be informed through email when your membership is approved." />,
          { transition: Slide, hideProgressBar: true, autoClose: 5000 }
        )
      } else {
        toast.success(
          <ToastContent msg="Successfully registered. Please login again." />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      }
      
      dispatch(handleLogout())
      history.push('/login')
    }).catch(err => {
      console.log(err.response)
      if (err.response?.data?.errors?.reference) {
        toast.error(
          <ToastContentError title="Invalid Reference No." msg="The reference number you provided is not valid" />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      } else {
        toast.error(
          <ToastContentError title="Error" msg="An error has occured. Please try again later." />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      }
      
      })
      }
    }
  })

  }

  useEffect(() => {
    if (premium && premium !== "Non-Premium") {
      setFormdata(form => ({...form, type: premium}))
    }
  }, [premium])

  const steps = [
    {
      id: 'personal-info',
      title: 'Personal Info',
      subtitle: 'Add Personal Info',
      content: <PersonalInfo setFormdata={setFormdata} stepper={stepper} type='wizard-horizontal' />
    },
    {
      id: 'step-address',
      title: 'Contact Info',
      subtitle: 'Add Contact Info',
      content: <Address premium={premium} setFormdata={setFormdata} stepper={stepper} handleFormSubmit={handleFormSubmit} type='wizard-horizontal' />
    },
    {
      id: 'social-links',
      title: 'Proof of Payments',
      subtitle: 'Upload Proof of Payments',
      content: <ProofPayments formdata={formdata} setFormdata={setFormdata} handleFormSubmit={handleFormSubmit} stepper={stepper} type='wizard-horizontal' />
    }
  ]

  const nonPremiumSteps = [
    {
      id: 'personal-info',
      title: 'Personal Info',
      subtitle: 'Add Personal Info',
      content: <PersonalInfo setFormdata={setFormdata} stepper={stepper} type='wizard-horizontal' />
    },
    {
      id: 'step-address',
      title: 'Contact Info',
      subtitle: 'Add Contact Info',
      content: <Address premium={premium} setFormdata={setFormdata} stepper={stepper} handleFormSubmit={handleFormSubmit} type='wizard-horizontal' />
    }
  ]

  return (
    <>
      {premium === null ? (
      <Row className='pricing-card'>
        <Col className='mx-center' sm={{ offset: 1, size: 10 }} lg={{ offset: 1, size: 10 }} md='12'>
            <Row>
              <Col md='4' xs='12'>
              <Card
                className={classnames('text-center', {
                  [`basic-pricing`]: 'basic',
                  popular: false
                })}
              >
                <CardBody>
                  {false ? (
                    <div className='pricing-badge text-right'>
                      <Badge color='light-primary' pill>
                        Popular
                      </Badge>
                    </div>
                  ) : null}
                  <img className="mb-2 mt-5" src={require('@src/assets/images/illustration/Pot1.svg').default} alt='pricing svg' />
                  <h3>Non-Premium</h3>
                  <CardText>Get Started Quickly</CardText>
                  <div className='annual-plan'>
                    <div className='plan-price mt-2'>
                      <sup className='font-medium-1 font-weight-bold text-primary mr-25'>{pesoSign}</sup>
                      <span className={`pricing-standard-value font-weight-bolder text-primary`}>
                        0
                      </span>
                    </div>
                  </div>
                  <ListGroup tag='ul' className='list-group-circle text-left mb-2'>
                    <ListGroupItem tag='li'>
                        Join Free Trainings
                    </ListGroupItem>
                    <ListGroupItem tag='li'>
                        Avail Paid Trainings without Discount
                    </ListGroupItem>
                  </ListGroup>
                  <Button.Ripple
                    color="primary"
                    outline={true}
                    block
                    onClick={() => setPremium("Non-Premium")}
                  >
                    Click Here to Start
                  </Button.Ripple>
                </CardBody>
              </Card>
            </Col>

            <Col md='4' xs='12'>
              <Card
                className={classnames('text-center', {
                  [`standard-pricing`]: 'standard',
                  popular: true
                })}
              >
                <CardBody>
                  {true ? (
                    <div className='pricing-badge text-right'>
                      <Badge color='light-primary' pill>
                        Popular
                      </Badge>
                    </div>
                  ) : null}
                  <img className="mb-1" src={require('@src/assets/images/illustration/Pot2.svg').default} alt='pricing svg' />
                  <h3>Regular Premium</h3>
                  <CardText>More Free and Discounted Trainings</CardText>
                  <div className='annual-plan'>
                    <div className='plan-price mt-2'>
                      <sup className='font-medium-1 font-weight-bold text-primary mr-25'>{pesoSign}</sup>
                      <span className={`pricing-standard-value font-weight-bolder text-primary`}>
                        250
                      </span>
                    </div>
                  </div>
                  <ListGroup tag='ul' className='list-group-circle text-left mb-2'>
                    <ListGroupItem tag='li'>
                        Join Free Trainings
                    </ListGroupItem>
                    <ListGroupItem tag='li'>
                        Avail Paid Trainings with Discounts
                    </ListGroupItem>
                  </ListGroup>
                  <Button.Ripple
                    color="success"
                    outline={false}
                    block
                    onClick={() => setPremium("Regular")}
                  >
                    Click Here to Start
                  </Button.Ripple>
                </CardBody>
              </Card>
            </Col>

            <Col md='4' xs='12'>
              <Card
                className={classnames('text-center', {
                  [`standard-pricing`]: 'standard',
                  popular: true
                })}
              >
                <CardBody>
                  {true ? (
                    <div className='pricing-badge text-right'>
                      <Badge color='light-primary' pill>
                        Popular
                      </Badge>
                    </div>
                  ) : null}
                  <img className="mb-1" src={require('@src/assets/images/illustration/Pot2.svg').default} alt='pricing svg' />
                  <h3>Student Premium</h3>
                  <CardText>More Free and Discounted Trainings</CardText>
                  <div className='annual-plan'>
                    <div className='plan-price mt-2'>
                      <sup className='font-medium-1 font-weight-bold text-primary mr-25'>{pesoSign}</sup>
                      <span className={`pricing-standard-value font-weight-bolder text-primary`}>
                        150
                      </span>
                    </div>
                  </div>
                  <ListGroup tag='ul' className='list-group-circle text-left mb-2'>
                    <ListGroupItem tag='li'>
                        Join Free Trainings
                    </ListGroupItem>
                    <ListGroupItem tag='li'>
                        Avail Paid Trainings with Discounts
                    </ListGroupItem>
                  </ListGroup>
                  <Button.Ripple
                    color="success"
                    outline={false}
                    block
                    onClick={() => setPremium("Student")}
                  >
                    Click Here to Start
                  </Button.Ripple>
                </CardBody>
              </Card>
            </Col>

          </Row>
        </Col>
      </Row>
      ) : (
      <div className='horizontal-wizard'>
        <Wizard instance={el => setStepper(el)} ref={ref} steps={premium !== "Non-Premium" ? steps : nonPremiumSteps } />
      </div>
      )}
    </>
  )
}

export default MemberAdd
