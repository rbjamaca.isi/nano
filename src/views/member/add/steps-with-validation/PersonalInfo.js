import { Fragment } from 'react'
import Select from 'react-select'
import InputPasswordToggle from '@components/input-password-toggle'
import classnames from 'classnames'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { selectThemeColors, isObjEmpty } from '@utils'
import { Label, FormGroup, Row, Col, Button, Form, Input } from 'reactstrap'
import { validateText } from 'utility/Utils'

import '@styles/react/libs/react-select/_react-select.scss'
import { useParams } from 'react-router-dom'

const PersonalInfo = ({ stepper, setFormdata }) => {
  const { register, errors, handleSubmit, trigger, watch } = useForm()

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(form => ({...form, ...data}))
      stepper.next()
    }
  }

  const civilstatusOptions = [
    { value: 'Single', label: 'Single' },
    { value: 'Married', label: 'Married' },
    { value: 'Widowed', label: 'Widowed' },
    { value: 'Divorced', label: 'Divorced' },
    { value: 'Others', label: 'Others' }
  ]

  const {firstname, lastname, email} = useParams()

  const bloodtypeOptions = [
    { value: 'A', label: 'A' },
    { value: 'AB', label: 'AB' },
    { value: 'B', label: 'B' },
    { value: 'O', label: 'O' }
  ]

  const isFormValid = () => {
    return validateText(watch('dob')) 
    && validateText(watch('occupation'))
    && validateText(watch('password'))
    && validateText(watch('password_confirmation'))
  }

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Personal Info</h5>
        <small>Add Personal Information.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='firstname'>
              First Name
            </Label>
            <Input
              type='text'
              name='firstname'
              id='firstname'
              placeholder='First Name'
              value={firstname}
              inputProps={{readOnly:true}}
              // innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['firstname'] })}
            />
            </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='lastname'>
              Last Name
            </Label>
            <Input
              type='text'
              name='lastname'
              id='lastname'
              placeholder='Last Name'
              inputProps={{readOnly:true}}
              value={lastname}
              // innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['lastname'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='email'>
              Email
            </Label>
            <Input
              type='text'
              name='email'
              id='email'
              placeholder='Email'
              inputProps={{readOnly:true}}
              value={email}
              // innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['email'] })}
            />
          </FormGroup>
        </Row>
        {/* <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='height'>
              Height
            </Label>
            <Input
              type='number'
              name='height'
              id='height'
              placeholder='Height'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['height'] })}
            />
            </FormGroup>
              <FormGroup tag={Col}>
            <Label className='form-label' for='weight'>
              Weight
            </Label>
            <Input
              type='number'
              name='weight'
              id='weight'
              placeholder='Weight'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['weight'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='bloodtype'>
              Blood Type
            </Label>
            <Select
              theme={selectThemeColors}
              isClearable={false}
              id='bloodtype'
              className='react-select'
              classNamePrefix='select'
              options={bloodtypeOptions}
              defaultValue={bloodtypeOptions[0]}
            />
          </FormGroup>
        </Row> */}
        <Row>
          <FormGroup tag={Col} md='8'>
            <Label className='form-label' for='dob'>
             Date of Birth
            </Label>
            <Input
              type='date'
              name='dob'
              id='dob'
              placeholder='Date of Birth'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['dob'] })}
            />
          </FormGroup>
          {/* <FormGroup tag={Col}>
            <Label className='form-label' for='citizenship'>
              Citizenship
            </Label>
            <Input
              type='text'
              name='citizenship'
              id='citizenship'
              placeholder='Citizenship'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['citizenship'] })}
            />
          </FormGroup> */}
        </Row>
        <Row>
          <FormGroup tag={Col} md='8'>
            <Label className='form-label' for='occupation'>
             Occupation/Job Title
            </Label>
            <Input
              type='text'
              name='occupation'
              id='occupation'
              placeholder='Occupation/Job Title'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['occupation'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='civilstatus'>
              Civil Status
            </Label>
            <Select
              theme={selectThemeColors}
              isClearable={false}
              id='civilstatus'
              className='react-select'
              classNamePrefix='select'
              options={civilstatusOptions}
              defaultValue={civilstatusOptions[0]}
            />
          </FormGroup>
        </Row>
        <div className='content-header'>
        <h5 className='mb-1'>Security</h5>
        <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='password'>
             Password
            </Label>
            <InputPasswordToggle
              name='password'
              id='password'
              placeholder='Password'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['password'] })}
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='password_confirmation'>
              Password Confirmation
            </Label>
            <InputPasswordToggle
              name='password_confirmation'
              id='confirm'
              placeholder='Confirm Password'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['password_confirmation'] })}
            />
          </FormGroup>
        </Row>
      </div>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' color='primary' className='btn-next' disabled={!isFormValid()}>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default PersonalInfo
