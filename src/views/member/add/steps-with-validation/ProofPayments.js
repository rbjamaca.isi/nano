import { Fragment, useEffect, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, Upload } from 'react-feather'
import { Label, FormGroup, Row, Col, Button, Form, Input } from 'reactstrap'
import FileUpload from '../../../shared/file-uploader'
import Uppy from '@uppy/core'
import { getFiles } from 'redux/reducers/file'
import { useSelector } from 'react-redux'
import Avatar from '@core/components/avatar'
import ImageUploader from "react-images-upload"
import { validateText } from 'utility/Utils'


const ProofPayments = ({ stepper, formdata, handleFormSubmit }) => {
  const { register, errors, handleSubmit, trigger, watch } = useForm()
  const [pictures, setPictures] = useState([])

  const onSubmit = (data) => {
    trigger()
  if (isObjEmpty(errors)) {
      // setFormdata(form => ({...form, proof: pictures[0]}))
      handleFormSubmit({...data, proof: pictures[0]})
    }
  }

  const onDrop = picture => {
    setPictures(picture)
  }

  const isFormValid = () => {
    return pictures.length > 0 
    && validateText(watch('mode')) 
    && validateText(watch('reference'))
  }

  useEffect(() => { console.log(pictures) }, [pictures])

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Proof of Payments</h5>
        <small>Upload Proof of Payments.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>

      <Row>
        <FormGroup tag={Col}>
            <Label className='form-label' for='mode'>
             Payment
            </Label>
            <Input
              type='number'
              name='payment_amount'
              id='payment'
              value={formdata?.type === "Regular" ? 250 : 150}
              innerProps={{readOnly:true}}
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['mode'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='mode'>
             Mode of Payment
            </Label>
            <Input
              type='text'
              name='mode'
              id='mode'
              placeholder='Gcash'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['mode'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='reference'>
              Reference Number
            </Label>
            <Input
              type='text'
              name='reference'
              id='reference'
              placeholder='Reference no.'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['reference'] })}
            />
          </FormGroup>
        </Row>
        <ImageUploader
          withPreview={true}
          withIcon={true}
          onChange={onDrop}
          imgExtension={[".jpg", ".gif", ".png", ".gif"]}
          maxFileSize={5242880}
          singleImage={true}
        />
       {/* <FileUpload /> */}
       {/* <label
          htmlFor="button-file"
          className='flex items-center justify-center relative w-128 h-128 rounded-8 mx-8 mb-16 overflow-hidden cursor-pointer shadow hover:shadow-lg'>
          <input
            accept="image/*"
            className="hidden"
            id="button-file"
            type="file"
            onChange={handleUploadChange}
          />
           <Avatar size='xl' color='success' icon={<Upload size={50} />} />
        </label> */}
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' disabled={!isFormValid()} color='success' className='btn-submit'>
            Submit
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default ProofPayments
