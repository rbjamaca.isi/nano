import * as yup from 'yup'
import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { yupResolver } from '@hookform/resolvers/yup'
import { Form, Label, Input, FormGroup, Row, Col, Button, CustomInput } from 'reactstrap'

const AccountDetails = ({ stepper, setFormdata, setPremium, premium }) => {
 

  const { register, errors, handleSubmit, trigger } = useForm()

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(data)
      stepper.next()
    }
  }

  return (
    <Fragment>
      {/* <div className='content-header'>
        <h5 className='mb-0'>Account Details</h5>
        <small className='text-muted'>Enter Account Details.</small>
      </div> */}
      <Form onSubmit={handleSubmit(onSubmit)}>
        {/* <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='username'>
              Username
            </Label>
            <Input
              name='username'
              id='username'
              placeholder='johndoe'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['username'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='email'>
              Email
            </Label>
            <Input
              type='email'
              name='email'
              id='email'
              placeholder='john.doe@email.com'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['email'] })}
            />
          </FormGroup>
        </Row> */}
        {/* <Row>
          <div className='form-group form-password-toggle col-md-6'>
            <Label className='form-label' for='password'>
              Password
            </Label>
            <Input
              type='password'
              name='password'
              id='password'
              placeholder='Password'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['password'] })}
            />
          </div>
          <div className='form-group form-password-toggle col-md-6'>
            <Label className='form-label' for='confirm-password-val'>
              Confirm Password
            </Label>
            <Input
              type='password'
              name='confirm-password-val'
              id='confirm-password-val'
              placeholder='Confirm Password'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['confirm-password-val'] })}
            />
          </div>
        </Row> */}
        <div className='content-header mt-50'>
          <h5 className='mb-0'>Membership Type</h5>
          <small className='text-muted'>Select membership type.</small>
        </div>
        {premium && premium === "Premium" && (<FormGroup tag={Col} md="12">
          <Label className='form-label' for='name'>
              Membership
          </Label>
        <ul className='other-payment-options list-unstyled'>
          <li className='py-50'>
            <CustomInput type='radio' innerRef={register({ required: true })} label='Regular Premium' value="Regular" name='type' id='prof-premium' />
          </li>
          <li className='py-50'>
            <CustomInput type='radio' innerRef={register({ required: true })} label='Student Premium' value="Student" name='type' id='stud-premium' />
          </li>
        </ul>
        </FormGroup>)}
        <div className='d-flex justify-content-end'>
          <Button.Ripple type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default AccountDetails
