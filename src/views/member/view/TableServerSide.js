// ** React Imports
import { memo, useState } from 'react'
import { Link } from 'react-router-dom'
import { useForm } from 'react-hook-form'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import Moment from 'react-moment'
import {
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  UncontrolledTooltip,
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Button,
  Form,
  FormGroup,
  Label,
  Input, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap'
import { Eye } from 'react-feather'
import cmtImg from '@src/assets/images/banner/banner-12.jpg'

// ** Custom Components
import Sidebar from '@components/sidebar'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'

// ** Toastify
const ToastContent = ({ msg, role }) => (
  <Fragment>
    <div className='toastify-header'>
      {/* <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Welcome, {name}</h6>
      </div> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const MembersTable = ({ userData, list, loading }) => {
  // ** States
  const { register, errors, handleSubmit, trigger } = useForm()
  const [open, setOpen] = useState(false)
  
  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  // ** Form Submit
  const onSubmit = (data) => {

  }

  console.log(list)

   return (
    <div>
    <Card>
      <Table responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Full Name</th>
            <th>Date Registered</th>
            <th>Status</th>
            <th>Subscription</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {list && list.map(data => (
            <tr key={data.id}>
              <td>
                {data.id}
              </td>
              <td>
                {data.firstname} {data.lastname}
              </td>
              <td>
                <Moment format="LL">
                  {data.created_at}
                </Moment>
              </td>
              <td>
                {data.status}
              </td>
              <td>
                {data.is_premium ? "Premium" : "Non-Premium"}
              </td>
              <td>
                <div className='column-action d-flex align-items-center'>
                  <Link
                  to={(`/admin/member/${data.id}`)}
                  id={`send-tooltip-${data.id}`}>
                  <Eye size={17} />
                </Link>
                <UncontrolledTooltip placement='top' target={`send-tooltip-${data.id}`}>
                  Preview Account
                </UncontrolledTooltip>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
    <Sidebar
    size='lg'
    open={open}
    title='Upload Proof of Payment'
    headerClassName='mb-1'
    contentClassName='p-0'
    toggleSidebar={toggleSidebar}
  >
    <Form onSubmit={handleSubmit(onSubmit)}>
      <FormGroup>
        <Label className='form-label'>
          Payment Gateway
        </Label>
        <Input placeholder='GCash' />
      </FormGroup>
      <FormGroup>
        <Label className='form-label'>
          Reference Number
        </Label>
        <Input placeholder='Input Reference Number' />
      </FormGroup>
      <FormGroup className='d-flex flex-wrap mt-2'>
        <Button className='mr-1' color='primary' onClick={() => setOpen(false)}>
          Save
        </Button>
        <Button color='secondary' onClick={() => setOpen(false)} outline>
          Cancel
        </Button>
      </FormGroup>
    </Form>
  </Sidebar>
  </div>
  )
}

export default memo(MembersTable)
