import * as yup from 'yup'
import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty, selectThemeColors } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { yupResolver } from '@hookform/resolvers/yup'
import { Form, Label, Input, FormGroup, Row, Col, Button, CustomInput } from 'reactstrap'

import CreatableSelect from 'react-select/creatable'
import makeAnimated from 'react-select/animated'

const colorOptions = [
  { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
  { value: 'blue', label: 'Blue', color: '#0052CC', isFixed: true },
  { value: 'purple', label: 'Purple', color: '#5243AA', isFixed: true },
  { value: 'red', label: 'Red', color: '#FF5630', isFixed: false },
  { value: 'orange', label: 'Orange', color: '#FF8B00', isFixed: false },
  { value: 'yellow', label: 'Yellow', color: '#FFC400', isFixed: false }
]

const Details = ({ stepper, setFormdata, tagData }) => {
 
  const animatedComponents = makeAnimated()

  const { register, errors, handleSubmit, trigger } = useForm()

  const handleChange = (newValue) => {
    setTags(newValue)
  }

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(data)
      stepper.next()
    }
  }

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Company Details</h5>
        <small className='text-muted'>Enter Company Details.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='company'>
              Company Name
            </Label>
            <Input
              name='company'
              id='company'
              placeholder='Company Name'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['company'] })}
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='link'>
              Contact Number
            </Label>
            <Input
              name='contact'
              id='contact'
              type='number'
              placeholder='Contact Number'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['contact'] })}
            />
          </FormGroup>
        </Row>
        <div className='d-flex justify-content-end'>
          <Button.Ripple type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default Details
