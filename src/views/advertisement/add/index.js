// ** React Imports
import React, { useRef, useState, useEffect, Fragment } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'

// ** Third Party Components
import classnames from 'classnames'
import { Breadcrumb, BreadcrumbItem, Form, Row, FormGroup, Col, Label, Input, Button, Card } from 'reactstrap'
import { X, Plus, ArrowLeft, ArrowRight } from 'react-feather'
import { toast, Slide } from 'react-toastify'
import { isObjEmpty, selectThemeColors } from '@utils'
import CreatableSelect from 'react-select/creatable'
import makeAnimated from 'react-select/animated'
import Repeater from '@components/repeater'
import ImageUploader from "react-images-upload"

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'

// ** Toastify
const ToastContent = ({ msg }) => (
  <React.Fragment>
    <div>
      <h5 className='toast-title font-weight-bold'>{msg}</h5>
    </div>
  </React.Fragment>
)

const SponsorAdd = () => {
  // ** States
  const history = useHistory()
  const animatedComponents = makeAnimated()
  const { register, errors, handleSubmit, trigger } = useForm()

  const [formdata, setFormdata] = useState([]),
    [sponsors, setSponsors] = useState(null),
    [count, setCount] = useState(1),
    [pictures, setPictures] = useState([])

  // ** Vars
  const { id } = useParams()

  const onDrop = picture => {
    setPictures([...pictures, picture[0]])
  }

  const deleteForm = e => {
    e.preventDefault()
    e.target.closest('form').remove()
  }


  // ** Form Submit
  const handleFormSubmit = (data) => {
    const ads = data
    const images = pictures
    const form_data = new FormData()
    for (const key in formdata) {
      form_data.append(key, Array.isArray(formdata[key]) ? JSON.stringify(formdata[key]) : formdata[key])
    }
    form_data.append('ads', JSON.stringify(ads))
    for (const val in images) {
      form_data.append('images[]', images[val])
    }
    form_data.append('id', id)
    API_ACTION(form_data, `/api/advert`).then(res => {
      toast.success(
        <ToastContent msg="Successfully added sponsor" />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      history.push('/sponsors')
    }).catch(err => {
      console.log(err)
    })
  }

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/sponsors'> <h5 className="text-primary">Ads</h5> </Link>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <Link to='#'> <h5>Add New Ads for Sponsor #{id}</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <Card className='py-2 px-2'>
        <div className='content-header mb-50'>
          <h5 className='mb-0'>Ads Details</h5>
          <small className='text-muted'>Enter Ad Details.</small>
        </div>
        <Form onSubmit={handleSubmit(handleFormSubmit)}>
          <Repeater count={count}>
            {i => (
              <Row className='justify-content-between align-items-center' key={i}>
                <ImageUploader
                  withPreview={true}
                  withIcon={true}
                  onChange={onDrop}
                  imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                  maxFileSize={5242880}
                  singleImage={true}
                />
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Title</Label>
                    <Input type='text' id={`item-name-${i}`} name={`${i}.title`} innerRef={register({ required: true })} placeholder='Ad Title' />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Target URL</Label>
                    <Input type='url' id={`item-name-${i}`} name={`${i}.url`} innerRef={register({ required: true })} placeholder='Target URL' />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Start</Label>
                    <Input type='date' id={`item-start-${i}`} name={`${i}.start`} innerRef={register({ required: true })} />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>End</Label>
                    <Input type='date' id={`item-end-${i}`} name={`${i}.end`} innerRef={register({ required: true })} />
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Button.Ripple color='danger' className='text-nowrap px-1' onClick={deleteForm} outline>
                    <X size={14} className='mr-50' />
                    <span>Delete</span>
                  </Button.Ripple>
                </Col>
                <Col sm={12}>
                  <hr />
                </Col>
              </Row>
            )}
          </Repeater>

          <div className='d-flex justify-content-end'>
            <Button.Ripple type='button' color='link' onClick={() => history.goBack()}>
              Back
            </Button.Ripple>
            <Button.Ripple type='submit' color='primary' className='btn-next'>
              <span className='align-middle d-sm-inline-block d-none'>Submit</span>
            </Button.Ripple>
          </div>
        </Form>
      </Card>
    </Fragment>
  )
}

export default SponsorAdd
