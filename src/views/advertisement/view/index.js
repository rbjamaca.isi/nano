// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Third Party Components
import Select from 'react-select'
import {
  Card, CardHeader, CardTitle, CardBody, Input, Row, Col, Label, CustomInput, Button,
  Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap'

// ** Tables
import TableServerSide from './TableServerSide'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'

// ** Toastify
const ToastContent = ({ msg, role }) => (
  <Fragment>
    <div className='toastify-header'>
      {/* <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Welcome, {name}</h6>
      </div> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const Trainings = () => {
  // ** State
  const [loading, setLoading] = useState(null)
  const history = useHistory()
  const [currentRole, setCurrentRole] = useState({ value: 'all', label: 'All' })
  // const [list, setList] = useState([])

  // ** Dummy data
  const list = [
    { id: 2, company: 'Company B', contact: '09168470982', created_at: '09/05/2021'},
    { id: 1, company: 'Company A', contact: '09168470981', created_at: '09/05/2021'}
  ]
 
  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
      try {
        const response = await API_RETRIEVE(`/api/admin/sponsors`)
        setLoading(false)
        // setList(response.data.data.sponsors)
      } catch (err) {
        console.log(err)
      }
  }

  useEffect(() => { getData() }, [])

  // ** Filter options
  const roleOptions = [
    { value: 'all', label: 'All' },
    { value: 'order3', label: 'Order #3' },
    { value: 'order2', label: 'Order #2' },
    { value: 'order1', label: 'Order #1' }
  ]

  return (
    <Fragment>
    <div style={{ paddingBottom: 10 }}>
      <Breadcrumb>
        <BreadcrumbItem>
          <Link to='/sponsor/view'> <h5 className="text-primary">Sponsors</h5> </Link>
        </BreadcrumbItem>
      </Breadcrumb>
    </div>
    <Card>
      <CardHeader>
        Search Filter
      </CardHeader>
      <CardBody>
        <Row>
          <Col md='4'>
            <Select
              isClearable={false}
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              options={roleOptions}
              value={currentRole}
              onChange={data => {
                setCurrentRole(data)
                // dispatch(
                //   getData({
                //     page: currentPage,
                //     perPage: rowsPerPage,
                //     role: data.value,
                //     currentPlan: currentPlan.value,
                //     status: currentStatus.value,
                //     q: searchTerm
                //   })
                // )
              }}
            />
          </Col>
          <Col className='my-md-0 my-1' md='4'>
            <Select
              theme={selectThemeColors}
              isClearable={false}
              className='react-select'
              classNamePrefix='select'
            // options={planOptions}
            // value={currentPlan}
            // onChange={data => {
            //   setCurrentPlan(data)
            //   dispatch(
            //     getData({
            //       page: currentPage,
            //       perPage: rowsPerPage,
            //       role: currentRole.value,
            //       currentPlan: data.value,
            //       status: currentStatus.value,
            //       q: searchTerm
            //     })
            //   )
            // }}
            />
          </Col>
          <Col md='4'>
            <Input className='w-full ' type='select'>
              <option value=''>Select Status</option>
              <option value='downloaded'>Downloaded</option>
              <option value='draft'>Draft</option>
              <option value='paid'>Paid</option>
              <option value='partial payment'>Partial Payment</option>
              <option value='past due'>Past Due</option>
              <option value='partial payment'>Partial Payment</option>
            </Input>
          </Col>
        </Row>
      </CardBody>
    </Card>

    <Card>
      <CardHeader>
        <div className='invoice-list-table-header w-100'>
          <Row>
            <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
              <div className='d-flex align-items-center mr-2'>
                <Label for='rows-per-page'>Show</Label>
                <CustomInput
                  className='form-control ml-50 pr-3'
                  type='select'
                  id='rows-per-page'
                // onChange={handlePerPage}
                >
                  <option value='10'>10</option>
                  <option value='25'>25</option>
                  <option value='50'>50</option>
                </CustomInput>
              </div>

            </Col>
             <Col
                lg='6'
                className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'
              >
                <Button.Ripple tag={Link} to='/ads/add' color='primary' outline>
                  Add New Advertisement
                </Button.Ripple>
              </Col>
          </Row>
        </div>
      </CardHeader>
      <Row>
        <Col sm='12'>
          <Card>
            <TableServerSide list={list} loading={loading} />
          </Card>
        </Col>
      </Row>
    </Card>
  </Fragment>
)
}


export default Trainings
