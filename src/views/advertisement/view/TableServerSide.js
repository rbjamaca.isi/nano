// ** React Imports
import { memo } from 'react'
import { Link } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import Moment from 'react-moment'
import {
  Table,
  UncontrolledTooltip,
  Card
} from 'reactstrap'
import {
  Eye
} from 'react-feather'

const SponsorTable = ({ userData, list, loading }) => {

  return (
    <Card>
      <Table striped hover bordered responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Company</th>
            <th>Contact No.</th>
            <th>Start</th>
            <th>End</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {list && list.map(data => (
            <tr key={data.id}>
              <td>
                {data.id}
              </td>
              <td>
                {data.company}
              </td>
              <td>
                {data.contact}
              </td>
              <td>
              <Moment format="LLL">
                  {data.start}
                </Moment>
              </td>
              <td>
              <Moment format="LLL">
                  {data.end}
                </Moment>
              </td>
              <td>
                   <Link
                  to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/sponsor/preview/${data.id}` : `/sponsor/preview/${data.id}`)}
                  id={`pw-tooltip-${data.id}`}>
                    <Eye size={17} className='mx-1' />
                  </Link>
                  <UncontrolledTooltip placement='top' target={`pw-tooltip-${data.id}`}>
                    Preview Sponsor
                  </UncontrolledTooltip>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
  )
}

export default memo(SponsorTable)
