// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData, timestamp } from 'utility/Utils'

// ** Third Party Components
import { Link } from 'react-router-dom'
import Breadcrumbs from '@components/breadcrumbs'
import Header from './Header'
import { Row, Col, Card, CardBody, CardImg } from 'reactstrap'

// ** Styles
import '@styles/base/pages/page-knowledge-base.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '../../data/api'
import { Slide, toast } from 'react-toastify'

const Training = () => {
  // ** States
  const [data, setData] = useState(null)
  const [loading, setLoading] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  let userData

  //** User data
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      userData = JSON.parse(localStorage.getItem('userData'))
    } 
  }, [])

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
      try {
        const response = await API_RETRIEVE(`/api/training`)
        setLoading(false)
        setData(response.data.data.trainings)
      } catch (err) {
        console.log(err)
      }
  }

  useEffect(() => { getData() }, [])

  // dummy data
 const image = [
    {
      id: 1,
      category: 'sales-automation',
      img: require('@src/assets/images/illustration/sales.svg').default,
      title: 'Sales Automation',
      desc: 'There is perhaps no better demonstration of the folly of image of our tiny world.'
    },
    {
      id: 2,
      category: 'marketing-automation',
      img: require('@src/assets/images/illustration/marketing.svg').default,
      title: 'Marketing Automation',
      desc: 'Look again at that dot. That’s here. That’s home. That’s us. On it everyone you love.'
    },
    {
      id: 3,
      category: 'api-questions',
      img: require('@src/assets/images/illustration/api.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 4,
      category: 'demand',
      img: require('@src/assets/images/illustration/demand.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 5,
      category: 'demand',
      img: require('@src/assets/images/illustration/email.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 6,
      category: 'demand',
      img: require('@src/assets/images/illustration/personalization.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    }
  ]

  const Content = ({ item, image }) => (
    <Col className='kb-search-content' key={item.id} md='4' sm='6'>
      <Card>
        <Link to={`/training/preview/${item.id}`}>
          {<CardImg 
          src={image[Math.floor(Math.random() * image.length)].img}
          // src={`https://source.unsplash.com/random/1600x900/?t=${Math.random()}`}
          alt='knowledge-base-image' top />}
          <CardBody className='text-center'>
            <h4>{item.title}</h4>
            <p className='text-body mt-1 mb-0'>{item.desc}</p>
          </CardBody>
        </Link>
      </Card>
    </Col>
  )

  const renderContent = () => {
    return data.map(item => {
      const titleCondition = item.title.toLowerCase().includes(searchTerm.toLowerCase()),
        descCondition = item.desc.toLowerCase().includes(searchTerm.toLowerCase())

      if (searchTerm.length < 1) {
        return <Content key={item.id} item={item} image={image} />
      } else if (titleCondition || descCondition) {
        return <Content key={item.id} item={item}  image={image} />
      } else {
        return null
      }
    })
  }

  if (!data) {
    return null
  }
  return (
    <Fragment>
      <Header searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
      {/* {data !== null ? ( */}
        <div id='knowledge-base-content'>
          <Row className='kb-search-content-info match-height'>{renderContent()}</Row>
        </div>
      {/* ) : null} */}
    </Fragment>
  )
}

export default Training
