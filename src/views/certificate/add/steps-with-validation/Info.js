import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty } from '@utils'
import { useForm } from 'react-hook-form'
import Repeater from '@components/repeater'
import { Row, Col, Card, CardHeader, CardBody, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { X, Plus, ArrowLeft, ArrowRight } from 'react-feather'
import ImageUploader from "react-images-upload"

const Info = ({ stepper, setFormdata, handleFormSubmit }) => {
  const { register, errors, handleSubmit, trigger } = useForm()
  const [count, setCount] = useState(1)
  const [pictures, setPictures] = useState([])

  const onDrop = picture => {
    setPictures([...pictures, picture[0]])
  }

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      handleFormSubmit({ads: data, images: pictures})
    }
  }

  const increaseCount = () => {
    setCount(count + 1)
  }

  const deleteForm = e => {
    e.preventDefault()
    e.target.closest('form').remove()
  }


  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Ads Info</h5>
        <small>Add Ads Information.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Repeater count={count}>
          {i => (
            <Form key={i}>
              <Row className='justify-content-between align-items-center'>
              <ImageUploader
                    withPreview={true}
                    withIcon={true}
                    onChange={onDrop}
                    imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                    maxFileSize={5242880}
                    singleImage={true}
                  />
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Title</Label>
                    <Input type='text' id={`item-name-${i}`} name={`${i}.title`} innerRef={register({ required: true })} placeholder='Ad Title' />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Target URL</Label>
                    <Input type='url' id={`item-name-${i}`} name={`${i}.url`} innerRef={register({ required: true })} placeholder='Target URL' />
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Button.Ripple color='danger' className='text-nowrap px-1' onClick={deleteForm} outline>
                    <X size={14} className='mr-50' />
                    <span>Delete</span>
                  </Button.Ripple>
                </Col>
                <Col sm={12}>
                  <hr />
                </Col>
              </Row>
            </Form>
          )}
        </Repeater>
        <Button.Ripple className='btn-icon mb-2' color='primary' outline onClick={increaseCount}>
          <Plus size={14} />
          <span className='align-middle ml-25'>Add New</span>
        </Button.Ripple>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' color='success' className='btn-submit'>
            Submit
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default Info
