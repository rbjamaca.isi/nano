// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Third Party Components
import Select from 'react-select'
import {
  Card, CardHeader, CardTitle, CardBody, Input, Row, Col, Label, CustomInput, Button,
  Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap'

// ** Tables
import TableServerSide from './TableServerSide'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'

// ** Toastify
const ToastContent = ({ msg, role }) => (
  <Fragment>
    <div className='toastify-header'>
      {/* <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Welcome, {name}</h6>
      </div> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const Trainings = () => {
  // ** State
  const [loading, setLoading] = useState(null)
  const history = useHistory()
  const [currentRole, setCurrentRole] = useState({ value: 'all', label: 'All' })

   // ** Dummy data
   const list = 
   [
     {
       id: '1',
    title: 'Webinar on Marketing and Promotion Strategies',
    company: 'Company B',
    contact: '09168470982',
    firstname: 'John',
    lastname: 'Doe',
    created_at: '05/18/2021'
  }
]

  // const [list, setList] = useState([])
 
  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
      try {
        const response = await API_RETRIEVE(`/api/sponsor`)
        setLoading(false)
        // setList(response.data.data.sponsors)
      } catch (err) {
        console.log(err)
      }
  }

  useEffect(() => { getData() }, [])

  // ** Filter options
  const roleOptions = [
    { value: 'all', label: 'All' },
    { value: 'order3', label: 'Order #3' },
    { value: 'order2', label: 'Order #2' },
    { value: 'order1', label: 'Order #1' }
  ]

  return (
    <Fragment>
    <div style={{ paddingBottom: 10 }}>
      <Breadcrumb>
        <BreadcrumbItem>
          <Link to='/certificate/view'> <h5 className="text-primary">Certificates</h5> </Link>
        </BreadcrumbItem>
      </Breadcrumb>
    </div>

    <Card>
      <CardHeader>
        <div className='invoice-list-table-header w-100'>
          <Row>
            <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
              <div className='d-flex align-items-center mr-2'>
                <Label for='rows-per-page'>Show</Label>
                <CustomInput
                  className='form-control ml-50 pr-3'
                  type='select'
                  id='rows-per-page'
                // onChange={handlePerPage}
                >
                  <option value='10'>10</option>
                  <option value='25'>25</option>
                  <option value='50'>50</option>
                </CustomInput>
              </div>

            </Col>
          </Row>
        </div>
      </CardHeader>
      <Row>
        <Col sm='12'>
          <Card>
            <TableServerSide list={list} loading={loading} />
          </Card>
        </Col>
      </Row>
    </Card>
  </Fragment>
)
}


export default Trainings
