// ** React Imports
import { memo } from 'react'
import { Link } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import Moment from 'react-moment'
import {
  Table,
  UncontrolledTooltip,
  Card
} from 'reactstrap'
import {
  Eye
} from 'react-feather'

const openInNewTab = (url) => {
  const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
  if (newWindow) newWindow.opener = null
  }
  
const CertificateTable = ({ userData, list, loading }) => {

  return (
    <Card>
      <Table striped hover bordered responsive>
        <thead>
          <tr>
            <th>Certificate Number</th>
            <th>Attendee</th>
            <th>Date</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {list && list.map(data => (
            <tr key={data.id}>
              <td>
                {data.id}
              </td>
              <td>
                {data.firstname} {data.lastname}
              </td>
              <td>
              <Moment format="LLL">
                  {data.created_at}
                </Moment>
              </td>
              <td>
                   <Link
                  onClick={() => {
                    openInNewTab(isUserLoggedIn() && getUserData().role === 'Admin' ? `/certificate/preview/${data.id}` : `/certificate/preview/${data.id}`)
                  }}
                  id={`pw-tooltip-${data.id}`}>
                    <Eye size={17} className='mx-1' />
                  </Link>
                  <UncontrolledTooltip placement='top' target={`pw-tooltip-${data.id}`}>
                    Preview Certificate
                  </UncontrolledTooltip>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
  )
}

export default memo(CertificateTable)
