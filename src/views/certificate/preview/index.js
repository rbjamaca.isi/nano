import React, { useEffect, useState } from 'react'
import { PDFViewer, Page, Text, View, Document, StyleSheet, Font, Image } from '@react-pdf/renderer'
import { useHistory, useParams } from 'react-router-dom'
import Moment from 'react-moment'
import roboto_regular from "./fonts/Roboto-Light.ttf"
import roboto_medium from "./fonts/Roboto-Medium.ttf"
import roboto_bold from "./fonts/Roboto-Bold.ttf"
import italic from "./fonts/OpenSans-Italic.ttf"
import semibold from "./fonts/OpenSans-SemiBold.ttf"
// ** Store & Actions
import { API_RETRIEVE } from '@src/data/api'

// ** Imports
import template_1 from 'assets/images/certificates/template_1.png'
import template_2 from 'assets/images/certificates/template_2.jpg'
import template_3 from 'assets/images/certificates/template_3.png'
import logo from 'assets/images/logo/logo.jpg'
import badge from 'assets/images/certificates/badge.png'

// Register font
Font.register({ family: 'Open Sans Italic', src: italic })
Font.register({ family: 'Roboto Regular', src: roboto_regular })
Font.register({ family: 'Roboto Medium', src: roboto_medium })
Font.register({ family: 'Roboto Bold', src: roboto_bold })

const styles = StyleSheet.create({
  pageBackground: {
    position: 'absolute',
    minWidth: '100%',
    minHeight: '100%',
    display: 'block',
    height: '100%',
    width: '100%'
  },
  container: {
    textAlign: 'center'
  },
  alignment: {
    top: '13%'
  },
  logo: {
    width: 100,
    height: 'auto',
    alignSelf: 'center',
    paddingBottom: 20
  },
  badge: {
    position: 'absolute',
    width: 100,
    height: 'auto',
    left: '70%',
    bottom: '-30%'
  },
  title: {
    fontSize: 30,
    letterSpacing: 1,
    color: '#f2bb2b',
    fontFamily: 'Roboto Regular'
  },
  subtitle: {
    paddingTop: 50,
    fontSize: 20,
    letterSpacing: 1,
    fontFamily: 'Roboto Regular'
  },
  name: {
    paddingTop: 10,
    fontSize: 60,
    letterSpacing: 1,
    color: '#1e8c05',
    fontFamily: 'Roboto Bold'
  },
  subtext: {
    paddingTop: 50,
    fontSize: 20,
    letterSpacing: 1,
    fontFamily: 'Roboto Regular'
  },
  training: {
    // alignSelf: 'center',
    color: '#f2bb2b',
    paddingTop: 20,
    fontSize: 35,
    letterSpacing: 1,
    fontFamily: 'Roboto Medium'
    // maxWidth: '70%'
  },
  date: {
    paddingTop: 15,
    fontSize: 20,
    fontFamily: 'Roboto Regular'
  },
  line: {
    position: 'absolute',
    fontFamily: 'Roboto Regular',
    bottom: '-20%'
  },
  authorized: {
    position: 'absolute',
    fontFamily: 'Roboto Regular',
    bottom: '-25%'
  },
  position: {
    position: 'absolute',
    fontFamily: 'Roboto Regular',
    bottom: '-30%'
  }
})

const CertificatePDF = () => {
  // ** States
  const history = useHistory()
  const [loading, setLoading] = useState(null)
  const [open, setOpen] = useState(false)

  // ** Vars
  const { id } = useParams()

  // ** Dummy data
  const data = {
    id: 1,
    title: 'Webinar on Marketing',
    company: 'Company B',
    contact: '09168470982',
    firstname: 'John',
    lastname: 'Doe',
    created_at: '09/06/2021'
  }

  // const [data, setData] = useState(null)

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/sponsor/${id}`)
      setLoading(false)
      // setData(response.data.data)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  return (
    <PDFViewer height={1500} width='100%'>
      <Document>
        <Page size="LETTER" orientation="portrait" style={styles.container}>
          <Image
            src={template_3}
            style={styles.pageBackground}
          />
          <div style={styles.alignment}>
            <Image
              src={logo}
              style={styles.logo}
            />
            <Text style={styles.title}>
              CERTIFICATE OF PARTICIPATION
            </Text>
            <Text style={styles.subtitle}>
              This is to certify that
            </Text>
            <Text style={styles.name}>
              {data ? data?.firstname.toUpperCase() : ''} {data ? data?.lastname.toUpperCase() : ''}
            </Text>
            <Text style={styles.subtext}>
              has successfully completed a training on
            </Text>
            <Text style={styles.training}>
              {data ? data?.title : ''}
            </Text>
            <Text style={styles.date}>
              {/* {data ? data?.created_at : ''} */}
              10th June 2021
            </Text>
            <Text style={styles.line}>
              _____________________
            </Text>
            <Text style={styles.authorized}>
              Dr. Personnel Name
            </Text>
            <Text style={styles.position}>
              Director
            </Text>
            <Image
              src={badge}
              style={styles.badge}
            />
          </div>
        </Page>
      </Document>
    </PDFViewer>
  )
}

export default CertificatePDF

