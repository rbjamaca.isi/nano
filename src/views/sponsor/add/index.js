// ** React Imports
import React, { useRef, useState, useEffect } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { getUserData } from 'utility/Utils'

// ** Third Party Components
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { toast, Slide } from 'react-toastify'

// ** Pages
import Wizard from '@components/wizard'
import Info from './steps-with-validation/Info'
import Details from './steps-with-validation/Details'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'

// ** Toastify
const ToastContent = ({ msg }) => (
  <React.Fragment>
    <div>
      <h5 className='toast-title font-weight-bold'>{msg}</h5>
    </div>
    {/* <div>
      <span>{msg}</span>
    </div> */}
  </React.Fragment>
)

const SponsorAdd = () => {
  // ** States
  const [stepper, setStepper] = useState(null)
  const ref = useRef(null)
  const [formdata, setFormdata] = useState([])
  const history = useHistory()

  // ** Vars
  const { id } = getUserData()

  // Form
  const handleFormSubmit = ({ads, images}) => {
    const form_data = new FormData()
    for (const key in formdata) {
      form_data.append(key, Array.isArray(formdata[key]) ? JSON.stringify(formdata[key]) : formdata[key])
    }
    form_data.append('ads', JSON.stringify(ads))
    for (const val in images) {
      form_data.append('images[]', images[val])
    }
    API_ACTION(form_data, `/api/sponsor`).then(res => {
      toast.success(
        <ToastContent msg="Successfully added sponsor" />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      history.push('/sponsors')
    }).catch(err => {
      console.log(err)
    })
  }

  const steps = [
    {
      id: 'details',
      title: 'Company Details',
      subtitle: 'Enter Company Details.',
      content: <Details setFormdata={setFormdata} stepper={stepper} />
    },
    {
      id: 'info',
      title: 'Ads Info',
      subtitle: 'Add Ads Info',
      content: <Info setFormdata={setFormdata} handleFormSubmit={handleFormSubmit} stepper={stepper} />
    }
  ]

  return (
    <>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/sponsors'> <h5 className="text-primary">Sponsors</h5> </Link>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <Link to='#'> <h5>Add New Sponsor</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <div className='vertical-wizard'>
      <Wizard
        type='vertical'
        ref={ref}
        steps={steps}
        options={{
          linear: false
        }}
        instance={el => setStepper(el)}
      />
    </div>
    </>
  )
}

export default SponsorAdd
