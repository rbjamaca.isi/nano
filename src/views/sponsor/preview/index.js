// ** React Imports
import { Fragment, useState, useEffect } from 'react'
import { useHistory, useParams, Link } from 'react-router-dom'
import classnames from 'classnames'

// ** Utils
import { getUserData, isUserLoggedIn, timestamp } from 'utility/Utils'

// ** Third Party Components
import { toast, Slide } from 'react-toastify'
import Avatar from '@components/avatar'
import { Plus, Award, Check, Eye, Trash, Edit } from 'react-feather'
import Moment from 'react-moment'
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardHeader,
  Form,
  Table,
  Button, FormGroup, Label, Input,
  Breadcrumb, BreadcrumbItem,
  UncontrolledTooltip
} from 'reactstrap'

// ** Styles
import '@styles/base/pages/page-blog.scss'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import moment from 'moment'
import { useForm } from 'react-hook-form'

// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='toast-title font-weight-bold'>Successful!</h6>
      </div>
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const SponsorPreview = () => {
  // ** States
  const [formdata, setFormdata] = useState([])
  const history = useHistory()
  const [loading, setLoading] = useState(null)
  const [open, setOpen] = useState(false)
  const { register, errors, handleSubmit, trigger, getValues } = useForm()
  const [pictures, setPictures] = useState(null)

  const onDrop = picture => {
    setPictures(picture)
  }

  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  // ** Vars
  const { id } = useParams()

  // Form Submit
  const handleFormSubmit = (e) => {
    e.preventDefault()
    const form_data = new FormData()
    const formdata = getValues()

    for (const key in formdata) {
      console.log(key, formdata[key])
      form_data.append(key, formdata[key])
    }
    form_data.append('my_file', pictures[0])
    if (isUserLoggedIn()) {
      API_ACTION(form_data, `/api/training/proof/${id}`).then(res => {
        toast.success(
          <ToastContent msg="Submitted successfully. You will be informed when your request is approved" />,
          { transition: Slide, hideProgressBar: true, autoClose: 5000 }
        )
        setOpen(false)
      }).catch(err => {
        console.log(err)
      })
    } else {
      API_ACTION(form_data, `/api/training/non-member/proof/${id}`).then(res => {
        toast.success(
          <ToastContent msg="Submitted successfully. You will be informed when your request is approved" />,
          { transition: Slide, hideProgressBar: true, autoClose: 5000 }
        )
        setOpen(false)
      }).catch(err => {
        console.log(err)
      })
    }

  }

  useEffect(() => {
    console.log(formdata)
  }, [formdata])


  // ** Dummy data
  // const data = {
  //   id: 2,
  //   company: 'Company B',
  //   contact: '09168470982',
  //   created_at: '09/05/2021',
  //   ads:
  //     [
  //       {
  //         id: 1,
  //         image: `https://source.unsplash.com/1980x660/?learning,knowledge,videocall?t=${timestamp}`,
  //         title: 'Sample Title',
  //         target_url: 'www.google.com',
  //         created_at: '09/05/2021'
  //       }
  //     ]
  // }

  const [data, setData] = useState(null)
  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/sponsor/${id}`)
      setLoading(false)
      setData(response.data.data)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  const handleChange = () => {
    const values = getValues()
  }

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/sponsors'> <h5 className="text-primary">Sponsors</h5> </Link>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <Link to='#'> <h5>{data && data.company ? data.company : ''}</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      {data !== null ? (
        <Row>
          <Col sm='12'>
            <Card>
              <CardHeader>
                <CardTitle tag='h1'>{data.company}</CardTitle>
              </CardHeader>

              <CardBody>
                <h4 className="py-50">Basic Information</h4>
                <Form>
                  <FormGroup row>
                    <Label sm='5'>
                      Date Registered
                    </Label>
                    <Col>
                      <Moment format="LLL">
                        {data.created_at}
                      </Moment>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Label sm='5'>
                      Contact No.
                    </Label>
                    <Col>
                      {data.contact}
                    </Col>
                  </FormGroup>

                  <Row className='my-2'>
                    <Col>
                      <h4 className="py-50">Ads Information</h4>
                    </Col>
                    <Col
                      lg='6'
                      className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'
                    >
                      <Button.Ripple tag={Link} to={`/ads/add/${id}`} color='primary' outline>
                        Create New Ads
                      </Button.Ripple>
                    </Col>
                  </Row>

                  <Table hover bordered responsive>
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Target Url</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data && data.adverts.map(row => (
                        <tr key={row.id}>
                          <td>
                            {row.id}
                          </td>
                          <td>
                            <img src={`/storage/images/adverts/${row.image}`} className='img-fluid' top />
                          </td>
                          <td>
                            {row.title}
                          </td>
                          <td>
                            <a href={row.target_url}>{row.url}</a>
                          </td>
                          <td>
                          <Moment format="LL">
                              {row.start}
                          </Moment>
                          </td>
                          <td>
                          <Moment format="LL">
                              {row.end}
                            </Moment>
                          </td>
                          <td>
                          <div className='column-action d-flex align-items-center'>

                          <Link
                              // to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/ads/edit/${row.id}` : `/ads/edit/${row.id}`)}
                            onClick={() => alert('under construction')}
                              id={`update-${row.id}`}>
                              <Edit size={17} className='mx-1' />
                            </Link>
                            <UncontrolledTooltip placement='top' target={`update-${row.id}`}>
                              Update
                            </UncontrolledTooltip>
                            <Link
                            onClick={() => alert('under construction')}
                              id={`remove-${row.id}`}>
                              <Trash size={17} className='mx-1' />
                            </Link>
                            <UncontrolledTooltip placement='top' target={`remove-${row.id}`}>
                              Remove
                            </UncontrolledTooltip>
                            </div>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>

                  <div className='my-4 hidden'>
                  <FormGroup row>
                    <Col sm='5'>
                      <Button.Ripple type='button' color='link' onClick={() => history.goBack()}>
                        Back
                      </Button.Ripple>
                    </Col>
                    <Col>
                      <Button.Ripple className='mr-1' color='primary' type='button' onClick={() => history.push(`/sponsor/edit/${data.id}`)}>
                        Update
                      </Button.Ripple>
                      <Button.Ripple outline color='secondary' onClick={() => alert('todo: confirmation')}>
                        Remove
                      </Button.Ripple>
                    </Col>
                  </FormGroup>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      ) : null}
    </Fragment>
  )
}

export default SponsorPreview
