// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Third Party Components
import {
  Card, CardHeader, CardBody, Input, Row, Col, Label, CustomInput, Button, Badge, FormGroup,
  Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, ModalFooter, UncontrolledTooltip
} from 'reactstrap'
import {
  Eye,
  Edit,
  User
} from 'react-feather'
import Moment from 'react-moment'
import moment from 'moment'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Tables
import Table from '@src/views/misc/Table'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'

const Sponsors = () => {
  // ** State
  const [loading, setLoading] = useState(null)
  const history = useHistory()

  const [data, setData] = useState([])
  const [filteredData, setFilteredData] = useState([])
  const [currentPage, setCurrentPage] = useState(0)
  const [searchName, setSearchName] = useState('')
  const [searchStatus, setSearchStatus] = useState('')
  const [searchSubscription, setSearchSubscription] = useState('')
  const [currentSubscription, setcurrentSubscription] = useState({ value: 'all', label: 'All' })

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/sponsor`)
      setLoading(false)
      setData(response.data.data.sponsors)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  // ** Function to handle name filter
  const handleNameFilter = e => {
    const value = e.target.value
    let updatedData = []
    const dataToFilter = () => {
      if (searchStatus.length) {
        return filteredData
      } else {
        return data
      }
    }

    setSearchName(value)
    if (value.length) {
      updatedData = dataToFilter().filter(item => {
        const startsWith = item.company.toLowerCase().startsWith(value.toLowerCase())
        const includes = item.company.toLowerCase().includes(value.toLowerCase())

        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData([...updatedData])
      setSearchName(value)
    }
  }

  // ** Table Common Column
  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
      width: '10%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.id}
          </div>
        )
      }
    },
    {
      name: 'Company',
      selector: 'company',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.company}
          </div>
        )
      }
    },
    {
      name: 'Contact No.',
      selector: 'contact',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.contact}
          </div>
        )
      }
    },
    {
      name: 'Date Registered',
      selector: 'created_at',
      sortable: true,
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            <Moment format="LLL">
              {row.created_at}
            </Moment>
          </div>
        )
      }
    },
    {
      name: 'ACTIONS',
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            <Link
              to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/sponsor/preview/${row.id}` : `/sponsor/preview/${row.id}`)}
              id={`pw-tooltip-${row.id}`}>
              <Eye size={17} className='mx-1' />
            </Link>
            <UncontrolledTooltip placement='top' target={`pw-tooltip-${row.id}`}>
              Preview Sponsor
            </UncontrolledTooltip>
          </div>
        )
      }
    }
  ]

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/sponsor/view'> <h5 className="text-primary">Sponsors</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <Card>
        <CardHeader>
          Search Filter
        </CardHeader>
        <CardBody>
          <Row>
            <Col className='my-md-0 my-1'>
              <FormGroup>
                <Input id='name' placeholder='Input Company Name' value={searchName} onChange={handleNameFilter} />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <div className='invoice-list-table-header w-100'>
            <Row>
              <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
                <div className='d-flex align-items-center mr-2'>
                  <Label for='rows-per-page'>Show</Label>
                  <CustomInput
                    className='form-control ml-50 pr-3'
                    type='select'
                    id='rows-per-page'
                  // onChange={handlePerPage}
                  >
                    <option value='10'>10</option>
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                  </CustomInput>
                </div>

              </Col>
              <Col
                lg='6'
                className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'
              >
                <Button.Ripple tag={Link} to='/sponsor/add' color='primary' outline>
                  Add New Sponsor
                </Button.Ripple>
              </Col>
            </Row>
          </div>
        </CardHeader>
        <Row>
          <Col sm='12'>
            <Card>
              <Table
                filteredData={filteredData}
                data={data}
                searchName={searchName}
                searchStatus={searchStatus}
                searchSubscription={searchSubscription}
                columns={columns}
                currentPage={currentPage}
              />
            </Card>
          </Col>
        </Row>
      </Card>
    </Fragment>
  )
}


export default Sponsors
