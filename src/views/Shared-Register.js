import { Fragment, useState, useContext } from 'react'
import { isObjEmpty } from '@utils'
import classnames from 'classnames'
import { useSkin } from '@hooks/useSkin'
import useJwt from '@src/auth/jwt/useJwt'
import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { handleLogin } from '@store/actions/auth'
import { Link, useHistory } from 'react-router-dom'
import { AbilityContext } from 'utility/context/Can'
import InputPasswordToggle from '@components/input-password-toggle'
import { Facebook, Twitter, Mail, GitHub, Coffee } from 'react-feather'
import Ability from 'configs/acl/ability'
import { Row, Col, CardTitle, CardText, FormGroup, Label, Button, Form, Input, CustomInput, Spinner } from 'reactstrap'

import '@styles/base/pages/page-auth.scss'
import axios from 'axios'
import { CustomToast } from 'utility/CustomToast'
import { Slide, toast } from 'react-toastify'
import { API_ACTION } from 'data/api'

const Register = () => {
  const ability = useContext(AbilityContext)

  const [skin, setSkin] = useSkin()

  const history = useHistory()

  const dispatch = useDispatch()

  const { register, errors, handleSubmit, trigger } = useForm()

  const [email, setEmail] = useState('')
  const [valErrors, setValErrors] = useState({})
  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [password, setPassword] = useState('')
  const [password_confirmation, setPassword_confirmation] = useState('')
  const [terms, setTerms] = useState(false)
  const [loading, setLoading] = useState(false)

  const illustration = skin === 'dark' ? 'register-v2-dark.svg' : 'register-v2.svg',
    source = require(`@src/assets/images/pages/${illustration}`).default

  const Terms = () => {
    return (
      <Fragment>
        I agree to
        <a className='ml-25' href='/' onClick={e => e.preventDefault()}>
          privacy policy & terms
        </a>
      </Fragment>
    )
  }

 const onSubmit = () => {
    setLoading(true)
    if (isObjEmpty(errors)) {
      API_ACTION({email}, 'api/email-checker').then(res => {
        axios.post(`https://api.email-validator.net/api/verify?EmailAddress=${email}&APIKey=ev-a4653e5496a3b877ebde76b4f12e98e3`).then(res => {
          setLoading(false)

          if (res.data.ratelimit_remain === 0) {
            toast.error(
              <CustomToast title="Cannot validate email" msg='Sorry. We cannot validate your email at the moment. Please try again after a few hours' icon={<Coffee size={12} />} color="danger" />,
              { transition: Slide, hideProgressBar: true, autoClose: 2000 }
            )
          } else {
            if (res.data.status !== 200) {
              toast.error(
                <CustomToast title="Invalid email" msg='The email you provided is not valid. Please use an existing email.' icon={<Coffee size={12} />} color="danger" />,
                { transition: Slide, hideProgressBar: true, autoClose: 2000 }
              )
            } else {
              API_ACTION({email, firstname, lastname, password, password_confirmation}, `/api/member/details`).then(res => {
                
                  toast.success(
                    <CustomToast title="Success" msg="Successfully registered. Please login again." icon={<Coffee size={12} />} color="success"/>,
                    { transition: Slide, hideProgressBar: true, autoClose: 2000 }
                  )
                
                dispatch(handleLogout())
                history.push('/login')
              }).catch(err => {
                console.log(err.response)
               
                  toast.error(
                    <CustomToast title="Error" msg="An error has occured. Please try again later." icon={<Coffee size={12} />} color="danger"/>,
                    { transition: Slide, hideProgressBar: true, autoClose: 2000 }
                  )
                
                })
            }
          }

          
        })
      }).catch(res => {
        setLoading(false)
        toast.error(
          <CustomToast title="Email already taken" msg='It seems like the email you tried to register is already taken. Please try logging in instead.' icon={<Coffee size={12} />} color="danger" />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
      })
    }
  }

  const handleFirstnameChange = e => {
    const errs = valErrors
    if (errs.username) delete errs.firstname
    setFirstname(e.target.value)
    setValErrors(errs)
  }

  const handleLastnameChange = e => {
    const errs = valErrors
    if (errs.username) delete errs.lastname
    setLastname(e.target.value)
    setValErrors(errs)
  }

  const handleEmailChange = e => {
    const errs = valErrors
    if (errs.email) delete errs.email
    setEmail(e.target.value)
    setValErrors(errs)
  }

  return (
    <div className='auth-wrapper auth-v2'>
      <Row className='auth-inner m-0'>
        <Col className='align-items-center auth-bg' sm='12'>
          <Col className='px-xl-2 mx-auto' sm='8' md='6' lg='12'>
            <CardTitle tag='h2' className='font-weight-bold mb-1'>
              Learning adventure starts here 🚀
            </CardTitle>
            <CardText className='mb-2'>Create an account to get the full experience!</CardText>

            <Form action='/' className='auth-register-form mt-2' onSubmit={handleSubmit(onSubmit)}>
              <FormGroup>
                <Label className='form-label' for='firstname'>
                  Firstname
                </Label>
                <Input
                  autoFocus
                  type='text'
                  value={firstname}
                  placeholder='johndoe'
                  id='firstname'
                  name='firstname'
                  onChange={handleFirstnameChange}
                  className={classnames({ 'is-invalid': errors['firstname'] })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
                {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='lastname'>
                  Lastname
                </Label>
                <Input
                  type='text'
                  value={lastname}
                  placeholder='johndoe'
                  id='lastname'
                  name='lastname'
                  onChange={handleLastnameChange}
                  className={classnames({ 'is-invalid': errors['lastname'] })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
                {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='register-email'>
                  Email
                </Label>
                <Input
                  type='email'
                  value={email}
                  id='register-email'
                  name='register-email'
                  onChange={handleEmailChange}
                  placeholder='john@example.com'
                  className={classnames({ 'is-invalid': errors['register-email'] })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
                {Object.keys(valErrors).length && valErrors.email ? (
                  <small className='text-danger'>{valErrors.email}</small>
                ) : null}
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='register-password'>
                  Password
                </Label>
                <InputPasswordToggle
                  value={password}
                  id='register-password'
                  name='register-password'
                  className='input-group-merge'
                  onChange={e => setPassword(e.target.value)}
                  className={classnames({ 'is-invalid': errors['register-password'] })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
              </FormGroup>
              <FormGroup>
                <Label className='form-label' for='password_confirmation'>
                  Password Confirmation
                </Label>
                <InputPasswordToggle
                  value={password_confirmation}
                  id='password_confirmation'
                  name='password_confirmation'
                  className='input-group-merge'
                  onChange={e => setPassword_confirmation(e.target.value)}
                  className={classnames({ 'is-invalid': errors['password_confirmation'] })}
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
              </FormGroup>
              <FormGroup>
                <CustomInput
                  type='checkbox'
                  id='terms'
                  name='terms'
                  value='terms'
                  label={<Terms />}
                  className='custom-control-Primary'
                  innerRef={register({ required: true })}
                  onChange={e => setTerms(e.target.checked)}
                  invalid={errors.terms && true}
                />
              </FormGroup>
              <Button.Ripple type='submit' block color='primary'>
                {loading ? <> <Spinner size='sm' type='grow' />
                <span className='ml-50'>Loading...</span></> : 'Sign Up'}
              </Button.Ripple>
            </Form>
            <p className='text-center mt-2'>
              <span className='mr-25'>Already have an account?</span>
              <Link to='/login'>
                <span>Sign in instead</span>
              </Link>
            </p>
          </Col>
        </Col>
      </Row>
    </div>
  )
}

export default Register
