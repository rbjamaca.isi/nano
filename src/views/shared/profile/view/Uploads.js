// ** Custom Components
import moment from 'moment'

// ** Third Party Components
import { DollarSign, TrendingUp, User, Check, Star, Flag, Phone } from 'react-feather'
import { Card, Row, Col, Label, FormGroup, CardHeader, CardTitle, CardBody, Media, CardText, Button } from 'reactstrap'

const Uploads = ({ data }) => {
  return (
    <Card className='card-congratulations-medal'>
      <CardHeader>
        <CardTitle tag='h4' className='mb-2'>
          Proof of Payments
        </CardTitle>
      </CardHeader>
      <CardBody>
        {data.mode ? <>
      <FormGroup row>
              <Label sm='5'>
                  Mode of Payment
              </Label>
              <Col>
                  {data.mode}
              </Col>
        </FormGroup>
        <FormGroup row>
              <Label sm='5'>
                  Reference no.
              </Label>
              <Col>
                  {data.reference}
              </Col>
        </FormGroup>
        <CardText className='py-1 h5 text-primary'>
            <img className='img-fluid' src={`/storage/images/proof/${data.proof}`} height={600} />
          </CardText>
        </> : <> Free Member </>}
      </CardBody>
    </Card>
  )
}

export default Uploads
