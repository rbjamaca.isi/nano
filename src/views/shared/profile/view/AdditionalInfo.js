// ** React Imports
import React, { Fragment, useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router-dom'

// ** Third Party Components
import { toast, Slide } from 'react-toastify'
import {
    Card,
    CardHeader,
    CardTitle,
    CardBody,
    FormGroup,
    Col,
    Input,
    Form,
    Button,
    CustomInput,
    Label, Modal, ModalHeader, ModalBody, ModalFooter
} from 'reactstrap'
import { CustomToast } from 'utility/CustomToast'
import { Edit, Coffee } from 'react-feather'

// ** Store & Actions
import { API_ACTION } from '@src/data/api'

// ** Toastify
const ToastContent = ({ msg }) => (
    <React.Fragment>
        <div>
            <h5 className='toast-title font-weight-bold'>{msg}</h5>
        </div>
    </React.Fragment>
)

const AdditionalInfo = ({ data, onSubmit, role, id, modal, setModal }) => {
    const history = useHistory()
    const form_data = new FormData()
    const form_data_password = new FormData()
    const [modal_password, setModalPassword] = useState(false)
    const toggle = () => setModal(!modal)
    const toggle_password = () => setModalPassword(!modal_password)

    const [email, setEmail] = useState(data?.email),
        [street, setStreet] = useState(data?.street),
        [barangay, setBarangay] = useState(data?.barangay),
        [city, setCity] = useState(data?.city),
        [province, setProvince] = useState(data?.province),
        [postalcode, setPostalCode] = useState(data?.postalcode),
        [mobile, setMobile] = useState(data?.mobile),
        [password, setPassword] = useState(''),
        [password_confirmation, setPasswordConfirmation] = useState('')

    // ** Form Submit
    const handleUpdate = () => {
        form_data.append('email', email)
        form_data.append('street', street)
        form_data.append('barangay', barangay)
        form_data.append('city', city)
        form_data.append('province', province)
        form_data.append('postalcode', postalcode)
        form_data.append('mobile', mobile)
        form_data.append('id', id)
        API_ACTION(form_data, `/api/member-update`).then(res => {
            toast.success(
                <ToastContent msg="Successfully updated profile." />,
                { transition: Slide, hideProgressBar: true, autoClose: 2000 }
            )
            // location.reload()
        }).catch(err => {
            if (err.response.status === 422) {
                toast.error(
                    <CustomToast title="Email already taken" msg='It seems like the email you tried to register is already taken. Please use another email.' icon={<Coffee size={12} />} color="danger" />,
                  { transition: Slide, hideProgressBar: true, autoClose: 2000 }
                )
              } else {
                toast.error(
                  <CustomToast title="Error" msg="An error has occurred. Please try again later." icon={<Coffee size={12} />} color="danger" />,
                  { transition: Slide, hideProgressBar: true, autoClose: 2000 }
                )
              }
        })
    }


    // ** Form Submit Password
    const handleUpdatePassword = () => {
        form_data_password.append('id', id)
        form_data_password.append('password', password)

        console.log(password)

        API_ACTION(form_data_password, `/api/password`).then(res => {
            toast.success(
                <ToastContent msg="Successfully created new password." />,
                { transition: Slide, hideProgressBar: true, autoClose: 2000 }
            )
            location.reload()
        }).catch(err => {
            console.log(err)
        })
    }

    return (
        <Card>
            <CardHeader>
                <CardTitle tag='h4'>Personal Information</CardTitle>
            </CardHeader>

            <CardBody>
                <Form>
                    <FormGroup row>
                        <Label sm='5'>
                            Full Name
                        </Label>
                        <Col>
                            {data.firstname} {data.middlename} {data.lastname}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Civil Status
                        </Label>
                        <Col>
                            {data.civilstatus}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Date of Birth
                        </Label>
                        <Col>
                            {data.dob}
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label sm='5'>
                            Citizenship
                        </Label>
                        <Col>
                            {data.citizenship}
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label sm='5'>
                            Occupation/Job Title
                        </Label>
                        <Col>
                            {data.occupation}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Membership
                        </Label>
                        <Col>
                            {data.is_premium ? data.type : 'Non-Premium'}
                        </Col>
                    </FormGroup>
                    <hr />
                    <h4 className="py-50">Contact Information</h4>
                    <FormGroup row>
                        <Label sm='5'>
                            Address
                        </Label>
                        <Col>
                            {data.street} {data.barangay} {data.city}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Province
                        </Label>
                        <Col>
                            {data.province}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Postal Code
                        </Label>
                        <Col>
                            {data.postalcode}
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm='5'>
                            Mobile No.
                        </Label>
                        <Col>
                            {data.mobile}
                        </Col>
                    </FormGroup>
                    <hr />
                    <h4 className="py-50">Login Information</h4>
                    <FormGroup row>
                        <Label sm='5'>
                            Email Address
                        </Label>
                        <Col>
                            {data.email}
                        </Col>
                    </FormGroup>
                    <Button color='primary' onClick={() => setModalPassword(!modal_password)} outline>
                        <Edit className='d-block d-md-none' size={14} />
                        <span className='font-weight-bold d-none d-md-block'>Update Password</span>
                    </Button>

                    <Modal isOpen={modal} toggle={toggle}>
                        <ModalHeader toggle={toggle}>Update Information</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                                <Label sm='4'>
                                    Email Address
                                </Label>
                                <Col>
                                    <Input
                                        type='email'
                                        id='email'
                                        name='email'
                                        value={email}
                                        onChange={e => setEmail(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Street
                                </Label>
                                <Col>
                                    <Input
                                        type='text'
                                        id='street'
                                        name='street'
                                        value={street}
                                        onChange={e => setStreet(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Barangay
                                </Label>
                                <Col>
                                    <Input
                                        type='text'
                                        id='barangay'
                                        name='barangay'
                                        value={barangay}
                                        onChange={e => setBarangay(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    City
                                </Label>
                                <Col>
                                    <Input
                                        type='text'
                                        id='city'
                                        name='city'
                                        value={city}
                                        onChange={e => setCity(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Province
                                </Label>
                                <Col>
                                    <Input
                                        type='text'
                                        id='province'
                                        name='province'
                                        value={province}
                                        onChange={e => setProvince(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Postal Code
                                </Label>
                                <Col>
                                    <Input
                                        type='number'
                                        id='postalcode'
                                        name='postalcode'
                                        value={postalcode}
                                        onChange={e => setPostalCode(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Mobile Number
                                </Label>
                                <Col>
                                    <Input
                                        type='number'
                                        id='mobile'
                                        name='mobile'
                                        value={mobile}
                                        onChange={e => setMobile(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color='primary' onClick={() => handleUpdate()}>Save</Button>
                            <Button color='link' onClick={toggle}>Close</Button>
                        </ModalFooter>
                    </Modal>

                    <Modal isOpen={modal_password} toggle={toggle_password}>
                        <ModalHeader toggle={toggle}>Update Password</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                                <Label sm='4'>
                                    Password
                                </Label>
                                <Col>
                                    <Input
                                        type='password'
                                        id='password'
                                        name='password'
                                        value={password}
                                        onChange={e => setPassword(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm='4'>
                                    Confirm Password
                                </Label>
                                <Col>
                                    <Input
                                        type='password'
                                        id='password'
                                        value={password_confirmation}
                                        name='password_confirmation'
                                        onChange={e => setPasswordConfirmation(e.target.value)}
                                    />
                                </Col>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color='primary' disabled={password !== password_confirmation} onClick={() => handleUpdatePassword()}>Save New Password</Button>
                            <Button color='link' onClick={toggle_password}>Close</Button>
                        </ModalFooter>
                    </Modal>

                    {role === 'Admin' &&
                        <FormGroup className='mb-0' row>
                            <Col sm='5'>
                                <Button.Ripple color='secondary' type='reset' onClick={() => history.goBack()}>
                                    Back
                                </Button.Ripple>
                            </Col>
                            <Col>
                                <Button.Ripple className='mr-1' disabled={data.status !== 'Pending'} color='primary' type='button' onClick={() => onSubmit('Active')}>
                                    Approve
                                </Button.Ripple>
                                <Button.Ripple outline color='secondary' disabled={data.status !== 'Pending'} onClick={() => onSubmit("Inactive")}>
                                    Reject
                                </Button.Ripple>
                            </Col>
                        </FormGroup>
                    }
                </Form>
            </CardBody>
        </Card>
    )
}
export default AdditionalInfo
