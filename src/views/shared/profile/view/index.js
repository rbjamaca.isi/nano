// ** React Imports
import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom'

// ** Reactstrap
import { Row, Col, Alert, Breadcrumb, BreadcrumbItem } from 'reactstrap'

// ** User View Components
import PlanCard from './PlanCard'
import ProfileInfoCard from './ProfileInfoCard'
import UserTimeline from './UserTimeline'
import AdditionalInfo from './AdditionalInfo'

// ** Styles
import '@styles/react/apps/app-users.scss'

// ** Store & Actions
import { API_RETRIEVE } from '../@src/data/api'

const UserView = props => {
  // ** Vars
   const { id } = useParams()

  // ** States
  const [loading, setLoading] = useState(null)
  const [info, setInfo] = useState(null)
  const [name, setName] = useState(null)

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/member/${id}`)
      setLoading(false)
      setInfo(response.data.data.user)
      setName(`${response.data.data.user.firstname} ${response.data.data.user.lastname}`)
      console.log(response.data.data.user)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  return info && info !== null && info !== undefined ? (
    <div className='app-user-view'>
      <div style={{ paddingBottom: 20 }}>
       <Breadcrumb>
        <BreadcrumbItem>
          <Link to='/members'> Members </Link>
        </BreadcrumbItem>
        <BreadcrumbItem active>
          <Link to='#'> {info.firstname} {info.lastname}</Link>
        </BreadcrumbItem>
      </Breadcrumb>
      </div>
      <Row>
        <Col xl='9' lg='8' md='7'>
          <ProfileInfoCard selectedUser={info} name={name} />
        </Col>
        <Col xl='3' lg='4' md='5'>
          <PlanCard info={info} />
        </Col>
      </Row>
      <Row>
        <Col md='6'>
        <AdditionalInfo data={info} />
        </Col>
        <Col md='6'>
        <UserTimeline />
          {/* <PermissionsTable /> */}
        </Col>
      </Row>
      <Row>
        <Col sm='12'>
        </Col>
      </Row>
    </div>
  ) : (
    <Alert color='danger'>
      <h4 className='alert-heading'>User not found</h4>
      <div className='alert-body'>
        User with id: {id} doesn't exist. Check list of all Users: <Link to='/apps/user/list'>Users List</Link>
      </div>
    </Alert>
  )
}
export default UserView
