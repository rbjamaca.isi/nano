// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData, timestamp, withDiscount, pesoSign } from 'utility/Utils'

// ** Third Party Components
import { Link, useHistory } from 'react-router-dom'
import { Row, Col, Card, CardBody, CardImg, Badge, Button, CardTitle, CardText, Media } from 'reactstrap'
import Avatar from '@components/avatar'
import AvatarGroup from '@components/avatar-group'
import { Facebook, Twitter, Mail, Calendar, MapPin, User } from 'react-feather'
import Moment from 'react-moment'

// ** Styles
import '@styles/base/pages/page-knowledge-base.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'

const Availed = () => {
    // ** States
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [open, setOpen] = useState(false)
    const history = useHistory()

    // ** Get data on mount
    const getData = async () => {
        setLoading(true)
        try {
            const response = await API_RETRIEVE(`/api/training-availed`)
            setLoading(false)
            setData(response.data.trainings)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => { getData() }, [])

    const statusObj = {
        Approved: 'light-success',
        Pending: 'light-secondary',
        Rejected: 'light-warning'
    }

    const Content = ({ item, image }) => (
        <div class='col-xs-12 col-lg-12 col-md-12 col-sm-12'>
            <Col className='kb-search-content' key={item.id}>
                <Card className='card-developer-meetup'>
                    <div class='row'>
                        <div class="col d-flex justify-content-center bg-white">
                            <img class='p-1' style={{ objectFit: 'contain' }}
                                src={item.image ? `/storage/images/training/${item.image}` : image[Math.floor(Math.random() * image.length)].img}
                                width='100%' height='400' alt="" />
                        </div>
                        <div class="col d-flex justify-content-center pt-2">
                            <CardBody>
                                <div className='meetup-header d-flex align-items-center'>
                                    <div className='my-auto'>
                                        <CardTitle tag='h1' className='mb-25'>
                                            {item.title}
                                        </CardTitle>
                                        <CardText className='mb-0'>

                                        </CardText>
                                    </div>
                                </div>
                                <Media>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<Calendar size={18} />} />
                                    <Media body>
                                        <h6 className='mb-0'>
                                            <Moment format='LL'>
                                                {item.schedule_start}
                                            </Moment> - &nbsp;
                                            <Moment format='LL'>
                                                {item.schedule_end}
                                            </Moment>
                                        </h6>
                                        <small>
                                            <Moment format='LT'>
                                                {item.schedule_start}
                                            </Moment> - &nbsp;
                                            <Moment format='LT'>
                                                {item.schedule_end}
                                            </Moment>
                                        </small>
                                    </Media>
                                </Media>

                                <Media className='mt-2'>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<MapPin size={18} />} />
                                    <Media body>
                                        <h6 className='mb-0'>{item.location}</h6>
                                        {item.pivot.status === 'Approved' ? <small><Link to={`/training/link/${item.code}`}>Google Meet Link</Link></small> : <small>Google Meet</small>}
                                    </Media>
                                </Media>
                                <Media className='mt-2'>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<User size={18} />} />
                                    <Media body>
                                        <div style={{ marginTop: '7px', marginBottom: '7px' }}> Speakers </div>
                                        <ul>
                                            {item.speakers.map(speaker => (
                                                <li key={speaker.id}>
                                                    <h6 className='mb-0'>{speaker.name}</h6>
                                                    <small>
                                                        {speaker.occupation}
                                                    </small>
                                                </li>
                                            ))}
                                        </ul>
                                    </Media>
                                </Media>
                                <hr />
                                <span className='text-body mt-1'> Payment Information </span>
                                {item.pivot.payment_method !== null ? <>
                                    <div className='my-1'> {item.pivot.payment_method}
                                        <span> Ref no. {item.pivot.ref_no} </span>
                                    </div>
                                    <div className='mt-1'>
                                        Amount &nbsp;
                                        <sup className='font-medium-1 font-weight-bold text-primary mr-25'>&#8369;</sup>
                                        <span className='font-medium-5 font-weight-bolder text-primary'>
                                            {item.pivot.payment_amount}
                                        </span>
                                        <p className='mt-1'>Approval Status: &nbsp;
                                            <Badge className='text-capitalize' color={statusObj[item.pivot.status]} pill>
                                                {item.pivot.status}
                                            </Badge>
                                        </p>
                                    </div>
                                </> : <div className='my-1'>
                                    <span> Free Training </span>
                                </div>
                                }
                            </CardBody>
                        </div>
                    </div>
                </Card>
            </Col>
        </div>
    )

    const renderContent = () => {
        return data && data.map(item => {
            return <Content key={item.id} item={item} />
        })
    }

    if (!data) {
        return <div class="row mb-5 text-center">
            <div class="col-12 col-lg-6 mx-auto">
                <h2 class="mt-4 mb-2 fs-1 fw-bold">No Trainings Availed</h2>
            </div>
        </div>
    }

    return (
        <Card>
                <div id='knowledge-base-content'>
                    <Row className='kb-search-content-info match-height'>
                        {renderContent()}
                    </Row>
                </div>
        </Card>

    )

}

export default Availed
