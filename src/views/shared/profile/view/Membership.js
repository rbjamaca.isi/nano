// ** Custom Components
import Avatar from '@components/avatar'
import medal from '@src/assets/images/illustration/badge.svg'
import moment from 'moment'

// ** Third Party Components
import { Card, CardHeader, CardTitle, CardBody, Media, CardText, Button } from 'reactstrap'

const Membership = ({ data }) => {
  return (
    <Card className='card-congratulations-medal'>
      <CardHeader>
        <CardTitle tag='h4'>
          Membership
        </CardTitle>
      </CardHeader>
      <CardBody>
        <CardText className='h1 text-primary text-bold capitalize'>{data.coop && data.coop.coop ? data.coop.coop : 'COOP 1'}</CardText>
        <CardText className='font-small-3'>
        Member since {moment(data.created_at).format('MMMM YYYY')}
        </CardText>
        <Button.Ripple color='primary'>View Details</Button.Ripple>
        <img className='congratulation-medal' src={medal} alt='Medal Pic' />
      </CardBody>
    </Card>
  )
}

export default Membership
