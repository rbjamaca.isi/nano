import { useState } from 'react'
import { AlignJustify, Rss, Info, Image, Users, Edit } from 'react-feather'
import { Card, CardImg, Collapse, Navbar, Nav, NavItem, NavLink, Button } from 'reactstrap'
import grizzy from '@src/assets/images/avatars/grizzy.png'
import banner from '@src/assets/images/banner/banner-4.jpg'

// ** Custom Components
import Avatar from '@components/avatar'
import { timestamp } from 'utility/Utils'

const ProfileHeader = ({ data, name, update, setUpdate, setModal, modal }) => {
  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)

   // ** render user img
   const renderUserImg = () => {
    if (data !== null) {
      const stateNum = Math.floor(Math.random() * 6),
        states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary'],
        color = states[stateNum]
      return (
        <Avatar
          initials={name}
          color={color}
          className='rounded'
          content={name}
          contentStyles={{
            borderRadius: 0,
            fontSize: 'calc(36px)',
            width: '100%',
            height: '100%'
          }}
          style={{
            height: '90px',
            width: 'auto',
            padding: '20px'
          }}
        />
      )
    }
  }

  return (
    <Card className='profile-header mb-2'>
      <div className='bg-light'>
        <img className='rounded mx-auto d-block' src={`https://source.unsplash.com/1980x460/?banner,book,library?t=${timestamp}` || banner} />
      </div>
      <div className='position-relative'>
        <div className='profile-img-container d-flex align-items-center'>
          <div className='profile-img'>
            <img className='rounded img-fluid' src={data.photo ? `storage/images/${data.photo}` : grizzy} alt='Card image' />
          </div>
          <div className='profile-title ml-3'>
            <h2 className='text-primary'>{data.firstname} {data.middlename} {data.lastname}</h2>
            <p className='text-secondary'>Member No. {data.id}</p>
          </div>
        </div>
      </div>
      <div className='profile-header-nav'>
        <Navbar className='justify-content-end justify-content-md-between w-100' expand='md' light>
          <Button color='' className='btn-icon navbar-toggler' onClick={toggle}>
            <AlignJustify size={21} />
          </Button>
          <Collapse isOpen={isOpen} navbar>
            <div className='profile-tabs d-flex justify-content-between flex-wrap mt-1 mt-md-0'>
              <Button color='primary' onClick={() => setModal(!modal)} outline>
                <Edit className='d-block d-md-none' size={14} />
                <span className='font-weight-bold d-none d-md-block'>Update</span>
              </Button>
            </div>
          </Collapse>
        </Navbar>
      </div>
    </Card>
  )
}

export default ProfileHeader
