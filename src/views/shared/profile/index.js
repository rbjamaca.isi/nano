// ** React Imports
import { Fragment, useState, useEffect } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'
import { useForm } from 'react-hook-form'

// ** Utils
import { getUserData } from 'utility/Utils'

// ** Pages
import ProfileHeader from './ProfileHeader'
import AdditionalInfo from './view/AdditionalInfo'
import Uploads from './view/Uploads'
import Header from '@src/views/misc/Header'

// ** Third Party Components
import { DollarSign, TrendingUp, User, Check, Calendar, MapPin, Star, Flag, Phone } from 'react-feather'
import { Row, Col, Card, CardBody, CardHeader, CardTitle, CardText, Media, Badge, Button, Form, FormGroup, Label, Input } from 'reactstrap'
import Moment from 'react-moment'

// ** Custom Components
import Sidebar from '@components/sidebar'

// ** Styles
import '@styles/react/pages/page-profile.scss'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import { Slide, toast } from 'react-toastify'
import Avatar from '@core/components/avatar'

// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='toast-title font-weight-bold'>Success!</h6>
      </div>
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const Profile = () => {
  //  ** States
  const [data, setData] = useState(null)
  const [name, setName] = useState(null)
  const [block, setBlock] = useState(false)
  const [formdata, setFormdata] = useState([])
  const history = useHistory()

  const [modal, setModal] = useState(false)

  // ** Vars
  const { id } = useParams()
  const { register, errors, handleSubmit, trigger, getValues } = useForm()

  // ** Form Submit
  const onSubmit = (status) => {

    const formdata = getValues()

    formdata.status = status
    formdata.name = data.firstname
    formdata.email = data.email

    API_ACTION(formdata, `/api/admin/member/${id}`, 'put').then(res => {
      toast.success(
        <ToastContent msg="Successfully changed member status." />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      history.push('/members')
    }).catch(err => {
      console.log(err)
    })
  }

  useEffect(() => {
    console.log(formdata)
  }, [formdata])

  // ** Get data on mount
  const getData = async () => {
    try {
      const response = await API_RETRIEVE(`/api/admin/${id}`)
      setData(response.data.data.user)
      setName(`${response.data.data.user.firstname} ${response.data.data.user.lastname}`)
    } catch (err) {
      console.log(err)
    }
  }

  const statusObj = {
    Pending: 'light-warning',
    Active: 'light-success',
    Inactive: 'light-warning',
    Rejected: 'light-danger'
  }
  
  const renderContent = (data) => {
    console.log('this', data)
          if (data.length < 1) {
            return <div class="row mb-5 text-center">
                <div class="col-12 col-lg-6 mx-auto">
                    <h2 class="mt-4 mb-2 fs-1 fw-bold">No Trainings Availed</h2>
                </div>
            </div>
        }
        return data && data.map(item => {
            return <Content key={item.id} item={item} />
        })
    }

    const Content = ({ item, image }) => {
      
    return (<div class='col-xs-12 col-lg-12 col-md-12 col-sm-12'>
          <Col className='kb-search-content' key={item.id}>
              <Card className='card-developer-meetup'>
                
                  <div class='row'>
                      <div class="col d-flex justify-content-center bg-white">
                          <img class='p-1' style={{ objectFit: 'contain' }}
                              src={item.image ? `/storage/images/training/${item.image}` : image[Math.floor(Math.random() * image.length)].img}
                              width='100%' height='400' alt="" />
                      </div>
                      <div class="col d-flex justify-content-center pt-2">
                          <CardBody>
                              <div className='meetup-header d-flex align-items-center'>
                                  <div className='my-auto'>
                                      <CardTitle tag='h1' className='mb-25'>
                                          {item.title}
                                      </CardTitle>
                                      <CardText className='mb-0'>

                                      </CardText>
                                  </div>
                              </div>
                              <Media>
                                  <Avatar color='light-primary' className='rounded mr-1' icon={<Calendar size={18} />} />
                                  <Media body>
                                      <h6 className='mb-0'>
                                          <Moment format='LL'>
                                              {item.schedule_start}
                                          </Moment> - &nbsp;
                                          <Moment format='LL'>
                                              {item.schedule_end}
                                          </Moment>
                                      </h6>
                                      <small>
                                          <Moment format='LT'>
                                              {item.schedule_start}
                                          </Moment> - &nbsp;
                                          <Moment format='LT'>
                                              {item.schedule_end}
                                          </Moment>
                                      </small>
                                  </Media>
                              </Media>

                              <Media className='mt-2'>
                                  <Avatar color='light-primary' className='rounded mr-1' icon={<MapPin size={18} />} />
                                  <Media body>
                                      <h6 className='mb-0'>{item.location}</h6>
                                      {item.pivot.status === 'Approved' ? <small><Link to={`/training/link/${item.code}`}>Google Meet Link</Link></small> : <small>Google Meet</small>}
                                  </Media>
                              </Media>
                              <Media className='mt-2'>
                                  <Avatar color='light-primary' className='rounded mr-1' icon={<User size={18} />} />
                                  <Media body>
                                      <div style={{ marginTop: '7px', marginBottom: '7px' }}> Speakers </div>
                                      <ul>
                                          {item.speakers.map(speaker => (
                                              <li key={speaker.id}>
                                                  <h6 className='mb-0'>{speaker.name}</h6>
                                                  <small>
                                                      {speaker.occupation}
                                                  </small>
                                              </li>
                                          ))}
                                      </ul>
                                  </Media>
                              </Media>
                              <hr />
                              <span className='text-body mt-1'> Payment Information </span>
                              {item.pivot.payment_method !== null ? <>
                                  <div className='my-1'> {item.pivot.payment_method}
                                      <span> Ref no. {item.pivot.ref_no} </span>
                                  </div>
                                  <div className='mt-1'>
                                      Amount &nbsp;
                                      <sup className='font-medium-1 font-weight-bold text-primary mr-25'>&#8369;</sup>
                                      <span className='font-medium-5 font-weight-bolder text-primary'>
                                          {item.pivot.payment_amount}
                                      </span>
                                      <p className='mt-1'>Approval Status: &nbsp;
                                          <Badge className='text-capitalize' color={statusObj[item.pivot.status]} pill>
                                              {item.pivot.status}
                                          </Badge>
                                      </p>
                                  </div>
                              </> : <div className='my-1'>
                                  <span> Free Training </span>
                              </div>
                              }
                          </CardBody>
                      </div>
                  </div>
              </Card>
          </Col>
      </div>
  ) 
}

  useEffect(() => { getData() }, [])

  return (
    <Fragment>
       {getUserData()?.role !== 'Admin' &&
        <Card>
          <Header />
        </Card>
      }
      {data !== null ? (
        <div id='user-profile'>
          <Row>
            <Col sm='12'>
              <ProfileHeader data={data} name={name} role={getUserData()?.role} setModal={setModal} modal={modal} />
            </Col>
          </Row>
          <section id='profile-info'>
            <Row>
              <Col>
                <AdditionalInfo onSubmit={onSubmit} data={data} role={getUserData()?.role} id={id} setModal={setModal} modal={modal}/>
              </Col>
              <Col>
                <Row>
                  <Col>
                    <Uploads data={data} />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Card className='card-developer-meetup'>
                        <CardHeader>
                            <CardTitle tag='h4'>Trainings Availed</CardTitle>
                        </CardHeader>
                        {renderContent(data && data?.trainings)}
                    </Card>
                  </Col>
                </Row>
              </Col>
            </Row>
          </section>
        </div>
      ) : null}
    </Fragment>
  )
}

export default Profile
