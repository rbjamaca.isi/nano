import { Fragment } from 'react'
import { Row, Col } from 'reactstrap'
import FileUploaderRestrictions from './FileUploaderRestrictions'
import ExtensionsHeader from '@components/extensions-header'

import 'uppy/dist/uppy.css'
import '@uppy/status-bar/dist/style.css'
import '@styles/react/libs/file-uploader/file-uploader.scss'

const Uploader = ({endPoint, message, redirect}) => {
  return (
    <Fragment>
      <Row>
        <Col sm='12'>
          <FileUploaderRestrictions endPoint={endPoint} message={message} redirect={redirect} />
        </Col>
      </Row>
    </Fragment>
  )
}

export default Uploader
