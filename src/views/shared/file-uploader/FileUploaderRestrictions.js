import { useState, Fragment } from 'react'
import Uppy from '@uppy/core'
import { DragDrop, Dashboard, ProgressBar } from '@uppy/react'
import { Card, CardHeader, CardTitle, CardBody, CardText } from 'reactstrap'
import { handleFileSave } from 'redux/actions/file'
import { useDispatch } from 'react-redux'
import { store } from 'redux/storeConfig/store'
import { XHRUpload } from 'uppy'
import { getUserData } from 'utility/Utils'
const ReduxStore = require('@uppy/store-redux')
const defaultStore = require('@uppy/store-default')
import Avatar from '@components/avatar'
import { toast, Slide, Zoom, Flip, Bounce } from 'react-toastify'
import { Check, X, AlertTriangle, Info } from 'react-feather'
import { useHistory } from 'react-router-dom'

const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='text-success ml-50 mb-0'>Successful!</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)


const FileUpload = ({endPoint, message, redirect}) => {
  const history = useHistory()
  const dispatch = useDispatch()
  const uppy = new Uppy({
    debug: true,
    autoProceed: false,
    restrictions: {
      maxFileSize: 1000000,
      maxNumberOfFiles: 1,
      minNumberOfFiles: 1,
      allowedFileTypes: ['image/*', 'video/*']
    }
  })
  .use(XHRUpload, {
    endpoint: `${process.env.REACT_APP_API_BASE_URL}${endPoint}`,
    fieldName: 'my_file',
    formData: true,
    headers: {
      Authorization: `Bearer ${localStorage.getItem('accessToken')}`
    }
  })

  uppy.on('complete', res => {
    toast.success(
      <ToastContent msg={message} />,
      { transition: Slide, hideProgressBar: true, autoClose: 5000 }
    )
    history.push(redirect)
  })

  return (
    <div className='pb-50'>
        <Dashboard
          note='Clicking on upload will send your request to join the training'
          uppy={uppy}/>
    </div>
  )
}

export default FileUpload
