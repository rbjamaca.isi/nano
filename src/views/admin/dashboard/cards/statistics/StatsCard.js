import classnames from 'classnames'
import Avatar from '@components/avatar'
import { TrendingUp, User, Box, DollarSign } from 'react-feather'
import { Card, CardHeader, CardTitle, CardBody, CardText, Row, Col, Media } from 'reactstrap'
import { useEffect, useState } from 'react'
import { API_RETRIEVE } from 'data/api'

const StatsCard = ({ cols }) => {
  // const data = [
  //   {
  //     title: '230k',
  //     subtitle: 'Colections',
  //     color: 'light-primary',
  //     icon: <TrendingUp size={24} />
  //   },
  //   {
  //     title: '8.549k',
  //     subtitle: 'Claims',
  //     color: 'light-info',
  //     icon: <Box size={24} />
  //   },
  //   {
  //     title: '201k',
  //     subtitle: 'Members',
  //     color: 'light-danger',
  //     icon: <User size={24} />
  //   },
  //   {
  //     title: '1.3k',
  //     subtitle: 'Near Expiry',
  //     color: 'light-success',
  //     icon: <DollarSign size={24} />
  //   }
  // ]

  const returnIcon = (icon) => {
    if (icon === "Trending") return <TrendingUp size={24} />
    if (icon === "Box") return <Box size={24} />
    if (icon === "User") return <User size={24} />
    if (icon === "Dollar") return <DollarSign size={24} />
  }
  

  const [data, setData] = useState(null)
  const getData = async () => {
    await API_RETRIEVE('/api/admin/charts/overview').then(res => {
      setData(res.data.data.overview)
    })
  }
  
  useEffect(() => { getData() }, [])

  const renderData = () => {
    return data && data.map((item, index) => {
      const margin = Object.keys(cols)
      return (
        <Col
          key={index}
          {...cols}
          className={classnames({
            [`mb-2 mb-${margin[0]}-0`]: index !== data.length - 1
          })}
        >
          <Media>
            <Avatar color={item.color} icon={returnIcon(item.icon)} className='mr-2' />
            <Media className='my-auto' body>
              <h4 className='font-weight-bolder mb-0'>{item.title}</h4>
              <CardText className='font-small-3 mb-0'>{item.subtitle}</CardText>
            </Media>
          </Media>
        </Col>
      )
    })
  }

  return (
    <Card className='card-statistics'>
      <CardHeader>
        <CardTitle tag='h4'>Overview</CardTitle>
        <CardText className='card-text font-small-2 mr-25 mb-0'>Updated 1 month ago</CardText>
      </CardHeader>
      <CardBody className='statistics-body'>
        <Row>{renderData()}</Row>
      </CardBody>
    </Card>
  )
}

export default StatsCard
