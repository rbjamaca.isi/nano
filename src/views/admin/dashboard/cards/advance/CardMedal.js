import { Card, CardBody, CardText, Button } from 'reactstrap'
import medal from '@src/assets/images/illustration/badge.svg'

const CardMedal = () => {
  return (
    <Card className='card-congratulations-medal'>
      <CardBody>
        <h5>Welcome back John!</h5>
        <CardText className='py-1 h3 text-primary'>You have 6 new notifications.</CardText>
        <Button.Ripple color='primary'>View Notifications</Button.Ripple>
      </CardBody>
    </Card>
  )
}

export default CardMedal
