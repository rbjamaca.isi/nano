import { useContext } from 'react'
import { Row, Col } from 'reactstrap'
import CompanyTable from './CompanyTable'
import { ThemeColors } from '@src/utility/context/ThemeColors'
import Earnings from '@src/views/admin/dashboard/cards/analytics/Earnings'
import Collectibles from '@src/views/admin/dashboard/cards/analytics/Collectibles'
import CardMedal from '@src/views/admin/dashboard/cards/advance/CardMedal'
import CardMeetup from '@src/views/admin/dashboard/cards/advance/CardMeetup'
import StatsCard from '@src/views/admin/dashboard/cards/statistics/StatsCard'
import GoalOverview from '@src/views/admin/dashboard/cards/analytics/GoalOverview'
import RevenueReport from '@src/views/admin/dashboard/cards/analytics/RevenueReport'
import OrdersBarChart from '@src/views/admin/dashboard/cards/statistics/OrdersBarChart'
import ProfitLineChart from '@src/views/admin/dashboard/cards/statistics/ProfitLineChart'
import CardTransactions from '@src/views/admin/dashboard/cards/advance/CardTransactions'
import CardBrowserStates from '@src/views/admin/dashboard/cards/advance/CardBrowserState'

import '@styles/react/libs/charts/apex-charts.scss'
import '@styles/base/pages/dashboard-ecommerce.scss'

const EcommerceDashboard = () => {
  const { colors } = useContext(ThemeColors),
    trackBgColor = '#e9ecef'

  return (
    <div id='dashboard-ecommerce'>
      <Row className='match-height'>
        <Col xs='12'>
          <StatsCard cols={{ xl: '3', sm: '6' }} />
        </Col>
      </Row>
      <Row className='match-height'>
        {/* <Col lg='8' md='12'>
          <RevenueReport primary={colors.primary.main} warning={colors.warning.main} />
        </Col> */}
      </Row>
      <Row className='match-height'>
        {/* <Col lg='8' xs='12'>
          <CompanyTable />
        </Col> */}
      </Row>
    </div>
  )
}

export default EcommerceDashboard
