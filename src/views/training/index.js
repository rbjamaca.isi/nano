// ** React Imports
import { Fragment, useState, useEffect, useRef } from 'react'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData, timestamp, withDiscount, pesoSign } from 'utility/Utils'

// ** Third Party Components
import { Link, useHistory } from 'react-router-dom'
import Breadcrumbs from '@components/breadcrumbs'
import SearchTraining from './SearchTraining'
import { Row, Col, Card, CardBody, CardImg, Badge, Button, Form, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'
import { Facebook, Twitter, Mail, Search, HelpCircle, Coffee } from 'react-feather'
import Moment from 'react-moment'
import { Parallax, Background } from 'react-parallax'
import Header from '@src/views/misc/Header'
import ReactPaginate from 'react-paginate'

// ** Styles
import '@styles/base/pages/page-knowledge-base.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'
import moment from 'moment'

const Training = () => {
  // ** States
  const [data, setData] = useState(null)
  const [ads, setAds] = useState([])
  const [loading, setLoading] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  let userData
  const [open, setOpen] = useState(false)
  const history = useHistory()
  const myRef = useRef(null)


  // ** Function to toggle sidebar
  const toggleSidebar = () => setOpen(!open)

  //** User data
  useEffect(() => {
    if (isUserLoggedIn() !== null) {
      userData = JSON.parse(localStorage.getItem('userData'))
      if (getUserData().role === 'Admin') {
        history.push('/landing')
      }
    }
  }, [])

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/training`)
      const ads = await API_RETRIEVE(`/api/advert`)
      setLoading(false)
      setData(response.data.data.trainings)
      setAds(ads.data.data.adverts)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  // dummy data
  const image = [
    {
      id: 1,
      category: 'sales-automation',
      img: require('@src/assets/images/illustration/sales.svg').default,
      title: 'Sales Automation',
      desc: 'There is perhaps no better demonstration of the folly of image of our tiny world.'
    },
    {
      id: 2,
      category: 'marketing-automation',
      img: require('@src/assets/images/illustration/marketing.svg').default,
      title: 'Marketing Automation',
      desc: 'Look again at that dot. That’s here. That’s home. That’s us. On it everyone you love.'
    },
    {
      id: 3,
      category: 'api-questions',
      img: require('@src/assets/images/illustration/api.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 4,
      category: 'demand',
      img: require('@src/assets/images/illustration/demand.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 5,
      category: 'demand',
      img: require('@src/assets/images/illustration/email.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    },
    {
      id: 6,
      category: 'demand',
      img: require('@src/assets/images/illustration/personalization.svg').default,
      title: 'API Questions',
      desc: 'every hero and coward, every creator and destroyer of civilization.'
    }
  ]

  // ** Parallax
  const image1 =
    "https://images.unsplash.com/photo-1498092651296-641e88c3b057?auto=format&fit=crop&w=1778&q=60&ixid=dW5zcGxhc2guY29tOzs7Ozs%3D"

  const insideStyles = {
    background: "white",
    padding: 20,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)"
  }
  // ** End Parallax

  const statusObj = {
    Open: 'light-success',
    Closed: 'light-secondary',
    Ongoing: 'light-warning'
  }

  // ** Pagination
  const [currentPage, setCurrentPage] = useState(1)
  const [rowsPerPage, setRowsPerPage] = useState(10)

  // Logic for displaying data
  const indexOfLast = currentPage * rowsPerPage
  const indexOfFirst = indexOfLast - rowsPerPage
  const currentData = data && data.slice(indexOfFirst, indexOfLast)

  const handlePagination = page => {
    setCurrentPage(page.selected + 1)
  }

  const CustomPagination = () => {
    const count = Number((data && data.length / rowsPerPage).toFixed(0))

    return (
      <ReactPaginate
        pageCount={count || 1}
        nextLabel=''
        breakLabel='...'
        previousLabel=''
        activeClassName='active'
        breakClassName='page-item'
        breakLinkClassName='page-link'
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={page => handlePagination(page)}
        pageClassName={'page-item'}
        nextLinkClassName={'page-link'}
        nextClassName={'page-item next'}
        previousClassName={'page-item prev'}
        previousLinkClassName={'page-link'}
        pageLinkClassName={'page-link'}
        containerClassName={'pagination react-paginate justify-content-end p-1'}
      />
    )
  }

  const Content = ({ item, image }) => (
    <div class='col-xs-12 col-lg-6 col-md-12 col-sm-12'>
      <Col className='kb-search-content' key={item.id}>
        <Card>
          <Link to={getUserData()?.role !== 'Admin' ? `/training/preview/${item.id}` : `/admin/training/preview/${item.id}`}>
            <div class="col d-flex justify-content-center bg-white">
              <img class='p-1' style={{ objectFit: 'contain' }}
                src={item.image ? `/storage/images/training/${item.image}` : image[Math.floor(Math.random() * image.length)].img}
                width='100%' height='400' alt="" />
            </div>
            <CardBody className='text-center'>
              <h4>{item.title}</h4>
              <h6>
                <Moment format='lll'>
                  {item.schedule_start}
                </Moment> - &nbsp;
                <Moment format='lll'>
                  {item.schedule_end}
                </Moment>
              </h6>
              <p className='text-body'>Status: &nbsp;
                <Badge className='text-capitalize' color={moment(Date.parse(item.schedule_start)).isAfter(moment.now()) ? statusObj.Open : statusObj.Closed} pill>
                  {moment(Date.parse(item.schedule_start)).isAfter(moment.now()) ? "Open" : "Closed"}
                </Badge>
              </p>
              {/* <p className='text-body mt-1 mb-0'>{item.desc.length > 200 ? `${item.desc.substring(0, 200)}...` : item.desc}</p> */}
              <hr />
              <div className='plan-price mt-2'>
                {/* <span className='pricing-duration text-body ml-25'> Discounted fee </span>
                <sup className='font-medium-1 font-weight-bold text-primary mr-25'>&#8369;</sup>
                <span className='font-medium-5 font-weight-bolder text-primary'>
                  {withDiscount(item.fee, 0.20)}
                </span> | */}
                <span className='pricing-duration text-body ml-25'> Fee </span>
                <sup className='font-medium-1 font-weight-bold text-primary mr-25'>&#8369;</sup>
                <span className='font-medium-5 font-weight-bolder text-primary'>
                  {item.fee}
                </span>
              </div>
            </CardBody>
          </Link>
        </Card>
      </Col>
    </div>
  )

  const renderContent = () => {
    if (currentData.length > 0) {
      return currentData.map(item => {
        const titleCondition = item.title.toLowerCase().includes(searchTerm.toLowerCase()),
          descCondition = item.desc.toLowerCase().includes(searchTerm.toLowerCase())

        if (searchTerm.length < 1) {
          return <Content key={item.id} item={item} image={image} />
        } else if (titleCondition || descCondition) {
          return <Content key={item.id} item={item} image={image} />
        } else {
          return null
        }
      })
    } else {
      return <Col><div class="row mb-5 text-center">
        <div class="col-12 col-lg-6 mx-auto">
          <h4 class="mt-4 mb-2 fs-1 fw-bold">No Trainings.</h4>
        </div>
      </div></Col>
    }

  }

  if (!currentData) {
    return null
  }
  return (
    <Fragment>
      {getUserData()?.role !== 'Admin' &&
        <Card>
          <Header />
        </Card>
      }
      <div className='mx-2'>
        <Row>
          <Col className='mb-2'>
            <Parallax bgImage="/images/landing/training9.png" strength={200} blur={4} className='bg-white'>
              <div style={{ height: 800 }}>
                <div style={insideStyles}>
                  <h1 style={{ fontWeight: 900, fontSize: '3.6em' }} className="fs-1 fw-bold text-primary text-center">Changing Lives.</h1>
                  <h1 style={{ fontWeight: 900, fontSize: '3.6em' }} className="fs-1 fw-bold text-primary text-center">Building Knowledge.</h1>
                  <h1 style={{ fontWeight: 900, fontSize: '3.6em' }} className="fs-1 fw-bold text-primary text-center">Making Success.</h1>
                  <SearchTraining myRef={myRef} searchTerm={searchTerm} setSearchTerm={setSearchTerm} />

                </div>
              </div>
            </Parallax>
          </Col>
        </Row>
        <Row className='mb-2'>
          {/* <Col sm='2'>
            <CardImg
              src={`https://i.ibb.co/V3kpNf1/jennie.png`}
            />
          </Col> */}
          <Col>
            <div class="row mb-5 text-center" ref={myRef}>
              <div class="col-12 col-lg-6 mx-auto">
                <h2 class="mt-4 mb-2 fs-1 fw-bold">Trainings</h2>
              </div>
            </div>

            {/* <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={this.state.pageCount}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={'pagination'}
          activeClassName={'active'}
        /> */}

            {CustomPagination()}


            <div id='knowledge-base-content'>
              <Row className='kb-search-content-info match-height'>
                {renderContent()}
              </Row>
            </div>
          </Col>
        </Row>
        <section class="py-5">
          <div class="container">
            <div class="row mb-4">
              <div class="col-12 col-md-10 col-lg-9 mx-auto">
                <div class="row mb-5 text-center">
                  <div class="col-12 col-lg-6 mx-auto">
                    <h2 class="mb-4 fs-1 fw-bold">Our Speakers</h2>
                    <p class="lh-lg">NANO Training and Computer Services' team includes dynamic speakers and thought leaders who educate and inspire audiences across the country.</p>
                  </div>
                </div>
                {/* <div class="row">
                  <div class="col-12 col-lg-6 mb-4">
                    <div class="p-4 bg-white shadow-sm rounded">
                      <div class="row text-center">
                        <div class="col-12 mx-auto">
                          <img class="img-fluid rounded-circle" src="/images/speaker/dolly_bation.png" width='150px' height='150px' alt="" />
                          <h5 class="m-2 fw-bold">Dolly Bation</h5>
                          <p class="text-primary">CPA</p>
                        </div>
                      </div>
                      <p class="lh-lg fw-bold text-justify pb-2">
                        Mr. Bation’s exceptional experience in business management provides essential insights on how to get the right punch of opportunities and manage the risks it entails. His actual work practice offers an important understanding of how the business environment of an ever-changing world directs decision-making, both in the local and global market.
                      </p>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6 mb-4">
                    <div class="p-4 bg-white shadow-sm rounded">
                      <div class="row text-center">
                        <div class="col-12 mx-auto">
                          <img class="img-fluid rounded-circle" src="/images/speaker/jimbo_fuentes.png" alt="" width='150px' height='150px' />
                          <h5 class="m-2 fw-bold">Dr. Jimbo A. Fuentes</h5>
                          <p class="text-primary">Professor</p>
                        </div>
                      </div>
                      <p class="lh-lg fw-bold text-justify">
                        As a member of the academe and the actual business world, Dr. Fuentes shares important knowledge on business management opportunities and addressing issues related to production and costs. His actual fieldwork for more than five (5) years in the Philippine Ports Authority (PPA) provides insights into the importance of efficient processes while ensuring ...
                      </p>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6 mb-4">
                    <div class="p-4 bg-white shadow-sm rounded">
                      <div class="row text-center">
                        <div class="col-12 mx-auto">
                          <img class="img-fluid rounded-circle" src="/images/speaker/heidi_grace_mendoza.png" width='150px' height='150px' alt="" />
                          <h5 class="m-2 fw-bold">Heidi Grace Mendoza, CPA, DBM</h5>
                          <p class="text-primary">Capitol University’s Director for Business Development & Management</p>
                        </div>
                      </div>
                      <p class="lh-lg fw-bold text-justify">
                        Dr. Mendoza presented her dissertation paper SELF-LEADERSHIP AND CORE SELFEVALUATIONS: AN APPROACH TO A PERSONAL MANAGEMENT DEVELOPMENT PROGRAM as her first international presentation in Singapore, where it won Outstanding Paper. One of her first advising roles was with Dr. Anthony Dagang for the paper ENTREPRENEURIAL PROPENSITY OF BUSINESS STUDENTS OF A CITY IN SOUTHERN PHILIPPINES.
                      </p>
                    </div>
                  </div>
                  <div class="col-12 col-lg-6 mb-4">
                    <div class="p-4 bg-white shadow-sm rounded">
                      <div class="row text-center">
                        <div class="col-12 mx-auto">
                          <img class="rounded-circle" src="/images/speaker/estrella_luz_penaloza.png" width='150px' height='150' alt="" />
                          <h5 class="m-2 fw-bold">Ma. Estrella Luz R. Penaloza, MA</h5>
                          <p class="text-primary">Senior Economic Development Specialist, NEDA</p>
                        </div>
                      </div>
                      <p class="lh-lg fw-bold text-justify">
                        She is an economic development specialist specializing in public financial management, tourism industry development, and rural and regional development. She took up her Master in International Development at Nagoya University, Japan. She facilitated the Nagoya University course on Introduction to Filipino language and Philippine culture. At the same time, she finished her Bachelor of Arts in Economics at the University of San Carlos graduated as Cum Laude.
                      </p>
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
            <div class="text-center"><a class="btn btn-primary" href="#">Show more speakers</a></div>
          </div>
        </section>
        {/* <section class="py-5">
          <div class="container">
            <div class="row">
              <div class="col d-flex">
                <div class="my-auto">
                  <h2 class="mb-4 fs-1 fw-bold">About Us</h2>
                  <p class="mb-4 lh-lg d-flex align-items-center justify-content-center fs-3 text-justify">
                    Neilson and Wilson made up the name zas Digital Institute Training and Development Services (DIT.ADS) on June 19, 2019. The concept of the business started when Neilson, the son, and Wilson, the father attended the Seminar on Research held at Davao City. The two  (Neilson and Wilson) together with the support of the family members envisioned to help the students and teachers in the city of Cagayan de Oro and those who find difficulty in engaging research especially when a research paper required the use of descriptive and inferential statistical tools. The pioneered also anticipated that in the coming years digital training for research will be in demand and could be easily learned by anybody interested in research through the use of Statistical Package for the Social Sciences (SPSS) software. SPSS is software for editing and analyzing all sorts of data.
                  </p>
                  {getUserData()?.role === 'Admin' ? <a class="btn btn-primary" href="/about_us">Read More</a> : <a class="btn btn-primary" href="/about">Read More</a>}
                </div>
              </div>
            </div>
          </div>
        </section> */}
        <Row>
          <Col>
            <section className="py-5 bg-white">
              <div className="container">
                <div className="row">
                  <div className="col-12 col-lg-6 d-flex"><img className="img-fluid my-auto" src="https://i.ibb.co/Z6nPg8w/connect.pngg" alt="" /></div>

                  <div className="col-12 col-lg-6 mt-5 mt-lg-0 order-1 order-lg-0">
                    <div className="mb-5 pr-5">
                      <h2 className="mb-3 fw-bold fs-1 fw-bold">Connect with us on Social Media</h2>
                      <p className="lh-lg fw-bold">Stay up to date with events happening at NANO.</p>
                    </div>
                    <ul className="list-unstyled">
                      <li className="mb-4">
                        <div className='auth-footer-btn'>
                          <a href='https://www.facebook.com/DIT.ADS.net' target='_blank'><Button.Ripple color='facebook'><Facebook size={30} /></Button.Ripple></a>
                          {/* <span className='pl-2 fw-bold'>https://www.facebook.com/DIT.ADS.net</span> */}
                        </div>
                      </li>
                      <li className="mb-4">
                        <div className='auth-footer-btn'>
                          <Button.Ripple color='twitter'><Twitter size={30} /></Button.Ripple>
                          {/* <span className='pl-2 fw-bold'>https://www.twitter.com/DIT.ADS.net</span> */}
                        </div>
                      </li>
                      <li>
                        <div className='auth-footer-btn'>
                          <Button.Ripple color='google'><Mail size={30} /></Button.Ripple>
                          {/* <span className='pl-2 fw-bold'>NANO2019@gmail.com</span> */}
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </section>
          </Col>
        </Row>

        {/* <Row className='my-2 bg-white'>
          <div class="col d-flex justify-content-center">
              <img class="img-fluid"
                // src={`/storage/images/adverts/${ads[Math.floor(Math.random() * ads.length)].image}`}
                src='images/ads/milktea.jpg'
                width='150px' height='150px' alt="" />
          </div>

          <div class="col d-flex justify-content-center">
              <img class="img-fluid p-1"
                src='images/ads/printhub.jpg'
                width='200px' height='150px' alt="" />
          </div>

          <div class="col d-flex justify-content-center">
              <img class="img-fluid p-1"
                src='images/ads/sukidesu.png'
                width='150px' height='150px' alt="" />
          </div>
        </Row> */}
      </div>
      {getUserData()?.role !== 'Admin' &&
        <div className='p-2'>
          <p className='clearfix mb-0'>
            <span className='float-md-left d-block d-md-inline-block mt-25'>
              COPYRIGHT © {new Date().getFullYear()}{' '}
              <a href='https://1.envato.market/pixinvent_portfolio' target='_blank' rel='noopener noreferrer'>
                NANO
              </a>
              <span className='d-none d-sm-inline-block'>, All rights Reserved</span>
            </span>
          </p>
        </div>
      }
    </Fragment>
  )
}

export default Training
