// ** React Imports
import { Fragment, useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import classnames from 'classnames'

// ** Utils
import { getUserData, isUserLoggedIn, renderClient, timestamp, withDiscount } from 'utility/Utils'

// ** Third Party Components
import { toast, Slide } from 'react-toastify'
import Avatar from '@components/avatar'
import { Plus, Award, Check, Coffee, XOctagon } from 'react-feather'
import cmtImg from '@src/assets/images/portrait/small/avatar-s-6.jpg'
import ImageUploader from "react-images-upload"
import { Parallax, Background } from 'react-parallax'
import Header from '@src/views/misc/Header'

import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardImg,
  Badge,
  Media,
  Form,
  Button, FormGroup, Label, Input
} from 'reactstrap'
import FileUpload from '../../shared/file-uploader'

// ** Custom Components
import Sidebar from '@components/sidebar'

// ** Styles
import '@styles/base/pages/page-blog.scss'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'
import moment from 'moment'
import { useForm } from 'react-hook-form'
import { CustomToast } from 'utility/CustomToast'
import CustomInput from 'reactstrap/lib/CustomInput'
import { handleLogout } from 'redux/actions/auth'
import { useDispatch } from 'react-redux'

// ** Toastify
const ToastContent = ({ msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Check size={12} />} />
        <h6 className='toast-title font-weight-bold'>Successful!</h6>
      </div>
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const ToastContentError = ({ title, msg }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='danger' icon={<XOctagon size={12} />} />
        <h6 className='toast-title ml-50 mb-0'>{title}</h6>
      </div>
      {/* <small className='text-muted'>11 Min Ago</small> */}
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const TrainingPreview = () => {
  // ** States
  const [formdata, setFormdata] = useState([])
  const history = useHistory()
  const [loading, setLoading] = useState(null)
  const [open, setOpen] = useState(false)
  const { register, errors, handleSubmit, trigger, watch, getValues } = useForm()
  const [pictures, setPictures] = useState([])
  const [free, setFree] = useState(true)
  const [premium, setPremium] = useState(false)
  const [user, setUser] = useState(null)
  const dispatch = useDispatch()
  const [header, setHeader] = useState(null)

  const onDrop = picture => {
    setPictures(picture)
  }

  // ** Function to toggle sidebar
  const toggleSidebar = () => { 
    setOpen(!open) 
    setPictures([])
  }
  
  // ** Vars
  const { id } = useParams()

  useEffect(() => {
    console.log(formdata)
  }, [formdata])

  const [list, setList] = useState(null)
  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
      try {
        const response = await API_RETRIEVE(`/api/training/${id}`)
        setLoading(false)
        setList(response.data.data)
      } catch (err) {
        console.log(err)
    }
    try {
      const response = await API_RETRIEVE(`/api/auth-user`)
      setLoading(false)
      setUser(response.data.user)
    } catch (err) {
      console.log(err)
    }
  }

  // Form Submit
  const handleFormSubmit = (e) => {
    e.preventDefault()
    const form_data = new FormData()
    const formdata = getValues()
    
    for (const key in formdata) {
      if (free) {
        if (key !== 'payment_method' && key !== 'payment_amount' && key !== 'payment_ref_no') {
          form_data.append(key, formdata[key])
        }
      } else {
        form_data.append(key, formdata[key])
      }
    }
    if (free) {
      form_data.append('status', 'Approved')
    }
    if (pictures[0]) {
      form_data.append('my_file', pictures[0])
    }

    // if (isUserLoggedIn()) {
      if (formdata.reference) {
        API_ACTION(form_data, `/api/upload/proof`).then(res => {
          toast.success(
            <ToastContent msg="Submitted successfully. You will be informed when your request is approved" />,
            { transition: Slide, hideProgressBar: true, autoClose: 5000 }
          )
          dispatch(handleLogout())
          setPictures([])
          setOpen(false)
          history.push('/login')
          getData()
        }).catch(err => {
          if (err?.response?.data?.errors?.reference) {
            toast.error(
              <CustomToast title="Invalid Reference No." msg="The reference number you provided is not valid" icon={<XOctagon size="12"/>} color="warning" />,
              { transition: Slide, hideProgressBar: true, autoClose: 2000 }
            )
          } else {
            toast.error(
              <ToastContentError title="Error" msg="An error has occured. Please try again later." icon={<XOctagon size="12"/>} color="warning" />,
              { transition: Slide, hideProgressBar: true, autoClose: 2000 }
            )
          }
        })
        return true
      }
      API_ACTION(form_data, `/api/training/proof/${id}`).then(res => {
        toast.success(
          <ToastContent msg="Submitted successfully. You will be informed when your request is approved" />,
          { transition: Slide, hideProgressBar: true, autoClose: 5000 }
        )
        setOpen(false)
        getData()
      }).catch(err => {
        if (err?.response?.data?.errors?.ref_no) {
          toast.error(
            <CustomToast title="Invalid Reference No." msg="The reference number you provided is not valid" icon={<XOctagon size="12"/>} color="warning" />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        } else {
          toast.error(
            <ToastContentError title="Error" msg="An error has occured. Please try again later." icon={<XOctagon size="12"/>} color="warning" />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        }
      })
  }

  const isFormValid = () => {
    const { payment_method, payment_amount, ref_no } = watch()
    return free || (pictures?.length > 0
          && payment_method?.length > 0
          && payment_amount?.length > 0
          && ref_no?.length > 0)
  }
  

  useEffect(() => { 
    getData() 
    setHeader(Math.floor(Math.random() * (15 - 2 + 1)) + 2)
  }, [])

     // ** Form Submit Status
     const handleStatus = async (id, status) => {
      // const form_data = new FormData()

      console.log(status)
      // form_data.append('status', status)

  
      await API_ACTION(status, `api/admin/training-status/${id}`, 'put')
          toast.success(
              <ToastContent msg="Successfully updated status." />,
              { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
          // location.reload()
  }


  // dummy data
  const data =
  {
    training_name: 'The Best Features Coming to iOS and Web design',
    avatar: require('@src/assets/images/portrait/small/avatar-s-7.jpg').default,
    userFullName: 'Ghani Pradita',
    createdTime: 'Jan 10, 2020',
    desc:
      '<p class="card-text mb-2">Before you get into the nitty-gritty of coming up with a perfect title, start with a rough draft: your working title. What is that, exactly? A lot of people confuse working titles with topics. Let\'s clear that Topics are very general and could yield several different blog posts. Think "raising healthy kids," or "kitchen storage." A writer might look at either of those topics and choose to take them in very, very different directions.A working title, on the other hand, is very specific and guides the creation of a single blog post. For example, from the topic "raising healthy kids," you could derive the following working title See how different and specific each of those is? That\'s what makes them working titles, instead of overarching topics.</p>'
  }

  // apila lang ni
  const sample = {
    img: require(`@src/assets/images/banner/banner-${Math.floor(Math.random() * 19) + 1}.jpg`).default,
    tags: ['Gaming', 'Video'],
    another_desc: '<h4>Topics</h4><ul><li>Preliminary thinking systems</li><li>Bandwidth efficient</li><li>Green space</li><li>Social impact</li><li>Thought partnership</li><li>Fully ethical life</li></ul>'
  }

  const badgeColorsArr = {
    Quote: 'light-info',
    Fashion: 'light-primary',
    Gaming: 'light-danger',
    Video: 'light-warning',
    Food: 'light-success'
  }

  const handleChange = () => {
    const values = getValues()
  }
  

  const renderTags = (list) => {
    const states = ['light-success', 'light-danger', 'light-warning', 'light-info', 'light-primary', 'light-secondary']
    return list.tags && list.tags.map((tag, index) => {
      const stateNum = Math.floor(Math.random() * 6)
      const color = states[stateNum]
      return (
        <a key={index} href='/' onClick={e => e.preventDefault()}>
          <Badge
            className={classnames({
              'mr-50': index !== list.tags.length - 1
            })}
            color={color}
            pill
          >
            {tag.desc}
          </Badge>
        </a>
      )
    })
  }

  const renderParagraphs = (desc) => {
    return desc.split('\n').map((str, index) => <p key={index}>{str}</p>)
  }

  const registerRedirect = () => {
    toast.warning(
      <CustomToast msg="You need to register first before you can proceed" title="Unregistered User" icon={<Coffee size="12"/>} color="warning" />,
      { transition: Slide, hideProgressBar: true, autoClose: 5000 }
    )
    history.push('/register')
  }

  const joinTraining = async () => {
    if (!isUserLoggedIn()) {
      registerRedirect()
      return false
    }
    API_RETRIEVE(`/api/member/training/join/${id}`).then(res => {
      toast.success(
        <ToastContent msg="Submitted successfully. You will receive an email for the link of the training" />,
        { transition: Slide, hideProgressBar: true, autoClose: 5000 }
      )
      setOpen(false)
      getData()
    }).catch(err => {
    
        toast.error(
          <ToastContentError title="Error" msg="An error has occured. Please try again later." icon={<XOctagon size="12"/>} color="warning" />,
          { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
    })
  }

  const insideStyles = {
    // background: "white",
    padding: 20,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)"
  }
  

  return (
    <Fragment>
       {getUserData()?.role !== 'Admin' &&
        <Card>
          <Header />
        </Card>
      }
      <div class={getUserData()?.role !== 'Admin' && 'mx-2' }>
      {list !== null ? (
        <Row>
          <Col>
            <Card className='mb-3'>
              {/* <CardImg src={`https://source.unsplash.com/1600x900/?learning`} className='img-fluid' top /> */}
              {/* {header && <Parallax bgImage={`/images/landing/${header}.jpg`} strength={300} className='bg-white'>
              <div style={{ height: 800 }}>
                <div style={insideStyles}>
                  <h1 style={{fontSize: '3.6em'}} className="fs-1 fw-bold text-white">{list.title}</h1>
                </div>
              </div>
            </Parallax>} */}
            <div class="col d-flex justify-content-center bg-white py-2">
            <img class='p-1' style={{ objectFit: 'contain' }}
              src={list.image ? `/storage/images/training/${list.image}` : image[Math.floor(Math.random() * image.length)].img}
              width='100%' height='400' alt="" />
          </div>
          <h1 style={{fontSize: '2.6em'}} className="fs-1 fw-bold d-flex justify-content-center ">{list.title}</h1>
              <CardBody>
                <CardTitle tag='h4'>{list.training}</CardTitle>
                <div className='my-1 py-25'>{renderTags(list)}</div>
                <div
                  
                >{renderParagraphs(list.desc)}</div>
                {/* <div
                  dangerouslySetInnerHTML={{
                    __html: sample.another_desc
                  }}
                ></div> */}
                <h5 className='mb-75'>Speaker(s):</h5>
                <Media>
                {list.speakers.map(speaker => (
                  <div key={speaker.id}>
                  {/* {renderClient(speaker)} */}
                  <Media body style={{display: 'inline-grid'}}>
                    
                        <h6 className='font-weight-bolder'>{speaker.name}</h6>
                        <CardText className='mb-1'>
                        {speaker.occupation}
                      </CardText>
                     
                  
                  </Media>
                  </div>
                ))}
                </Media>
                {/* <div className='my-1 py-25'>Certificates</div>
                <Media>
                  <Award size={60} />
                  <Media body className='pl-2'>
                    <h6 className='font-weight-bolder'>{data.cert ? data.cert : 'Certificate of Attendance'}</h6>
                    <CardText className='mb-0'>
                      Based in London, Uncode is a blog by Willie Clark. His posts explore modern design trends
                      through photos and quotes by influential creatives and web designer around the world.
                    </CardText>
                  </Media>
                </Media> */}
              </CardBody>
            </Card>
          </Col>
          <Col sm='12'>
            <Card>
              <CardBody>
                <h5 className='mb-75'>Location/Mode:</h5>
                <CardText>{list.location ? list.location : 'Virtual'}</CardText>
                <div className='mt-2'>
                  <h5 className='mb-75'>Schedule:</h5>
                  <CardText>{moment(list.schedule_start).format('lll')} - {moment(list.schedule_end).format('lll')}</CardText>
                </div>
                <div className='mt-2'>
                  <h5 className='mb-75'>Fee:</h5>
                  {!list.is_free && <CardText>&#8369; {list.fee && isUserLoggedIn() && getUserData().is_premium ? withDiscount(list.fee, 0.20) : list.fee}</CardText>}
                  {(list.is_free === 1 || list.price === 0) && <CardText>Free</CardText>}
                </div>
                {!list.is_free ? <>{moment(Date.parse(list.schedule_start)).isAfter(moment.now()) ? <div>
                {(isUserLoggedIn() && getUserData().role !== 'Admin') ? <Button className='my-2' color='primary' onClick={() => setOpen(true)}>
                  <Plus size={14} /> Join Training
                </Button> : !isUserLoggedIn() ? (
                <Button className='my-2' color='primary' onClick={registerRedirect}>
                  <Plus size={14} /> Join Training
                </Button>) : null
                }
                </div> : <div><Button className='my-2' color='primary' disabled>Registration Closed</Button>
               {(isUserLoggedIn() && getUserData().role !== 'Admin') && <p><i>Registration for this training is already closed. Click <a href='/home'>here</a> to select another training.</i></p>}
                </div>
                }
                </> : (isUserLoggedIn() && getUserData().role !== 'Admin') ? <Button className='my-2' color='primary' onClick={joinTraining}>
                  <Plus size={14} /> Join Training
                </Button> : (
                <Button className='my-2' color='primary' onClick={registerRedirect}>
                  <Plus size={14} /> Join Training
                </Button>)}
              </CardBody>
            </Card>
            <div class="text-center"><a class="btn btn-outline-primary" href='/home'>Select another training</a></div>

            <Sidebar
              size='lg'
              open={open}
              title='Upload Proof of Payment'
              headerClassName='mb-1'
              contentClassName='p-0'
              toggleSidebar={toggleSidebar}
            >
              <Form onSubmit={handleFormSubmit}>
                  <>
                   <FormGroup>
                      <Label className='form-label'>
                        <h6>Name</h6>
                      </Label>
                      <Input name="firstname" innerRef={{readOnly: true}} value={isUserLoggedIn() ? `${getUserData().firstname} ${getUserData().lastname}` : ''} onChange={handleChange} placeholder='John' />
                    </FormGroup>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Email</h6>
                      </Label>
                      <Input name="email" innerRef={{readOnly: true}} value={isUserLoggedIn() ? getUserData().email : ''} onChange={handleChange} placeholder='youremail@email.com' />
                    </FormGroup>
                  </>
                  {isUserLoggedIn() && user?.free_webinars > 0 ? <>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>You have {user?.free_webinars} free {user?.free_webinars > 1 ? 'webinars' : 'webinar'} left. Use one for this training?</h6>
                      </Label>
                      <ul className='other-payment-options list-unstyled'>
                        <li className='py-50'>
                          <CustomInput type='radio' name="free" onClick={() => setFree(true)} label='Yes' value={free} id='free-true' />
                        </li>
                        <li className='py-50'>
                          <CustomInput type='radio' name="free" onClick={() => setFree(false)} label='No' value={free} id='free-false' />
                        </li>
                      </ul>
                    </FormGroup>
                  </> : !user?.is_premium ? (
                    <>
                    {/* <FormGroup>
                      <Label className='form-label'>
                        <h6>You have {user?.free_webinars} free {user?.free_webinars > 1 ? 'webinars' : 'webinar'} left. Upgrade to Premium?</h6>
                      </Label>
                      <ul className='other-payment-options list-unstyled'>
                        <li className='py-50'>
                          <CustomInput type='radio' name="free" onClick={() => setPremium(true)} label='Yes' value={free} id='free-true' />
                        </li>
                        <li className='py-50'>
                          <CustomInput type='radio' name="free" onClick={() => setPremium(false)} label='No' value={free} id='free-false' />
                        </li>
                      </ul>
                    </FormGroup> */}
                    </>
                  ) : (
                    <>
                    </>
                  )}
                  {/* {!user?.is_premium && premium && (
                  <>
                      <FormGroup>
                        <Label className='form-label'>
                          <h6>Membership Payment Method</h6>
                        </Label>
                        <Input name="mode" innerRef={register({ required: true})} onChange={handleChange} placeholder='Gcash, BPI, Paymaya' />
                      </FormGroup>
                      <FormGroup>
                        <Label className='form-label'>
                          <h6>Amount</h6>
                        </Label>
                        <Input type="number" name="payment_amount" value={250} innerRef={register({ required: true})} innerProps={{readOnly:true}} onChange={handleChange} placeholder={250} />
                      </FormGroup>
                      <FormGroup>
                        <Label className='form-label'>
                          <h6>Reference Number</h6>
                        </Label>
                        <Input name="reference" innerRef={register({ required: true})} onChange={handleChange} placeholder="9876-543-21" />
                      </FormGroup>
                  <ImageUploader
                      withPreview={true}
                      withIcon={true}
                      onChange={onDrop}
                      imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                      maxFileSize={5242880}
                      singleImage={true}
                    />
                  </>
                  )} */}
                  {((user?.is_premium && user?.free_webinars <= 0) || (!free) || (!user?.is_premium && user?.free_webinars <= 0)) && (
                  <>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Training Payment Method</h6>
                      </Label>
                      <Input name="payment_method" innerRef={register({ required: true})} onChange={handleChange} placeholder='Gcash, BPI, Paymaya' />
                    </FormGroup>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Amount</h6>
                      </Label>
                      <Input type="number" name="payment_amount" value={list.fee && isUserLoggedIn() && getUserData().is_premium ? withDiscount(list.fee, 0.20) : list.fee} innerRef={register({ required: true})} innerProps={{readOnly:true}} onChange={handleChange} placeholder={list.fee} />
                    </FormGroup>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Reference Number</h6>
                      </Label>
                      <Input name="ref_no" innerRef={register({ required: true})} onChange={handleChange} placeholder="9876-543-21" />
                    </FormGroup>
                <ImageUploader
                    withPreview={true}
                    withIcon={true}
                    onChange={onDrop}
                    imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                    maxFileSize={5242880}
                    singleImage={true}
                  />
                </>
                )}
                {/* {(user?.is_premium && user?.is_premium && user?.free_webinars <= 0) || (user?.is_premium && !free && (
                  <>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Training Payment Method</h6>
                      </Label>
                      <Input name="payment_method" innerRef={register({ required: true})} onChange={handleChange} placeholder='Gcash, BPI, Paymaya' />
                    </FormGroup>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Amount</h6>
                      </Label>
                      <Input type="number" name="payment_amount" value={list.fee && isUserLoggedIn() && getUserData().is_premium ? withDiscount(list.fee, 0.20) : list.fee} innerRef={register({ required: true})} innerProps={{readOnly:true}} onChange={handleChange} placeholder={list.fee} />
                    </FormGroup>
                    <FormGroup>
                      <Label className='form-label'>
                        <h6>Reference Number</h6>
                      </Label>
                      <Input name="ref_no" innerRef={register({ required: true})} onChange={handleChange} placeholder="9876-543-21" />
                    </FormGroup>
                <ImageUploader
                    withPreview={true}
                    withIcon={true}
                    onChange={onDrop}
                    imgExtension={[".jpg", ".gif", ".png", ".gif"]}
                    maxFileSize={5242880}
                    singleImage={true}
                  />
                </>
                ))} */}
                {/* <FileUpload endPoint={`/api/training/proof/${id}`} message="Successfully uploaded proof of payment" redirect="/training" /> */}
                <FormGroup className='d-flex flex-wrap mt-2'>
                  <Button type='submit' className='mr-1' color='primary' disabled={!isFormValid()} >
                    Join
                  </Button>
                  <Button color='secondary' onClick={() => setOpen(false)} outline>
                    Cancel
                  </Button>
                </FormGroup>
              </Form>
            </Sidebar>
          </Col>
        </Row>
      ) : null}
      </div>
      {getUserData()?.role !== 'Admin' &&
        <div className='p-2'>
          <p className='clearfix mb-0'>
            <span className='float-md-left d-block d-md-inline-block mt-25'>
              COPYRIGHT © {new Date().getFullYear()}{' '}
              <a href='https://1.envato.market/pixinvent_portfolio' target='_blank' rel='noopener noreferrer'>
                NANO
              </a>
              <span className='d-none d-sm-inline-block'>, All rights Reserved</span>
            </span>
          </p>
        </div>
      }
    </Fragment>
  )
}

export default TrainingPreview
