import * as yup from 'yup'
import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty, selectThemeColors } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { yupResolver } from '@hookform/resolvers/yup'
import { Form, Label, Input, FormGroup, Row, Col, Button, CustomInput } from 'reactstrap'

import CreatableSelect from 'react-select/creatable'
import makeAnimated from 'react-select/animated'

const TrainingDetails = ({ stepper, setFormdata, tagData, data, status, handleSubmit, trigger, errors }) => {

  const animatedComponents = makeAnimated()

  // const { data, errors, handleSubmit, trigger } = useForm()
  const [tags, setTags] = useState([])
  const [update_status, setUpdateStatus] = useState(status)

  const handleTagsChange = (newValue) => {
    setTags(newValue)
  }

  const handleStatusChange = (e) => {
    setUpdateStatus(e.target.value)
  }

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(form => ({ ...form, ...data, tags, update_status }))
      stepper.next()
    }
  }

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Training Details</h5>
        <small className='text-muted'>Enter Training Details.</small>
      </div>
      {data !== null ? (
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Row>
            {/* <FormGroup tag={Col} md='12'>
              <Label for='status'>Status</Label>
            <ul className='other-payment-options list-unstyled'>
          <li className='py-50'>
            <CustomInput type='radio' innerRef={data({ required: true })} checked={update_status === 'Closed'} onChange={handleStatusChange} label='Closed' value="Closed" name='type' id='closed' />
          </li>
          <li className='py-50'>
            <CustomInput type='radio' innerRef={data({ required: true })} checked={update_status === 'Open'} onChange={handleStatusChange} label='Open' value="Open" name='type' id='open' />
          </li>
        </ul>
        </FormGroup> */}
            <FormGroup tag={Col} md='12'>
              <Label className='form-label' for='title'>
                Training Name
              </Label>
              <Input
                name='title'
                id='title'
                placeholder='Training Name'
                // value={data.title}
                innerRef={data({ required: true })}
                className={classnames({ 'is-invalid': errors['title'] })}
              />
            </FormGroup>
            <FormGroup tag={Col}>
              <Label className='form-label' for='link'>
                Training Link
              </Label>
              <Input
                name='link'
                id='link'
                placeholder='Link'
                // value={data.link}
                innerRef={data({ required: true })}
                className={classnames({ 'is-invalid': errors['link'] })}
                />
            </FormGroup>
            <FormGroup tag={Col} md='12'>
              <Label className='form-label' for='name'>
                Description
              </Label>
              <Input
                name='desc'
                id='desc'
                type='textarea'
                placeholder='Description'
                // value={data.desc}
                innerRef={data({ required: true })}
                className={classnames({ 'is-invalid': errors['desc'] })}
              />
            </FormGroup>
            <FormGroup tag={Col} md='12'>
              <Label>Tags</Label>
              <CreatableSelect
                isClearable={false}
                theme={selectThemeColors}
                closeMenuOnSelect={false}
                onChange={handleTagsChange}
                components={animatedComponents}
                isMulti
                defaultValue={data.tags}
                options={tagData}
                className='react-select'
                classNamePrefix='select'
              />
            </FormGroup>
          </Row>
          <div className='d-flex justify-content-end'>
            <Button.Ripple type='submit' color='primary' className='btn-next'>
              <span className='align-middle d-sm-inline-block d-none'>Next</span>
              <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
            </Button.Ripple>
          </div>
        </Form>
      ) : null}
    </Fragment>
  )
}

export default TrainingDetails
