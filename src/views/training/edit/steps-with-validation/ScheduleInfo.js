import { Fragment, useState } from 'react'
import Select from 'react-select'
import classnames from 'classnames'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { selectThemeColors, isObjEmpty } from '@utils'
import { Label, FormGroup, Row, Col, Button, Form, Input } from 'reactstrap'
import Flatpickr from 'react-flatpickr'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/flatpickr/flatpickr.scss'
import { DateTimeRangePicker  } from 'react-datetime-range-super-picker'
import 'react-datetime-range-super-picker/dist/index.css'

const ScheudleInfo = ({ stepper, setFormdata, data, handleSubmit, trigger, errors }) => {
  // ** States
  const [picker, setPicker] = useState(new Date())
  const [from_date, setFromDate] = useState(new Date())
  const [to_date, setToDate] = useState(new Date())

  const handleFromDateUpdate = ({date}) => {
    setFromDate(date.date)
  }

  const handleToDateUpdate = ({date}) => {
    setToDate(date.date)
  }

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(form => ({ ...form, ...data, sched: [from_date, to_date] }))
      stepper.next()
    }
  }
  

  const civilstatusOptions = [
    { value: 'Single', label: 'Single' },
    { value: 'Married', label: 'Married' },
    { value: 'Widowed', label: 'Widowed' },
    { value: 'Divorced', label: 'Divorced' },
    { value: 'Others', label: 'Others' }
  ]

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Schedule Info</h5>
        <small>Add Schedule Information.</small>
      </div>
      {data !== null ? (
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='date-time-picker'>
             Training Schedule
            </Label>
            {/* <Flatpickr
              data-enable-time
              value={picker}
              id='date-time-picker'
              className='form-control'
              onChange={date => setPicker(date)}
              value={picker}
              // options={{
              //   mode: 'range'
              //   // defaultDate: [new Date(), '2021-04-19']
              // }}
            /> */}
            <DateTimeRangePicker  from_date={from_date} to_date={to_date}
              onFromDateTimeUpdate={handleFromDateUpdate} 
              onToDateTimeUpdate={handleToDateUpdate} />
          </FormGroup>
        </Row>
        <Row>
          <FormGroup tag={Col}>
            <Label className='form-label' for='slots'>
            Slots
            </Label>
            <Input
              type='number'
              name='slots'
              id='slots'
              placeholder='Slots'
              // value={data.slots}
              innerRef={data({ required: true })}
              className={classnames({ 'is-invalid': errors['slots'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='fee'>
            Fee
            </Label>
            <Input
              type='number'
              name='fee'
              id='fee'
              placeholder='Fee'
              // value={data.fee}
              innerRef={data({ required: true })}
              className={classnames({ 'is-invalid': errors['fee'] })}
            />
          </FormGroup>
          <FormGroup tag={Col}>
            <Label className='form-label' for='location'>
            Location
            </Label>
            <Input
              name='location'
              id='location'
              placeholder="Enter 'Virtual' if it's virtual training"
              // value={data.location}
              innerRef={data({ required: true })}
              className={classnames({ 'is-invalid': errors['location'] })}
            />
          </FormGroup>
        </Row>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
      ) : null}
    </Fragment>
  )
}

export default ScheudleInfo
