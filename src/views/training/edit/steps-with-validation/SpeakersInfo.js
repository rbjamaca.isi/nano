import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty } from '@utils'
import { useForm } from 'react-hook-form'
import Repeater from '@components/repeater'
import { Row, Col, Card, CardHeader, CardBody, Form, FormGroup, Label, Input, Button } from 'reactstrap'
import { X, Plus, ArrowLeft, ArrowRight } from 'react-feather'

const SpeakersInfo = ({ stepper, setFormdata, handleFormSubmit, data }) => {
  const { register, errors, handleSubmit, trigger } = useForm()
  const [count, setCount] = useState(0)

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      handleFormSubmit({speakers: data})
    }
  }

  const increaseCount = () => {
    setCount(count + 1)
  }

  const deleteForm = e => {
    e.preventDefault()
    e.target.closest('form').remove()
  }


  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Speakers Info</h5>
        <small>Add Speakers Information.</small>
      </div>
      {data !== null ? (
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Repeater count={count}>
          {i => (
            <Form key={i}>
              <Row className='justify-content-between align-items-center'>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Full Name</Label>
                    <Input type='text' id={`item-name-${i}`} name={`${i}.name`} innerRef={register({ required: true })} placeholder='Full Name' />
                  </FormGroup>
                </Col>
                <Col>
                  <FormGroup>
                    <Label for={`item-name-${i}`}>Profession</Label>
                    <Input type='text' id={`item-name-${i}`} name={`${i}.occupation`} innerRef={register({ required: true })} placeholder='Profession' />
                  </FormGroup>
                </Col>
                <Col md={2}>
                  <Button.Ripple color='danger' className='text-nowrap px-1' onClick={deleteForm} outline>
                    <X size={14} className='mr-50' />
                    <span>Delete</span>
                  </Button.Ripple>
                </Col>
                <Col sm={12}>
                  <hr />
                </Col>
              </Row>
            </Form>
          )}
        </Repeater>
        <Button.Ripple className='btn-icon mb-2' color='primary' outline onClick={increaseCount}>
          <Plus size={14} />
          <span className='align-middle ml-25'>Add New</span>
        </Button.Ripple>
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' color='success' className='btn-submit'>
            Submit
          </Button.Ripple>
        </div>
      </Form>
      ) : null}
    </Fragment>
  )
}

export default SpeakersInfo
