// ** React Imports
import { Fragment, useState, useEffect } from 'react'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData, timestamp, withDiscount, pesoSign } from 'utility/Utils'

// ** Third Party Components
import { Link, useHistory } from 'react-router-dom'
import Breadcrumbs from '@components/breadcrumbs'
import SearchTraining from '../SearchTraining'
import { Row, Col, Card, CardBody, CardImg, Badge, Button, CardTitle, CardText, Media } from 'reactstrap'
import Avatar from '@components/avatar'
import AvatarGroup from '@components/avatar-group'
import { Facebook, Twitter, Mail, Calendar, MapPin, User } from 'react-feather'
import Moment from 'react-moment'
import { Parallax, Background } from 'react-parallax'
import Header from '@src/views/misc/Header'

// ** Styles
import '@styles/base/pages/page-knowledge-base.scss'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'
import moment from 'moment'

const Availed = () => {
    // ** States
    const [data, setData] = useState(null)
    const [ads, setAds] = useState([])
    const [loading, setLoading] = useState(false)
    const [searchTerm, setSearchTerm] = useState('')
    let userData
    const [open, setOpen] = useState(false)
    const history = useHistory()

    // ** Function to toggle sidebar
    const toggleSidebar = () => setOpen(!open)

    //** User data
    useEffect(() => {
        if (isUserLoggedIn() !== null) {
            userData = JSON.parse(localStorage.getItem('userData'))
            if (getUserData().role === 'Admin') {
                history.push('/landing')
            }
        }
    }, [])

    // ** Get data on mount
    const getData = async () => {
        setLoading(true)
        try {
            const response = await API_RETRIEVE(`/api/training-availed`)
            setLoading(false)
            setData(response.data.trainings)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => { getData() }, [])

    const statusObj = {
        Approved: 'light-success',
        Pending: 'light-secondary',
        Rejected: 'light-warning'
    }

    const Content = ({ item, image }) => (
        <div class='col-xs-12 col-lg-12 col-md-12 col-sm-12'>
            <Col className='kb-search-content' key={item.id}>
                <Card className='card-developer-meetup'>
                    <div class='row'>
                        <div class="col d-flex justify-content-center bg-white">
                            <img class='p-1' style={{ objectFit: 'contain' }}
                                src={item.image ? `/storage/images/training/${item.image}` : image[Math.floor(Math.random() * image.length)].img}
                                width='100%' height='400' alt="" />
                        </div>
                        <div class="col d-flex justify-content-center pt-2">
                            <CardBody>
                                <div className='meetup-header d-flex align-items-center'>
                                    <div className='my-auto'>
                                        <CardTitle tag='h1' className='mb-25'>
                                            {item.title}
                                        </CardTitle>
                                        <CardText className='mb-0'>

                                        </CardText>
                                    </div>
                                </div>
                                <Media>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<Calendar size={18} />} />
                                    <Media body>
                                        <h6 className='mb-0'>
                                            <Moment format='LL'>
                                                {item.schedule_start}
                                            </Moment> - &nbsp;
                                            <Moment format='LL'>
                                                {item.schedule_end}
                                            </Moment>
                                        </h6>
                                        <small>
                                            <Moment format='LT'>
                                                {item.schedule_start}
                                            </Moment> - &nbsp;
                                            <Moment format='LT'>
                                                {item.schedule_end}
                                            </Moment>
                                        </small>
                                    </Media>
                                </Media>

                                <Media className='mt-2'>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<MapPin size={18} />} />
                                    <Media body>
                                        <h6 className='mb-0'>{item.location}</h6>
                                        {item.pivot.status === 'Approved' ? <small><Link to={`/training/link/${item.code}`}>Google Meet Link</Link></small> : <small>Google Meet</small>}
                                    </Media>
                                </Media>
                                <Media className='mt-2'>
                                    <Avatar color='light-primary' className='rounded mr-1' icon={<User size={18} />} />
                                    <Media body>
                                        <div style={{ marginTop: '7px', marginBottom: '7px' }}> Speakers </div>
                                        <ul>
                                            {item.speakers.map(speaker => (
                                                <li key={speaker.id}>
                                                    <h6 className='mb-0'>{speaker.name}</h6>
                                                    <small>
                                                        {speaker.occupation}
                                                    </small>
                                                </li>
                                            ))}
                                        </ul>
                                    </Media>
                                </Media>
                                <hr />
                                <span className='text-body mt-1'> Payment Information </span>
                                {item.pivot.payment_method !== null ? <>
                                <div className='my-1'> {item.pivot.payment_method}
                                    <span> Ref no. {item.pivot.ref_no} </span>
                                </div>
                                <div className='mt-1'>
                                    Amount &nbsp;
                                    <sup className='font-medium-1 font-weight-bold text-primary mr-25'>&#8369;</sup>
                                    <span className='font-medium-5 font-weight-bolder text-primary'>
                                        {item.pivot.payment_amount}
                                    </span>
                                    <p className='mt-1'>Approval Status: &nbsp;
                                        <Badge className='text-capitalize' color={statusObj[item.pivot.status]} pill>
                                            {item.pivot.status}
                                        </Badge>
                                    </p>
                                </div>
                                </> : <div className='my-1'>
                                    <span> Free Training </span>
                                </div>
                                }
                            </CardBody>
                        </div>
                    </div>
                </Card>
            </Col>
        </div>
    )

    const renderContent = () => {
        return data && data.map(item => {
            const titleCondition = item.title.toLowerCase().includes(searchTerm.toLowerCase()),
                descCondition = item.desc.toLowerCase().includes(searchTerm.toLowerCase())

            if (searchTerm.length < 1) {
                return <Content key={item.id} item={item} />
            } else if (titleCondition || descCondition) {
                return <Content key={item.id} item={item} />
            } else {
                return null
            }
        })
    }

    if (!data) {
        return  <div class="row mb-5 text-center">
        <div class="col-12 col-lg-6 mx-auto">
            <h2 class="mt-4 mb-2 fs-1 fw-bold">No Trainings Availed</h2>
        </div>
    </div>
    }
    return (
        <Fragment>
            {getUserData()?.role !== 'Admin' &&
                <Card>
                    <Header />
                </Card>
            }
                <div class="ml-2"><a class="btn btn-primary mb-2" onClick={() => history.push('/home')}>Home</a></div>

            <div className='mx-2'>
                <Row className='mb-2'>
                    <Col>
                        <div class="row mb-5 text-center">
                            <div class="col-12 col-lg-6 mx-auto">
                                <h2 class="mt-4 mb-2 fs-1 fw-bold">Trainings Availed</h2>
                                <SearchTraining searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
                            </div>
                        </div>

                        <div id='knowledge-base-content'>
                            <Row className='kb-search-content-info match-height'>
                                {renderContent()}
                            </Row>
                        </div>
                    </Col>
                </Row>
                <div class="text-center"><a class="btn btn-outline-primary mb-2" href='/home'>Select another training</a></div>

                <Row className='my-2'>
                    <div class="col d-flex justify-content-center bg-white">
                        {ads.length > 0 &&
                            <img class="img-fluid"
                                src={`/storage/images/adverts/${ads[Math.floor(Math.random() * ads.length)].image}`}
                                width='150px' height='150px' alt="" />}
                    </div>
                    <div class="col d-flex justify-content-center bg-white">
                        {ads.length > 0 &&
                            <img class="img-fluid"
                                src={`/storage/images/adverts/${ads[Math.floor(Math.random() * ads.length)].image}`}
                                width='150px' height='150px' alt="" />}
                    </div>
                    <div class="col d-flex justify-content-center bg-white">
                        {ads.length > 0 &&
                            <img class="img-fluid"
                                src={`/storage/images/adverts/${ads[Math.floor(Math.random() * ads.length)].image}`}
                                width='150px' height='150px' alt="" />}
                    </div>
                </Row>
            </div>
            {getUserData()?.role !== 'Admin' &&
                <div className='p-2'>
                    <p className='clearfix mb-0'>
                        <span className='float-md-left d-block d-md-inline-block mt-25'>
                            COPYRIGHT © {new Date().getFullYear()}{' '}
                            <a href='https://1.envato.market/pixinvent_portfolio' target='_blank' rel='noopener noreferrer'>
                                NANO
                            </a>
                            <span className='d-none d-sm-inline-block'>, All rights Reserved</span>
                        </span>
                    </p>
                </div>
            }
        </Fragment>
    )
}

export default Availed
