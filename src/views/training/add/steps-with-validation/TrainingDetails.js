import * as yup from 'yup'
import { Fragment, useState } from 'react'
import classnames from 'classnames'
import { isObjEmpty, selectThemeColors } from '@utils'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { yupResolver } from '@hookform/resolvers/yup'
import { Form, Label, Input, FormGroup, Row, Col, Button, CustomInput } from 'reactstrap'

import CreatableSelect from 'react-select/creatable'
import makeAnimated from 'react-select/animated'

const colorOptions = [
  { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
  { value: 'blue', label: 'Blue', color: '#0052CC', isFixed: true },
  { value: 'purple', label: 'Purple', color: '#5243AA', isFixed: true },
  { value: 'red', label: 'Red', color: '#FF5630', isFixed: false },
  { value: 'orange', label: 'Orange', color: '#FF8B00', isFixed: false },
  { value: 'yellow', label: 'Yellow', color: '#FFC400', isFixed: false }
]

const TrainingDetails = ({ stepper, setFormdata, tagData }) => {
 
  const animatedComponents = makeAnimated()

  const { register, errors, handleSubmit, trigger } = useForm()
  const [tags, setTags] = useState([])

  const handleChange = (newValue) => {
    setTags(newValue)
  }

  const onSubmit = (data) => {
    trigger()
    if (isObjEmpty(errors)) {
      setFormdata(form => ({...form, ...data, tags}))
      stepper.next()
    }
  }
  

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Training Details</h5>
        <small className='text-muted'>Enter Training Details.</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <FormGroup tag={Col} md='12'>
            <Label className='form-label' for='title'>
              Training Name
            </Label>
            <Input
              name='title'
              id='title'
              placeholder='Training Name'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['title'] })}
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'>
            <Label className='form-label' for='link'>
              Training Link
            </Label>
            <Input
              name='link'
              id='link'
              placeholder='Link'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['link'] })}
            />
          </FormGroup>
          <FormGroup tag={Col} md='6'style={{marginTop: '2rem'}}>
              Click <a href="https://meet.google.com/new" target="_blank">here</a> to create a <b>Google Meet</b> link
          </FormGroup>
          <FormGroup tag={Col} md='12'>
            <Label className='form-label' for='name'>
              Training Overview
            </Label>
            <Input
              name='desc'
              id='desc'
              type='textarea'
              placeholder='Training Overview'
              innerRef={register({ required: true })}
              className={classnames({ 'is-invalid': errors['desc'] })}
            />
          </FormGroup>
          {/* <FormGroup tag={Col} md="12">
          <Label className='form-label' for='name'>
              Free or Paid
          </Label>
          <ul className='other-payment-options list-unstyled'>
            <li className='py-50'>
              <CustomInput type='radio' innerRef={register({ required: true })} label='Paid' value={false} name='is_free' id='free-true' />
            </li>
            <li className='py-50'>
              <CustomInput type='radio' innerRef={register({ required: true })} label='Free' value={true} name='is_free' id='free-false' />
            </li>
          </ul>
          </FormGroup> */}
          <FormGroup tag={Col} md='12'>
          <Label>Tags</Label>
            <CreatableSelect
              isClearable={false}
              theme={selectThemeColors}
              closeMenuOnSelect={false}
              onChange={handleChange}
              components={animatedComponents}
              isMulti
              options={tagData}
              className='react-select'
              classNamePrefix='select'
            />
            </FormGroup>
        </Row>
        <div className='d-flex justify-content-end'>
          <Button.Ripple type='submit' color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default TrainingDetails
