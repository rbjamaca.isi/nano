import { Fragment, useState } from 'react'
import Select from 'react-select'
import classnames from 'classnames'
import { useForm } from 'react-hook-form'
import { ArrowLeft, ArrowRight } from 'react-feather'
import { selectThemeColors, isObjEmpty } from '@utils'
import { Label, FormGroup, Row, Col, Button, Form, Input } from 'reactstrap'
import Flatpickr from 'react-flatpickr'

// ** Styles
import '@styles/react/libs/react-select/_react-select.scss'
import '@styles/react/libs/flatpickr/flatpickr.scss'
import ImageUploader from "react-images-upload"

const CertificatesInfo = ({ stepper, setFormdata }) => {
  // ** States
  const { register, errors, handleSubmit, trigger } = useForm()
  const [pictures, setPictures] = useState([])

  const onSubmit = (data) => {
    trigger()
    if (pictures.length > 0) {
      setFormdata(form => ({ ...form, image: pictures[0] }))
      stepper.next()
    }
  }

  const onDrop = picture => {
    setPictures(picture)
  }

  const isFormValid = () => {
    return pictures.length > 0
  }

  return (
    <Fragment>
      <div className='content-header'>
        <h5 className='mb-0'>Image Display</h5>
        <small>Add Custom Image Display</small>
      </div>
      <Form onSubmit={handleSubmit(onSubmit)}>
      <ImageUploader
          withPreview={true}
          withIcon={true}
          onChange={onDrop}
          imgExtension={[".jpg", ".gif", ".png", ".gif"]}
          maxFileSize={8242880}
          singleImage={true}
        />
        <div className='d-flex justify-content-between'>
          <Button.Ripple color='primary' className='btn-prev' onClick={() => stepper.previous()}>
            <ArrowLeft size={14} className='align-middle mr-sm-25 mr-0'></ArrowLeft>
            <span className='align-middle d-sm-inline-block d-none'>Previous</span>
          </Button.Ripple>
          <Button.Ripple type='submit' disabled={!isFormValid()} color='primary' className='btn-next'>
            <span className='align-middle d-sm-inline-block d-none'>Next</span>
            <ArrowRight size={14} className='align-middle ml-sm-25 ml-0'></ArrowRight>
          </Button.Ripple>
        </div>
      </Form>
    </Fragment>
  )
}

export default CertificatesInfo
