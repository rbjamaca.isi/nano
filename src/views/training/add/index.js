// ** React Imports
import React, { useRef, useState, useEffect } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { getUserData } from 'utility/Utils'

// ** Third Party Components
import { Breadcrumb, BreadcrumbItem } from 'reactstrap'
import { toast, Slide } from 'react-toastify'

// ** Pages
import Wizard from '@components/wizard'
import CertificatesInfo from './steps-with-validation/CertificatesInfo'
import SpeakersInfo from './steps-with-validation/SpeakersInfo'
import ScheduleInfo from './steps-with-validation/ScheduleInfo'
import TrainingDetails from './steps-with-validation/TrainingDetails'

// ** Store & Actions
import { API_ACTION, API_RETRIEVE } from '@src/data/api'

// ** Toastify
const ToastContent = ({ msg }) => (
  <React.Fragment>
    <div>
      <h5 className='toast-title font-weight-bold'>{msg}</h5>
    </div>
    {/* <div>
      <span>{msg}</span>
    </div> */}
  </React.Fragment>
)

const TrainingAdd = () => {
  // ** States
  const [stepper, setStepper] = useState(null)
  const ref = useRef(null)
  const [formdata, setFormdata] = useState([])
  const history = useHistory()
  const [tags, setTags] = useState(null)

  // ** Vars
  const { id } = getUserData()
  // Form

  const handleFormSubmit = ({speakers}) => {
    formdata.speakers = speakers
    const form_data = new FormData()
    for (const key in formdata) {
        form_data.append(key, Array.isArray(formdata[key]) || key === "speakers" ? JSON.stringify(formdata[key]) : formdata[key])
    }
    API_ACTION(form_data, `/api/training`).then(res => {
      toast.success(
        <ToastContent msg="Successfully added training" />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      history.push('/trainings')
    }).catch(err => {
      console.log(err)
    })
  }


  const getTags = () => {
    API_RETRIEVE('api/tags').then(res => {
      setTags(res.data.data.tags)
    })
  }
  

  useEffect(() => {
    getTags()
  }, [])

  const steps = [
    {
      id: 'training-details',
      title: 'Training Details',
      subtitle: 'Enter Training Details.',
      content: <TrainingDetails tagData={tags} setFormdata={setFormdata} stepper={stepper} />
    },
    {
      id: 'schedule-info',
      title: 'Schedule Info',
      subtitle: 'Add Schedule Info',
      content: <ScheduleInfo setFormdata={setFormdata} stepper={stepper} />
    },
    {
      id: 'cert-info',
      title: 'Image Display',
      subtitle: 'Add Custom Image Display',
      content: <CertificatesInfo setFormdata={setFormdata} stepper={stepper} />
    },
    {
      id: 'speaker-info',
      title: 'Speakers Info',
      subtitle: 'Add Speakers Info',
      content: <SpeakersInfo setFormdata={setFormdata} handleFormSubmit={handleFormSubmit} stepper={stepper} />
    }
  ]

  return (
    <>
      {/* <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/members'> <h5 className="text-primary">Members</h5> </Link>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <Link to='/coopmembers-add'> <h5>Add New Member</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div> */}
      <div className='vertical-wizard' style={{overflowX: 'scroll'}}>
      <Wizard
        type='vertical'
        ref={ref}
        steps={steps}
        options={{
          linear: false
        }}
        instance={el => setStepper(el)}
      />
    </div>
    </>
  )
}

export default TrainingAdd
