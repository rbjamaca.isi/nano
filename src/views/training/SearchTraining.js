import { Search } from 'react-feather'
import { Card, CardBody, CardText, Form, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'

const FaqFilter = ({ searchTerm, setSearchTerm, handleFilter, myRef }) => {
  const onChange = e => {
    if (handleFilter) {
      handleFilter(e)
    } else {
      setSearchTerm(e.target.value)
    }
  }

  const executeScroll = (e) => {
    e.preventDefault()
    myRef.current.scrollIntoView()  
    setSearchTerm("")  
  }

  return (
    // <div id='knowledge-base-search'>
    //   <Card className='knowledge-base-bg'>
    //     <CardBody className='text-center'>
    //       <h2 className='text-primary'>Trainings</h2>
    //       <CardText className='mb-2'>
    //         Popular searches: <span className='font-weight-bolder'>Sales automation, Email marketing</span>
    //       </CardText>
          <Form className='kb-search-input' onSubmit={executeScroll}>
            <InputGroup className='input-group-merge'>
              <InputGroupAddon addonType='prepend'>
                <InputGroupText>
                  <Search size={14} />
                </InputGroupText>
              </InputGroupAddon>
              <Input value={searchTerm} onChange={e => onChange(e)} placeholder='Search Training Title...' />
            </InputGroup>
          </Form>
    //     </CardBody>
    //   </Card>
    // </div>
  )
}

export default FaqFilter
