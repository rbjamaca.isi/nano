// ** React Imports
import { memo } from 'react'
import { Link } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Third Party Components
import Moment from 'react-moment'
import {
  Table,
  Badge,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
  UncontrolledTooltip,
  Card,
  Button,
  CardHeader,
  CardTitle,
  CardBody
} from 'reactstrap'
import {
  Eye,
  TrendingUp,
  Send,
  MoreVertical,
  Download,
  Edit,
  User,
  Trash,
  Copy,
  CheckCircle,
  Save,
  ArdataDownCircle,
  Info,
  PieChart
} from 'react-feather'
import moment from 'moment'

const CoopMembersTable = ({ userData, list, loading }) => {

  // ** Account Confirmation
  const handleConfirmation = () => {
    // 
  }

  const statusObj = {
    Open: 'light-success',
    Closed: 'light-secondary',
    Ongoing: 'light-warning'
  }

  return (
    <Card>
      {/* <CardHeader>
        <CardTitle tag='h4'>List of Members</CardTitle>
      </CardHeader> */}
      <Table striped hover bordered responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Price</th>
            <th>Attendees</th>
            <th>Start</th>
            <th>End</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {list && list.map(data => (
            <tr key={data.id}>
              <td>
                {data.id}
              </td>
              <td>
                {data.title}
              </td>
              <td>
                {data.fee}
              </td>
              <td>
                {data.attendees_count}
              </td>
              <td>
              <Moment format="LLL">
                  {data.schedule_start}
                </Moment>
              </td>
              <td>
                <Moment format="LLL">
                  {data.schedule_end}
                </Moment>
              </td>
              <td>
              <Badge className='text-capitalize' color={moment(Date.parse(data.schedule_start)).isAfter(moment.now()) ? statusObj.Open : statusObj.Closed} pill>
                {/* {item.status} */}
                {moment(Date.parse(data.schedule_start)).isAfter(moment.now()) ? "Open" : "Closed"}
              </Badge>
              </td>
              <td>
                   <Link
                  to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/admin/training/preview/${data.id}` : `/training/preview/${data.id}`)}
                  id={`pw-tooltip-${data.id}`}>
                    <Eye size={17} className='mx-1' />
                  </Link>
                  <UncontrolledTooltip placement='top' target={`pw-tooltip-${data.id}`}>
                    Preview Training
                  </UncontrolledTooltip>

                  <Link
                  to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/admin/training/edit/${data.id}` : `/training/edit/${data.id}`)}
                  id={`edit-tooltip-${data.id}`}>
                    <Edit size={17} className='mx-1' />
                  </Link>
                  <UncontrolledTooltip placement='top' target={`edit-tooltip-${data.id}`}>
                    Update Training
                  </UncontrolledTooltip>

                  <Link
                  to={`/trainings/attendees/${data.id}`}
                  id={`attendees-tooltip-${data.id}`}>
                    <User size={17} className='mx-1' />
                  </Link>
                  <UncontrolledTooltip placement='top' target={`attendees-tooltip-${data.id}`}>
                    View Attendees
                  </UncontrolledTooltip>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
  )
}

export default memo(CoopMembersTable)
