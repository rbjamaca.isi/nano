// ** React Imports
import { Fragment, useEffect, useState } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'

// ** Utils
import { selectThemeColors, isUserLoggedIn, getUserData } from 'utility/Utils'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'

// ** Third Party Components
import {
  Card, CardHeader, CardBody, Input, Row, Col, Label, CustomInput, Button, Badge, FormGroup, Table as Tble,
  Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, ModalFooter, UncontrolledTooltip
} from 'reactstrap'
import {
  Eye,
  Edit,
  User, Power
} from 'react-feather'
import Moment from 'react-moment'
import moment from 'moment'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Tables
import Table from '@src/views/misc/Table'

// ** Store & Actions
import { API_RETRIEVE, API_ACTION } from '@src/data/api'
import { Slide, toast } from 'react-toastify'

// ** Toastify
const ToastContent = ({ msg, role }) => (
  <Fragment>
    <div className='toastify-header'>
    </div>
    <div className='toastify-body'>
      <span>{msg}</span>
    </div>
  </Fragment>
)

const Trainings = () => {
  // ** State
  const [loading, setLoading] = useState(null)
  const history = useHistory()
  const form_data = new FormData()

  const [data, setData] = useState([])
  const [filteredData, setFilteredData] = useState([])
  const [currentPage, setCurrentPage] = useState(0)
  const [searchName, setSearchName] = useState('')
  const [searchStatus, setSearchStatus] = useState('')
  const [searchSubscription, setSearchSubscription] = useState('')
  const [currentSubscription, setcurrentSubscription] = useState({ value: 'all', label: 'All' })

  const [modal, setModal] = useState(false)
  const toggle = () => setModal(!modal)
  const [reload, setReload] = useState(false)
  const [btnLoading, setBtnLoading] = useState(false)
  const [state, setState] = useState({ selectedFile: [] })

  // ** Get data on mount
  const getData = async () => {
    setLoading(true)
    try {
      const response = await API_RETRIEVE(`/api/admin/trainings`)
      setLoading(false)
      setData(response.data.data.trainings)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => { getData() }, [])

  // ** Batch Upload 
  const batchUpload = () => {
    setBtnLoading(true)
    toast.success(
      <ToastContent msg="Uploading Members... Please wait" />,
      { transition: Slide, hideProgressBar: true, autoClose: 2000 }
    )
    const formData = new FormData()
    // Update the formData object
    formData.append(
      "file",
      state.selectedFile,
      state.selectedFile.name
    )
    // Details of the uploaded file
    console.log(state.selectedFile)
    API_ACTION(formData, "/api/admin/upload/member-batch").then(res => {
      setBtnLoading(false)
      setModal(false)
      toast.success(
        <ToastContent msg="Successfully Imported Users" />,
        { transition: Slide, hideProgressBar: true, autoClose: 2000 }
      )
      setReload(!reload)
    }).catch(err => {
      console.log(err)
    })
  }

   // ** Form Submit Status
   const handleStatus = async (id, status) => {
    await API_ACTION({status}, `api/admin/training-status/${id}`, 'put')
        toast.success(
            <ToastContent msg="Successfully updated status." />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
        )
        getData()
}

  // ** Select File Upload
  const handleInputChange = (e) => {
    setState({ selectedFile: e.target.files[0] })
  }

  // ** Function to handle name filter
  const handleNameFilter = e => {
    const value = e.target.value
    let updatedData = []
    const dataToFilter = () => {
      if (searchStatus.length) {
        return filteredData
      } else {
        return data
      }
    }

    setSearchName(value)
    if (value.length) {
      updatedData = dataToFilter().filter(item => {
        const startsWith = item.title.toLowerCase().startsWith(value.toLowerCase())
        const includes = item.title.toLowerCase().includes(value.toLowerCase())

        if (startsWith) {
          return startsWith
        } else if (!startsWith && includes) {
          return includes
        } else return null
      })
      setFilteredData([...updatedData])
      setSearchName(value)
    }
  }

  const statusObj = {
    Pending: 'light-warning',
    Active: 'light-success',
    Inactive: 'light-warning',
    Rejected: 'light-danger'
  }

  const activeObj = {
    Active: 'light-success',
    Disabled: 'light-danger'
  }

  // ** Table Common Column
  const columns = [
    {
      name: 'ID',
      selector: 'id',
      sortable: true,
      width: '5%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.id}
          </div>
        )
      }
    },
    {
      name: 'Title',
      selector: 'title',
      sortable: true,
      width: '20%',
      cell: row => {
        return (
          <div className='text-truncate'>
            {row.title}
          </div>
        )
      }
    },
    {
      name: 'Price',
      selector: 'fee',
      sortable: true,
      width: '5%',
      cell: row => {
        return (
          <div className='d-flex'>
            {row.fee}
          </div>
        )
      }
    },
    {
      name: 'Atteendees',
      selector: 'fee',
      width: '10%',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.attendees_count}
          </div>
        )
      }
    },
    {
      name: 'Collected Fees',
      selector: 'fee',
      width: '10%',
      sortable: true,
      cell: row => {
        return (
          <div className='d-flex'>
            {row.payments ? row.payments : row.fee === 0 ? 'Free' : 0}
          </div>
        )
      }
    },
    {
      name: 'Schedule',
      selector: 'schedule_start',
      sortable: true,
      cell: row => {
        return (
          <div>
            <Moment format="ll">
              {row.schedule_start}
            </Moment>
            &nbsp; - &nbsp;
            <Moment format="ll">
              {row.schedule_end}
            </Moment>
            <br />
            <Moment format="LT">
              {row.schedule_start}
            </Moment>
            &nbsp; - &nbsp;
            <Moment format="LT">
              {row.schedule_end}
            </Moment>
          </div>
        )
      }
    },
    {
      name: 'AVAILABILITY',
      selector: 'availability',
      width: '10%',
      sortable: true,
      cell: row => {
        return (
          <Badge className='text-capitalize' color={moment(Date.parse(row.schedule_start)).isAfter(moment.now()) ? statusObj.Open : statusObj.Closed} pill>
            {moment(Date.parse(row.schedule_start)).isAfter(moment.now()) ? "Open" : "Closed"}
          </Badge>
        )
      }
    },
    {
      name: 'STATUS',
      width: '7%',
      selector: 'status',
      sortable: true,
      cell: row => {
        return (
          <Badge className='text-capitalize' color={activeObj[row.status]} pill>
            {row.status}
          </Badge>
        )
      }
    },
    {
      name: 'ACTIONS',
      width: '15%',
      cell: row => {
        return (
          <div className='d-flex'>
            <Link
              to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/admin/training/preview/${row.id}` : `/training/preview/${row.id}`)}
              id={`pw-tooltip-${row.id}`}>
              <Eye size={17} className='mx-1' />
            </Link>
            <UncontrolledTooltip placement='top' target={`pw-tooltip-${row.id}`}>
              Preview Training
            </UncontrolledTooltip>

            <Link
              to={(isUserLoggedIn() && getUserData().role === 'Admin' ? `/admin/training/edit/${row.id}` : `/training/edit/${row.id}`)}
              id={`edit-tooltip-${row.id}`}>
              <Edit size={17} className='mx-1' />
            </Link>
            <UncontrolledTooltip placement='top' target={`edit-tooltip-${row.id}`}>
              Update Training
            </UncontrolledTooltip>

            <Link
              to={`/trainings/attendees/${row.id}`}
              id={`attendees-tooltip-${row.id}`}>
              <User size={17} className='mx-1' />
            </Link>
            <UncontrolledTooltip placement='top' target={`attendees-tooltip-${row.id}`}>
              View Attendees
            </UncontrolledTooltip>

            <Link
              onClick={() => handleStatus(row.id, row.status === 'Active' ? 'Disabled' : 'Active')}
              id={`status-tooltip-${row.id}`}>
              <Power size={17} className='mx-1' />
            </Link>
            <UncontrolledTooltip placement='top' target={`status-tooltip-${row.id}`}>
              {row.status === 'Active' ? 'Deactivate' : 'Activate'} 
            </UncontrolledTooltip>
          </div>
        )
      }
    }
  ]

  return (
    <Fragment>
      <div style={{ paddingBottom: 10 }}>
        <Breadcrumb>
          <BreadcrumbItem>
            <Link to='/training/view'> <h5 className="text-primary">Trainings</h5> </Link>
          </BreadcrumbItem>
        </Breadcrumb>
      </div>
      <Card>
        <CardHeader>
          Search Filter
        </CardHeader>
        <CardBody>
          <Row>
            <Col className='my-md-0 my-1'>
              <FormGroup>
                <Input id='name' placeholder='Input Title' value={searchName} onChange={handleNameFilter} />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <div className='invoice-list-table-header w-100'>
            <Row>
              <Col lg='6' className='d-flex align-items-center px-0 px-lg-1'>
                <div className='d-flex align-items-center mr-2'>
                  <Label for='rows-per-page'>Show</Label>
                  <CustomInput
                    className='form-control ml-50 pr-3'
                    type='select'
                    id='rows-per-page'
                  // onChange={handlePerPage}
                  >
                    <option value='10'>10</option>
                    <option value='25'>25</option>
                    <option value='50'>50</option>
                  </CustomInput>
                </div>

              </Col>
              <Col
                lg='6'
                className='actions-right d-flex align-items-center justify-content-lg-end flex-lg-nowrap flex-wrap mt-lg-0 mt-1 pr-lg-1 p-0'
              >
                <Button.Ripple tag={Link} to='/training/add' color='primary' outline>
                  Add New Training
                </Button.Ripple>
              </Col>
            </Row>
          </div>
        </CardHeader>
        <Row>
          <Col sm='12'>
            <Card>
              <Table
                filteredData={filteredData}
                data={data}
                searchName={searchName}
                searchStatus={searchStatus}
                searchSubscription={searchSubscription}
                columns={columns}
                currentPage={currentPage}
              />
              <Tble>
                <tbody>
                  <tr>
                    <th width="15%"></th>
                    <th width="20%"></th>
                    <th width="20%"><h6><strong>Total Collected: {searchStatus.length > 0 ? filteredData.reduce((prev, next) => prev + next.payments, 0) : data.reduce((prev, next) => prev + next.payments, 0)}</strong></h6></th>
                    <th></th>
                    <th></th>
                  </tr>
                </tbody>
              </Tble>

            </Card>
          </Col>
        </Row>
      </Card>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Batch Upload</ModalHeader>
        <ModalBody>
          <input
            type="file"
            multiple
            className="form-control"
            name="document[]"
            onChange={handleInputChange}
            required
            style={{
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis",
              paddingBottom: '35px'
            }}
          />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={batchUpload}>Upload</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    </Fragment>
  )
}


export default Trainings
