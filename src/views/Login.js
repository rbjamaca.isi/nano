import { Link, useHistory } from 'react-router-dom'
import InputPasswordToggle from '@components/input-password-toggle'
import { Card, CardBody, CardTitle, CardText, Form, FormGroup, Row, Col, Label, Input, CustomInput, Button } from 'reactstrap'
import Avatar from '@core/components/avatar'
import '@styles/base/pages/page-auth.scss'
import { useForm } from 'react-hook-form'
import classnames from 'classnames'
import { useDispatch } from 'react-redux'
import { isObjEmpty } from '@utils'
import useJwt from 'auth/jwt/useJwt'
import { handleLogin } from 'redux/actions/auth'
import { useContext, Fragment } from 'react'
// import { AbilityContext } from 'utility/context/Can'
import { AbilityContext } from '@src/utility/context/Can'
import { toast, Slide } from 'react-toastify' 
import { getAbilityForLoggedInUser, getHomeRouteForLoggedInUser } from 'utility/Utils' 
import { Facebook, Twitter, Mail, GitHub, HelpCircle, Coffee } from 'react-feather'
import Ability from 'configs/acl/ability'
import { useSkin } from '@hooks/useSkin'
import { CustomToast } from 'utility/CustomToast'

const ToastContent = ({ name, role }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Welcome, {name}</h6>
      </div>
    </div>
    <div className='toastify-body'>
      <span>You have successfully logged in as {role} to NANO.</span>
    </div>
  </Fragment>
)

const ToastContentUnverified = () => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='warning' icon={<Coffee size={12} />} />
        <h6 className='toast-title font-weight-bold'>Unverified Account</h6>
      </div>
    </div>
    <div className='toastify-body'>
      <span>Waiting for your payment to be verified.</span>
    </div>
  </Fragment>
)


const Login = () => {
  const [skin, setSkin] = useSkin()
  const dispatch = useDispatch()
  const { register, errors, handleSubmit } = useForm()
  const ability = useContext(AbilityContext)
  const history = useHistory()

  const onSubmit = values => {
    if (isObjEmpty(errors)) {
      useJwt.login(values).then(res => {
        const newAbility = getAbilityForLoggedInUser(res.data.user.role)
        const data = { ...res.data.user, ability: newAbility, accessToken: res.data.accessToken }

        if (res.data.user.role !== "Admin" && res.data.user.status !== "Active" && res.data.user.is_premium) {
          toast.error(
            <ToastContentUnverified/>,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        } else {
          dispatch(handleLogin(data))
          toast.success(
            <ToastContent name={`${data.firstname} ${data.lastname}` || data.username || 'John Doe'} role={data.role || 'admin'} />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
          ability.update(newAbility)
          history.push(getHomeRouteForLoggedInUser(data.role))
        }
      }).catch(err => {
        if (err.response.status === 403) {
          toast.error(
            <CustomToast title="Unauthorized" msg={err.response.data.message} icon={<Coffee size={12} />} color="danger" />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        } else {
          toast.error(
            <CustomToast title="Error" msg="An error has occurred. Please try again later." icon={<Coffee size={12} />} color="danger" />,
            { transition: Slide, hideProgressBar: true, autoClose: 2000 }
          )
        }
      })
      
    }
  }

  const illustration = skin === 'dark' ? 'login-v2-dark.svg' : 'login-v2.svg',
    source = require(`@src/assets/images/pages/${illustration}`).default,
    logo = require('@src/assets/images/logo/logo.png').default

  return (
    <div className='auth-wrapper auth-v2'>
      <Row className='auth-inner m-0'>
        <Link className='brand-logo' to='/'>
        <img src={logo} width='100px' />
        </Link>
        <Col className='d-none d-lg-flex align-items-center p-5' lg='8' sm='12'>
          <div className='w-100 d-lg-flex align-items-center justify-content-center px-5'>
          <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_xlmz9xwm.json"  background="transparent"  speed="1"  style={{width: 800, height: 800}}  loop autoplay></lottie-player>

          </div>
        </Col>
        <Col className='d-flex align-items-center auth-bg px-2 p-lg-5' lg='4' sm='12'>
          <Col className='px-xl-2 mx-auto' sm='8' md='6' lg='12'>
            <CardTitle tag='h2' className='font-weight-bold mb-1'>
              Welcome to NANO!
            </CardTitle>
            <CardText className='mb-2'>Please sign-in to your account and start your learning adventure</CardText>
            <Form className='auth-login-form mt-2'  onSubmit={handleSubmit(onSubmit)}>
              <FormGroup>
                <Label className='form-label' for='login-email'>
                  Email
                </Label>
                <Input autoFocus
                  type='email'
                  name='email'
                  id='email'
                  placeholder='john.doe@example.com'
                  innerRef={register({ required: true })}
                  className={classnames({ 'is-invalid': errors['email'] })}
                  autoFocus />
              </FormGroup>
              <FormGroup>
                <div className='d-flex justify-content-between'>
                  <Label className='form-label' for='login-password'>
                    Password
                  </Label>
                  <Link to='/forgot-password'>
                    <small>Forgot Password?</small>
                  </Link>
                </div>
                <InputPasswordToggle 
                  className='input-group-merge' 
                  name='password'
                  id='password'
                  innerRef={register({ required: true })} />
              </FormGroup>
              <FormGroup>
                <CustomInput type='checkbox' className='custom-control-Primary' id='remember-me' label='Remember Me' />
              </FormGroup>
              <Button.Ripple type='submit' color='primary' block>
                Sign in
              </Button.Ripple>
            </Form>
            <p className='text-center mt-2'>
              <span className='mr-25'>New on our platform?</span>
              <Link to='/register'>
                <span>Create an account</span>
              </Link>
            </p>
            {/* <div className='divider my-2'>
              <div className='divider-text'>or</div>
            </div>
            <div className='auth-footer-btn d-flex justify-content-center'>
              <Button.Ripple color='facebook'>
                <Facebook size={14} />
              </Button.Ripple>
              <Button.Ripple color='twitter'>
                <Twitter size={14} />
              </Button.Ripple>
              <Button.Ripple color='google'>
                <Mail size={14} />
              </Button.Ripple>
              <Button.Ripple className='mr-0' color='github'>
                <GitHub size={14} />
              </Button.Ripple>
            </div> */}
          </Col>
        </Col>
      </Row>
    </div>
  )
}

export default Login
