import { Home, PieChart, User, Award, FileText, Circle } from 'react-feather'

export default [
  {
    id: 'home',
    title: 'Home',
    icon: <Home size={20} />,
    navLink: '/landing',
    action: 'read',
    resource: 'Admin'
  },
  {
    id: 'dash',
    title: 'Dashboard',
    icon: <PieChart size={20} />,
    navLink: '/admin',
    action: 'read',
    resource: 'Admin'
  },
  {
    id: 'membersPage',
    title: 'Members',
    icon: <User size={20} />,
    navLink: '/members',
    resource: 'Admin'
  },
  // {
  //   id: 'attendeesPage',
  //   title: 'Attendees',
  //   icon: <User size={20} />,
  //   navLink: '/attendees/view',
  //   resource: 'Admin'
  // },
  {
    id: 'trainingsPage',
    title: 'Trainings',
    icon: <Award size={20} />,
    navLink: '/trainings',
    resource: 'Admin'
  },
  // {
  //   id: 'sponsorsPage',
  //   title: 'Sponsors',
  //   icon: <Award size={20} />,
  //   navLink: '/sponsors',
  //   resource: 'Admin'
  // },
  // {
  //   id: 'certificatesPage',
  //   title: 'Certificates',
  //   icon: <Award size={20} />,
  //   navLink: '/certificates',
  //   resource: 'Admin'
  // }
  {
    id: 'reports',
    title: 'Reports',
    icon: <FileText size={20} />,
    children: [
      {
        id: 'claimed',
        title: 'Claimed Rewards',
        icon: <Circle />,
        navLink: '/reports/claimed',
        resource: 'Admin'
      },
      {
        id: 'wallets',
        title: 'Account Wallets',
        icon: <Circle />,
        navLink: '/reports/wallets',
        resource: 'Admin'
      }
    ]
  }
]
