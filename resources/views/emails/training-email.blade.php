@component('mail::message')

# Hello, {{$data['name']}}!

{{$data['message']}}

@if ($data['status'] === 'Approved')
{{-- @component('mail::button', ['url' => URL::to('login')]) --}}
@component('mail::button', ['url' => 'https://dit-ads.com/training/link/'.$data['code']])
Sign In
@endcomponent
@endif


Thanks,<br>
Admin
@endcomponent
