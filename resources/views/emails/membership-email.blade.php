@component('mail::message')

@if ($data['status'] === 'Active')
# Welcome to NANO, {{$data['name']}}!
@endif

{{$data['message']}}

@if ($data['status'] === 'Active')
{{-- @component('mail::button', ['url' => URL::to('login')]) --}}
@component('mail::button', ['url' => 'https://dit-ads.com/login'])
Sign In
@endcomponent
@endif


Thanks,<br>
Admin
@endcomponent
