<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
   protected $guarded = [];

   /**
    * Get all of the speakers for the Training
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function speakers()
   {
       return $this->hasMany(Speaker::class);
   }

   /**
    * The attendees that belong to the Training
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
   public function attendees()
   {
       return $this->belongsToMany(User::class)->select(['training_user.payment_amount', 'payment_method', 'ref_no', 'photo', 'training_user.status', 'user_id', 'training_id']);
   }

   /**
    * The tags that belong to the Training
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
   public function tags()
   {
       return $this->belongsToMany(Tag::class);
   }
}
