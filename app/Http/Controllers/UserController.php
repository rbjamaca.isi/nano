<?php

namespace App\Http\Controllers;

use App\Mail\AttendeeTrainingMail;
use Illuminate\Http\Request;
use App\Mail\MyTestMail;
use App\Training;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\hash;
use Illuminate\Support\Facades\Mail;

use DB;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
            'role' => 'required',
            'gender' => 'required',
            'civilstatus' => 'required',
            'dob' => 'required',
            'occupation' => 'required',
            'photo' => 'required',
            'street' => 'required',
            'barangay' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postalcode' => 'required',
            'mobile' => 'required',
            'telephone' => 'required',
            'email' => 'required',
            'password' => 'required',
            'status' => 'required'
        ]);

        $user = new UserModel();

        $user->firstname = $request->input('firstname');
        $user->middlename = $request->input('middlename');
        $user->lastname = $request->input('lastname');
        $user->role = $request->input('role');
        $user->gender = $request->input('gender');
        $user->civilstatus = $request->input('civilstatus');
        $user->dob = $request->input('dob');
        $user->occupation = $request->input('occupation');
        $user->photo = $request->input('photo');
        $user->street = $request->input('street');
        $user->barangay = $request->input('barangay');
        $user->city = $request->input('city');
        $user->province = $request->input('province');
        $user->postalcode = $request->input('postalcode');
        $user->mobile = $request->input('mobile');
        $user->telephone = $request->input('telephone');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->status = $request->input('status');

        $user->save();
        return response()->json([
            'success' => true,
        ]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
        ]);
        $user = User::find($request->id);
     
        return response()->json([
            'data' => $user->update($request->except('firstname', 'middlename', 'lastname', 'role', 'gender', 'civilstatus', 'dob',
        'occupation', 'photo','status', 'password')),
            'success' => true,
        ]);
    }

    public function password(Request $request)
    {
        $user = User::find($request->id);

        $user->update(['password' => bcrypt($request->password)]);

        return response()->json([
            'data' => $user,
            'success' => true,
        ]);
    }

    public function index()
    {
        return response()->json([
            'user' => auth()->user(),
        ]);
    }

    public function uploadProof(Request $request)
    {
        $validatedData = $request->validate([
            'reference' => ['required', 'string', 'unique:users']
        ]);

        $fileWithExt = $request->file('my_file')->getClientOriginalName();
        $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
        $extension = $request->file('my_file')->getClientOriginalExtension();
        $fileToStore = $filename . '_' . time() . '.' . $extension;
        $path = $request->file('my_file')->storeAs('public/images/proof', $fileToStore);

        $user = User::find(auth()->user()->id);
        $request->merge([
            'proof' => $fileToStore,
            'is_premium' => true,
            'status' => 'Pending',
            'free_webinars' => 2,
            'type' => 'Regular'
        ]);
        $user->update($request->except('payment_method', 'ref_no', 'my_file'));

        return response()->json([
            'success' => true
        ]);
    }

    public function trainingProof(Request $request)
    {
        $validatedData = $request->validate([
            'ref_no' => ['sometimes', 'required', 'string', 'unique:training_user']
        ]);

        $training = Training::find($request->id);
        $user = Auth::user()->id;
        if ($request->my_file) {
            $fileWithExt = $request->file('my_file')->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $request->file('my_file')->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('my_file')->storeAs('public/images/training/proof', $fileToStore);


            $request->merge([
                'photo' => $fileToStore,
            ]);
        }

        if ($request->status && $request->status === "Approved") {
            $user_data = Auth::user();
            $approved = 'Your request to join the training ' . $training->title . ' has been approved. The training schedule is on ' . Carbon::parse($training->schedule_start)
                ->toCookieString() . ' to ' . Carbon::parse($training->schedule_end)->toCookieString() . '.';
            $data = [
                'name' => $user_data->firstname,
                'message' => $approved,
                'status' => 'Approved',
                'code' => $training->code,
            ];

            Mail::to($user_data->email)->send(new AttendeeTrainingMail($data));
        }

        if (!$request->my_file && !$training->attendees->contains($user)) {
            $free = User::find($user);
            $free->update(['free_webinars' => $free->free_webinars - 1]);
        }

        $training->attendees()->sync([$user => $request->except(['id', 'my_file', 'type', 'name', 'relativePath'])], false);

        return response()->json([
            'success' => $training
        ]);
    }

    public function trainingJoin($id)
    {

        $training = Training::find($id);
        $user = Auth::user()->id;

        $training->attendees()->sync([$user => ['status' => 'Approved']], false);
        $approved = 'Your request to join the training ' . $training->title . ' has been approved. The training schedule is on ' . Carbon::parse($training->schedule_start)
            ->toCookieString() . ' to ' . Carbon::parse($training->schedule_end)->toCookieString() . '.';

        $user_data = User::find($user);
        $data = [
            'name' => $user_data->firstname,
            'message' => $approved,
            'status' => 'Approved',
            'code' => $training->code,
        ];

        Mail::to($user_data->email)->send(new AttendeeTrainingMail($data));
        return response()->json([
            'user' => $user,
            'success' => $training
        ]);
    }

    public function availed()
    {

        $user = User::with(['trainings.tags', 'trainings.speakers'])->find(auth()->user()->id);


        return response()->json([
            'trainings' => $user->trainings,
            'success' => true,
        ]);
    }

    public function trainingProofNonMember(Request $request)
    {
        $user =  User::firstOrCreate(['email' => $request->email, 'role' => 'Participant', 'firstname' => $request->firstname, 'lastname' => $request->lastname, 'email' => $request->email, 'occupation' => $request->occupation]);
        $training = Training::find($request->id);

        if ($request->my_file) {
            $fileWithExt = $request->file('my_file')->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $request->file('my_file')->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('my_file')->storeAs('public/images/training/proof', $fileToStore);


            $request->merge([
                'photo' => $fileToStore,
            ]);
        }

        if (!$request->my_file && !$training->attendees->contains($user)) {
            $free = User::find($user->id);
            $free->update(['free_webinars' => $free->free_webinars - 1]);
        }

        $training->attendees()->attach([$user->id => $request->except(['id', 'my_file', 'type', 'name', 'relativePath', 'firstname', 'lastname', 'email', 'occupation'])]);

        return response()->json([
            'success' => $training
        ]);
    }

    public function authenticatedUser()
    {
        return response()->json([
            'user' => auth()->user()
        ]);
    }

    public function changeStatus(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
            'email' => 'required',
            'name' => 'required',
        ]);

        $status = $request->input('status');
        $email = $request->input('email');
        $aprroved = 'Your membership registration has been approved. You may login using your credentials by clicking on the button below.';
        $declined = 'Sorry, but your membership was declined';

        $number = $request->input('number');
        $api = 'TR-NADDI931244_X7GUC';
        $apipass = 'af]x#ackxq';

        if ($request->status) {
            User::find($id)
                ->update(
                    [
                        'status' => $status
                    ]
                );

            if ($status == "Active") {
                $data = [
                    'name' => $request->name,
                    'message' => $aprroved,
                    'status' => 'Active'
                ];
                // $result = $this->itexmo($number,$aprrove,$api, $apipass);
                // if ($result == 0){
                //     return response()->json([
                //         'success' => "Message successfully sent!"
                //         ]);
                // } 
                // else{	
                //     return response()->json([
                //         'success' =>  "Error Num ". $result . " was encountered!"
                //         ]);
                // }
                Mail::to($email)->send(new MyTestMail($data));
            } else if ($status == "Inactive") {
                $user = User::find($id);
                $user->delete();
                $data = [
                    'name' => $request->name,
                    'message' => $declined,
                    'status' => 'Inactive'
                ];
                // $result = $this->itexmo($number,$declined,$api, $apipass);
                // if ($result == 0){
                //     return response()->json([
                //         'success' =>  "Message successfully sent!"
                //         ]);
                // }
                // else{	
                //     return response()->json([
                //         'success' => "Error Num ". $result . " was encountered!"
                //         ]); 
                // }
                Mail::to($email)->send(new MyTestMail($data));
            }
        }
        return response()->json([
            'success' =>  "Message successfully sent!"
        ]);
    }

    public function itexmo($number, $message, $apicode, $passwd)
    {
        $ch = curl_init();
        $itexmo = array('1' => $number, '2' => $message, '3' => $apicode, 'passwd' => $passwd);
        curl_setopt($ch, CURLOPT_URL, "https://www.itexmo.com/php_api/api.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            http_build_query($itexmo)
        );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return curl_exec($ch);
        curl_close($ch);
    }

    public function emailChecker(Request $request)
    {
        $validatedData = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        return response()->json([
            'success' => true
        ]);
    }
}
