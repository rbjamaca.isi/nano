<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Codedge\Fpdf\Fpdf\Fpdf;

class pdfcontroller extends Controller
{
    private $fpdf;
    public function __construct()
    {
         
    }
 
    public function createPDF()
    {
        $this->fpdf = new Fpdf;
        $this->fpdf->AddPage("L", ['100', '100']);
        $this->fpdf->Text(10, 10, "Hello FPDF");
        $this->fpdf->AddFont('Comic','','comic.php');
        $this->fpdf->AddFont('Comic','B','comicbd.php');   
         
        $this->fpdf->Output();
        exit;
    }
}
