<?php

namespace App\Http\Controllers;

use App\Mail\MyTestMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request) {
        $validatedData = $request->validate([
            'firstname' => ['required', 'string'],
            'lastname' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        

        $validatedData['password'] = bcrypt($request->password);

        $user =  User::create($validatedData);

        // $accessToken = $user->createToken('authToken')->accessToken;
        $aprroved = 'Your have successfully registered as a free member. You may login using your credentials by clicking on the button below.';
            $data = [
                'name' => $user->firstname,
                'message' => $aprroved,
                'status' => 'Active'
            ];

        Mail::to($user->email)->send(new MyTestMail($data));
        
        return response()->json([
            'user' => $user,
            // 'accessToken' => $accessToken
        ], 201);

    }

    public function memberDetails(Request $request) {
        $validatedData = $request->validate([
            'reference' => ['required', 'string', 'sometimes', 'unique:users'],
            'firstname' => ['required', 'string'],
            'lastname' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $validatedData['password'] = bcrypt($request->password);
        $user =  User::create($validatedData);
        
        if ($request->proof){
            $fileWithExt = $request->file('proof')->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $request->file('proof')->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('proof')->storeAs('/public/images/proof', $fileToStore);

            $user->update([
                'proof' => $fileToStore,
            ]);
        }

        $request->merge([
            'is_premium' => $request->is_premium == "true" ? true : false,
            'free_webinars' => $request->type == 'Regular' ? 2 : 1,
        ]);

        $user->update($request->except('proof', 'password', 'password_confirmation'));
        
        if(!$request->is_premium) {
            $aprroved = 'Your have successfully registered as a free member. You may login using your credentials by clicking on the button below.';
            $data = [
                'name' => $user->firstname,
                'message' => $aprroved,
                'status' => 'Active'
            ];

            Mail::to($user->email)->send(new MyTestMail($data));
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ]);

    }

    public function login(Request $request) {

        $loginData = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        

        if (!auth()->attempt($loginData)) {
            return response()->json(['message' => 'E-mail or password is incorrect.'], 403);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        $user = User::find(auth()->user()->id);

       
        return response()->json([
            'success' => true,
            'user' => $user,
            'accessToken' => $accessToken
        ]);
    }


    public function logout(Request $res)
    {
      if (Auth::user()) {

        $user = Auth::user()->token();
        $user->revoke();

        return response()->json([
          'success' => true,
          'message' => 'You are now logged out.'
          
      ]);

      } else {

        return response()->json([
          'success' => false,
          'message' => 'Cannot log out.'
        ]);

      }

     }
}
