<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpeakerModel;

class SpeakerController extends Controller
{
    //training user
    public function store(Request $request){
        $request->validate([
            'payment_amount' => 'required',
            'payment_method' => 'required',
            'ref_no' => 'required',
            'photo' => 'mimes:jpeg,bmp,png',
            'status' => 'required',
        ]);
        $user_training = new UserModel();
            $user_training->payment_amount = $request->input('payment_amount');
            $user_training->payment_method = $request->input('payment_method');
            $user_training->ref_no = $request->input('ref_no');
            $user_training->photo = $request->input('photo');
            $user_training->status = $request->input('status');
            $user_training->save();
        return response()->json([
            'success' => true,
            ]);
         
    }

}
