<?php

namespace App\Http\Controllers;

use App\Advert;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['adverts'] = Advert::with('sponsor')
        ->whereDate('start', '<=', Carbon::now()->timezone('Asia/Manila')->format('Y-m-d'))
        ->whereDate('end', '>=', Carbon::now()->timezone('Asia/Manila')->format('Y-m-d'))
        ->latest()->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $images = [];
        foreach ($request->images as $image){
            $fileWithExt = $image->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $image->storeAs('/public/images/adverts', $fileToStore);
            $images[] = $fileToStore;
        }


        foreach (json_decode($request->ads) as $key => $value) {
            Advert::create([
                'sponsor_id' => $request->id,
                'title' => $value->title,
                'start' => $value->start,
                'end' => $value->end,
                'url' => $value->url,
                'image' => $images[$key]
            ]);
        }
        
        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function show(Advert $advert)
    {
        return response()->json([
            'success' => true,
            'data' => Advert::with('sponsor')->find($advert->id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advert $advert)
    {
        $data['advert'] = $advert->update($request->all());

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advert  $advert
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advert $advert)
    {
        $data['advert'] = $advert->delete();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
}
