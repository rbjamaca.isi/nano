<?php

namespace App\Http\Controllers;

use App\Speaker;
use App\Tag;
use Illuminate\Http\Request;
use App\Training;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TrainingController extends Controller
{
   public function store(Request $request){
    $request->validate([
        'title' => 'required',
        'desc' => 'required',
        'fee' => 'required',
        'image' => 'required|file',
        'sched' => 'required',
        'slots' => 'required',
    ]);
    
    $train = new Training();
    $train->title = $request->input('title');
    $train->code = $this->generate_code();
    $train->desc = $request->input('desc');
    $train->slots = $request->input('slots');
    $scheds = json_decode($request->sched);
    $start = Carbon::parse($scheds[0])->format('Y-m-d H:i:s');
    $end = Carbon::parse($scheds[1])->format('Y-m-d H:i:s');
    $train->schedule_start = Carbon::createFromFormat('Y-m-d H:i:s', $start, 'UTC')->setTimezone('Asia/Taipei');
    $train->schedule_end = Carbon::createFromFormat('Y-m-d H:i:s', $end, 'UTC')->setTimezone('Asia/Taipei');
    $train->fee = $request->input('fee');
    $train->location = $request->input('location');
    $train->link = $request->input('link');
    $train->slots = $request->input('slots');
    $train->is_free = $request->is_free == 'true' ? true : false;

    $fileWithExt = $request->file('image')->getClientOriginalName();
    $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
    $extension = $request->file('image')->getClientOriginalExtension();
    $fileToStore = $filename . '_' . time() . '.' . $extension;
    $path = $request->file('image')->storeAs('/public/images/training', $fileToStore);
    $train->image = $fileToStore;

    $train->save();
    
    $tags = json_decode($request->tags);
    foreach ($tags as  $tag) {
        $tag = Tag::firstOrCreate(['desc' => $tag->value]);
        $train->tags()->attach($tag->id);
    }

    if($request->speakers){
        $speakers = json_decode($request->speakers);

        foreach ($speakers as $speaker) {
            $speaker_data = Speaker::create(['name' => $speaker->name, 'occupation' => $speaker->occupation, 'training_id' => $train->id]);
        }
    }
    
    return response()->json([
        'training' => $train,
        'message' => 'success',
    ]);
   }

   public function code($code)
   {
       return response()->json([ 'training' => Training::with(['attendees'])->where('code', $code)->first()]);
   }

   public function tags()
   {
       $tag_arr = [];
       $tags = tag::all();

       $final = $tags->map(function($q){
            ['value' => $q->desc, 'label' => $q->desc, 'color' => $this->rand_color(), 'isFixed' => true];
            $tag_arr['value'] = $q->desc;
            $tag_arr['label'] = $q->desc;
            $tag_arr['color'] = $this->rand_color();
            $tag_arr['isFixed'] =true;
            return $tag_arr;
       });

       $data['tags'] = $final->values();
       
       return response()->json([
            'data' => $data,
        ]);
   }

   function rand_color()
    {
        return sprintf('#%06X', mt_rand(0xFF9999, 0xFFFF00));
        // return $rand = '#'.str_pad(dechex(rand(0x000000, 0xFFFFFF)), 6, 0, STR_PAD_LEFT);
    }

   private function generate_code($code = null)
   {
       $length = 6;
       $new_code = $code;
       if (empty($new_code)) {
           $pool = '0123456789abcdefghijklmnopcodestuvwxyzABCDEFGHIJKLMNOPcodeSTUVWXYZ';
           $new_code = substr(str_shuffle(str_repeat($pool, 5)), 1, $length);
       }
       $code_exists = Training::where('code', $new_code)->first();
       if (!empty($code_exists)) {
           return generate_code($code = $new_code);
       }
       return $new_code;
   }

   public function index(){
    $data['trainings'] = Training::with(['tags', 'speakers'])->where('status', 'Active')->latest()->get();

    return response()->json([
        "data" => $data
    ]);
   }

   public function show($id)
   {
       return response()->json([
           'data' => Training::with(['tags', 'speakers'])->findOrFail($id)
       ]);
   }

   public function update(Request $request)
   {
        $request->validate([
            'image' => 'required|sometimes',
        ]);

        $train = Training::find($request->id);

        if($request->image && $request->image != 'undefined'){
            $fileWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $request->file('image')->storeAs('/public/images/training', $fileToStore);
            $train->update(['image' => $fileToStore]);
        }
        if($request->sched){
            $scheds = json_decode($request->sched);
            $start = Carbon::parse($scheds[0])->format('Y-m-d H:i:s');
            $end = Carbon::parse($scheds[1])->format('Y-m-d H:i:s');

            $request->merge([
                'schedule_start' =>Carbon::createFromFormat('Y-m-d H:i:s', $start, 'UTC')->setTimezone('Asia/Taipei'),
                'schedule_end' => Carbon::createFromFormat('Y-m-d H:i:s', $end, 'UTC')->setTimezone('Asia/Taipei')
            ]);
        }

        // foreach ($request->tags as  $tag) {
        //     $tag = Tag::firstOrCreate(['desc' => $tag['value']]);
        //     $train->tags()->sync($tag->id, false);
        // }
    
        // if($request->speakers){
            
        //     foreach ($request->speakers as $speaker) {
        //         $speaker_data = Speaker::create(['name' => $speaker['name'], 'occupation' => $speaker['occupation'], 'training_id' => $train->id]);
        //     }
        // }

        return response()->json([
            'data' => $train->update($request->except('sched', 'image', 'speakers', 'tags', 'update_status')),
            'success' => true
        ]);
   }

   public function reopentraining(Request $request, $id){
    $request->validate([
        'from_date'=>'required',
        'to_date'=>'required',
    ]);
    $train = Training::find($id);
    // $start = Carbon::parse($request->sched[0])->format('Y-m-d H:i:s');
    // $end = Carbon::parse($request->sched[1])->format('Y-m-d H:i:s');
    if ($request->has('from_date')) {
        $result = Training::where('id', $id)
        ->update(
            [
                'schedule_start' => $request['from_date'],
                'schedule_end' => $request['to_date'],
            ]
        );
        return response()->json([
            
            'success' => true
        ]);
    }



   }
 
}
