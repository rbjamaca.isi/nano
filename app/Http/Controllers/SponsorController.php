<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Sponsor;
use Illuminate\Http\Request;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['sponsors'] = Sponsor::with('adverts')->latest()->get();

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'company' => 'required',
            'images' => 'required',
            'ads'=> 'required'            
        ]);
        
        $images = [];
        foreach ($request->images as $image){
            $fileWithExt = $image->getClientOriginalName();
            $filename = pathinfo($fileWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $fileToStore = $filename . '_' . time() . '.' . $extension;
            $path = $image->storeAs('/public/images/adverts', $fileToStore);
            $images[] = $fileToStore;
        }

        $data['sponsor'] = Sponsor::create($request->except(['images', 'ads']));
        $keys = [];
        foreach (json_decode($request->ads) as $key => $value) {
            Advert::create([
                'sponsor_id' => $data['sponsor']->id,
                'title' => $value->title,
                'start' => $value->start,
                'end' => $value->end,
                'url' => $value->url,
                'image' => $images[$key]
            ]);
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function show(Sponsor $sponsor)
    {
        return response()->json([
            'success' => true,
            'data' => Sponsor::with('adverts')->find($sponsor->id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sponsor $sponsor)
    {
        $data['sponsor'] = $sponsor->update($request->all());

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sponsor $sponsor)
    {
        $data['sponsor'] = $sponsor->delete();

        return response()->json([
            'success' => true
        ]);
    }
}
