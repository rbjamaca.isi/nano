<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Mail\AttendeeTrainingMail;
use App\Training;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index()
    {
        $data['users'] = User::whereNotIn('role', ['Admin', 'Participant'])->latest()->get();

        return response()->json([
            'data' => $data
        ]);
    }

    public function trainings(){
        $data['trainings'] = Training::with(['tags', 'speakers'])->withCount(['attendees' => function($q){
            $q->where('training_user.status', '!=', 'Rejected');
        }, 'attendees as payments' => function(Builder $q) {
            $q->where('training_user.status', '!=', 'Rejected')->select(DB::raw('sum(training_user.payment_amount)'));
        }])->latest()->get();
    
        return response()->json([
            "data" => $data
        ]);
    }

    public function attendees()
    {
        $data['users'] = User::select('users.*', 'trainings.*', 'training_user.*', 'training_user.status as attend_status')
                        ->join('training_user', 'users.id', '=', 'training_user.user_id')
                        ->join('trainings', 'trainings.id', '=', 'training_user.training_id')
                        ->where('training_user.status', '!=', 'Rejected')    
                        ->whereNotIn('role', ['Admin'])->orderBy('training_user.created_at', 'DESC')->get();

        return response()->json([
            'data' => $data
        ]);
    }

    public function chartOverview()
    {
       $data['overview'] = collect([
            [
                'title'=> User::whereNotIn('role', ['Super Admin', 'Admin'])->get()->count(),
                'subtitle'=> 'Members',
                'color'=> 'light-danger',
                'icon'=> 'User'],
           [
                'title'=> User::where('status', '!=', 'Inactive')->get()->sum('payment_amount'),
                'subtitle'=> 'Membership Fees',
                'color'=> 'light-primary',
                'icon'=> 'Trending'],
            [
                'title'=>  Training::all()->count(),
                'subtitle'=> 'Trainings',
                'color'=> 'light-info',
                'icon'=> 'Box'],
            
            [
                'title'=> Training::withCount(['attendees' => function(Builder $q){
                    $q->where('training_user.status', '!=', 'Rejected')->select(DB::raw('sum(training_user.payment_amount)'));
                }])->get()->sum('attendees_count'),
                'subtitle'=> 'Training Fees',
                'color'=> 'light-success',
                'icon'=> 'Dollar'],
       ]);

        return response()->json([
            'data' => $data,
        ]);
    }

    public function trainingAttendees($id)
    {
        $data['users'] = User::select('users.*', 'trainings.*', 'training_user.*', 'training_user.status as attend_status')
                        ->join('training_user', 'users.id', '=', 'training_user.user_id')
                        ->join('trainings', 'trainings.id', '=', 'training_user.training_id')   
                        ->where('training_user.status', '!=', 'Rejected')    
                        ->whereNotIn('role', ['Admin'])->orderBy('training_user.created_at', 'DESC')->where('training_id', $id)->get();

        return response()->json([
            'data' => $data
        ]);
    }

    public function attendeeStatus(Request $request, $id)
    {
        $this->validate($request, [
            'training_id' => 'required',
        ]);

        $training = Training::find($request->training_id);

        $status = $request->status;
        $approved = 'Your request to join the training '.$training->title.' has been approved. The training schedule is on '.Carbon::parse($training->schedule_start)
                    ->toCookieString().' to '.Carbon::parse($training->schedule_end)->toCookieString().'.';
        $rejected = 'We regret to inform you that your request to join the training '.$training->title.' has been rejected';

        if($status == 'Approved') {
            User::find($id)->trainings()->updateExistingPivot($request->training_id, ['status' => $request->status]);;
        }
        if($status == 'Rejected') {
            User::find($id)->trainings()->detach($training->id);
        }
        $user = User::find($id);
        $data = [
            'name' => $user->firstname,
            'message' => $status == "Approved" ? $approved : $rejected,
            'status' => $status,
            'code' => $training->code,
        ];
        
        Mail::to($user->email)->send(new AttendeeTrainingMail($data));

        return response()->json([
            'success' => true,
        ]);
    }

    public function show($id)
    {
        $data['user'] = User::with('trainings.speakers')->find($id);

        return response()->json([
            'data' => $data
        ]);
    }

    public function trainingStatus(Request $request, $id)
    {
        $training = Training::find($id);
        $training->update($request->all());
        
        return response()->json([
            'data' => $training
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());

        return response()->json([
            'data' => $user,
            'success' => true
        ]);
    }
}
